\documentclass[acmsmall,review,anonymous]{acmart}
%% \settopmatter{printfolios=true,printccs=false,printacmref=false}
%% page limit 25 excluding citations

\usepackage{amsmath,amsthm,amssymb,stmaryrd}
\usepackage{bbold}
\usepackage{bussproofs}
\usepackage{keystroke}
\usepackage{comment}
\usepackage[inline]{enumitem}
\usepackage{stmaryrd}
\usepackage[mathscr]{euscript}
\usepackage[euler]{textgreek}
\usepackage{isomath}
\usepackage{microtype}
\usepackage{etoolbox}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage{agda}
\usepackage{tikz}
\usepackage{tikz-cd}

\setcopyright{none}
\bibliographystyle{ACM-Reference-Format}
\citestyle{acmauthoryear}   %% For author/year citations, required

\makeatletter
\newcommand*\NoIndentAfterEnv[1]{%
  \AfterEndEnvironment{#1}{\par\@afterindentfalse\@afterheading}}
\makeatother

\NoIndentAfterEnv{code}
\NoIndentAfterEnv{figure}

\AgdaNoSpaceAroundCode

\DeclareUnicodeCharacter{7524}{$_\mathbb{U}$}
\DeclareUnicodeCharacter{9679}{$\bullet$}
\DeclareUnicodeCharacter{10181}{$\lbag$}
\DeclareUnicodeCharacter{10182}{$\rbag$}
\DeclareUnicodeCharacter{120161}{$_\mathbb{p}$}
\DeclareUnicodeCharacter{9678}{$\circledcirc$}
\DeclareUnicodeCharacter{120792}{$\mathbb{0}$}
\DeclareUnicodeCharacter{120793}{$\mathbb{1}$}
\DeclareUnicodeCharacter{120794}{$\mathbb{2}$}
\DeclareUnicodeCharacter{738}{$^{\Sigma}$}
\DeclareUnicodeCharacter{8345}{$_n$}
\DeclareUnicodeCharacter{8343}{$_l$}
\DeclareUnicodeCharacter{7523}{$_r$}
\DeclareUnicodeCharacter{11388}{$_j$}
\DeclareUnicodeCharacter{7522}{$_i$}
\DeclareUnicodeCharacter{10521}{$\Yleft$}
\DeclareUnicodeCharacter{10522}{$\Yright$}

\newcommand{\swapp}{\AgdaInductiveConstructor{swap₊}}
\newcommand{\idc}{\AgdaInductiveConstructor{id↔}}
\newcommand{\idctwo}{\AgdaInductiveConstructor{id⇔}}
\newcommand{\compose}{\AgdaOperator{\AgdaInductiveConstructor{◎}}}
\newcommand{\pr}[2]{\langle #1,#2 \rangle}
\newcommand{\wv}[2]{\lbag #1,#2 \rbag}
\newcommand{\fd}[3]{\langle #1 \Yleft #2 \Yright #3 \rangle}
\newcommand{\ot}{\AgdaFunction{𝟙}}
\newcommand{\bt}{\AgdaFunction{𝟚}}
\newcommand{\oplusu}{\AgdaInductiveConstructor{⊕}}
\newcommand{\otimeu}{\AgdaInductiveConstructor{⊗}}
\newcommand{\plusu}{\AgdaInductiveConstructor{⊕}}
\newcommand{\timeu}{\AgdaInductiveConstructor{⊗}}
\newcommand{\iso}{\AgdaOperator{\AgdaDatatype{↔}}}
\newcommand{\isotwo}{\AgdaOperator{\AgdaDatatype{⇔}}}
\newcommand{\byiso}[1]{{\leftrightarrow}{\langle} ~#1~ \rangle}
\newcommand{\byisotwo}[1]{{\Leftrightarrow}{\langle} ~#1~ \rangle}
\newcommand{\Cx}{\mathbb{C}}
\newcommand{\tc}{\mathtt{t}\!\mathtt{t}}
\newcommand{\fc}{\mathtt{f}\!\mathtt{f}}
\newcommand{\wc}[3]{\AgdaInductiveConstructor{⟨}\AgdaSpace{}\AgdaFunction{#1}\AgdaSpace{}
  \AgdaInductiveConstructor{⤙}\AgdaSpace{}\AgdaInductiveConstructor{#2}
  \AgdaSpace{}\AgdaInductiveConstructor{⤚}\AgdaSpace{}\AgdaFunction{#3}\AgdaSpace{}
  \AgdaInductiveConstructor{⟩}}

\newcommand{\amr}[1]{\textbf{Amr says:  #1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \title{Computing with Certified Copies}
%% \title{First-Class Certified Functional Dependencies} 
\title{A Classical Analogue to Quantum Entanglement in \\
  Two-Dimensional  Type Theory}
\author{Chao-Hong Chen and Amr Sabry}
\date{\today}

\begin{abstract}
  We show that the aspects of quantum entanglement that are needed to
  realize various quantum network protocols including teleportation
  and superdense coding can be modeled in a classical two-dimensional
  reversible language with a novel wave-like notion of dependent
  pairs. The two-dimensional language is built from ideas inspired by
  Homotopy Type Theory (HoTT) and the physical principle of
  conservation of information. The first layer of the language
  consists of programs that form sound and complete type isomorphisms
  over finite types. The second layer of the language consists of
  programs that form sound and complete equivalences of level-1
  programs. In such a context, it is possible to define a variant of dependent pairs with
  wave-like properties that make the functional dependency between the pair
  components expressive enough to directly model aspects of quantum
  entanglement.
\end{abstract}

\begin{document}

\maketitle 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

With the overwhelming and growing recent interest in quantum
computation, one of the main open questions for programming language
research is understanding the computational expressiveness of quantum
entanglement~\cite{Jozsa2011}. On one hand, there are results that
suggest that quantum entanglement has classical
analogues~\cite{PhysRevA.65.032321,Spreeuw1998} and indeed some
quantum protocols like teleportation and superdense coding can be
realized using such classical
analogues~\cite{DBLP:conf/calco/ReutterV17}. But on the other hand,
there are some deep and fundamental theorems of quantum
information~\cite{EPR1935,Appleby_2005,BELL_1966,Mermin_1993,Bell1964,Kochen1967-KOCTPO-3}
that would imply that quantum entanglement \emph{cannot} have any
complete classical analogue.

In this paper we show that a recently proposed classical analogue to
quantum entanglement~\cite{DBLP:conf/calco/ReutterV17}, known as the
\emph{classical groupoid model for quantum networks} and featuring a
new ``exotic'' notion of bits called ``groubits,'' is expressible
using ``essentially'' nothing more than a dependent type. We say
``essentially'' because the host language is not a conventional
functional language but rather a two-dimensional reversible
language~\cite{CARETTE20185} inspired by recent advances in Homotopy
Type Theory (HoTT)~\cite{hottbook}. In such language, it is possible
to define a dependent type whose elements model a ``wave'' of related
(entangled) values and use that type to model various quantum
protocols.

This paper is structured as follows. In Sec.~\ref{sec:story} we
introduce some necessary background and sketch the high level ideas of
the paper and its main results. In Sec.~\ref{sec:pi} we recall the
two-dimensional language $\Pi$ and its semantics, which is extended in
Sec.~\ref{sec:dpair} with the new notion of wave-like dependent pairs
and given two operational semantics in Sec.~\ref{sec:opsem}: a
conventional ``particle-style'' semantics and an efficient
``wave-style'' semantics. The main result, which is the modeling
of the esoteric groubits is developed in Sec.~\ref{sec:gbit} and is
used to model various quantum protocols. The last section concludes
and puts our work in perspective. Because the constructions in the
paper are quite subtle, we choose to provide them in an executable
Agda format.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Ideas}
\label{sec:story}

\paragraph{Quantum computation.} 
%
A good starting point for understanding quantum computation is to
consider classical circuits over \emph{bits} but replacing the bits
with \emph{qubits}, which are intuitively superpositions of bits
weighed by complex number amplitudes. Computationally, a qubit is an
abstract data type governed by the laws of quantum physics, whose
values are normalized vectors of complex numbers in the Hilbert space
$\Cx^2$ (modulo a global phase). By choosing an orthonormal basis, say
the classical bits $\tc$ and $\fc$, a qubit can be regarded as a
complex linear combination, $\alpha~\tc + \beta~\fc$, where $\alpha$
and $\beta$ are complex numbers such that
$|\alpha|^2+|\beta|^2=1$. This generalizes naturally to multiple
qubits: the state of a system of $n$ qubits is a vector in the Hilbert
space $(\Cx^2)^{\otimes{}n}$.

The operations one can perform on a collection of qubits, i.e., a
quantum memory, are of two kinds: quantum gates and
measurements. Quantum gates are unitary operations that are ``purely
quantum'' in the sense that they modify the quantum memory without
giving any feedback to the outside world: the quantum memory is viewed
as a {\em closed system}. A customary graphical representation for
these operations is the {\em quantum circuit}, akin to conventional
boolean circuits: wires represent qubits while boxes represents
operations to perform on them. One of the peculiar aspects of quantum
computation is that the state of a qubit is
non-duplicable~\cite{wootters82single}, a result known as the {\em
no-cloning theorem}. A corollary is that a quantum circuit is a very
simple kind of circuit: wires neither split nor merge.

Measurement is a fundamentally different kind of operation: it queries
the state of the quantum memory and returns a classical
result. Measuring the state of a quantum bit is a probabilistic and
destructive operation: it produces a classical answer with a
probability that depends on the amplitudes $\alpha, \beta$ in the
state of the qubit while projecting this state onto $\tc$ or $\fc$,
based on the result.

For a more detailed introduction to quantum computation, we refer the
reader to standard textbooks (e.g., ~\cite{NC00}).

\paragraph{Entanglement.}
% 
Entanglement may be regarded as one of the main characteristics
distinguishing quantum from classical mechanics. Entanglement involves
quantum correlations such that the measurement outcomes in one
subsystem are related to the measurement outcomes in another
one. Within the standard framework, given a quantum system composed of
$n$ qubit subsystems, a pure state of the total system is said to be
entangled if it cannot be written as a product of states of each
subsystem. That is, a state |Ψ⟩ is entangled if |Ψ⟩≠
|ψ₁⟩⊗···⊗|ψⱼ⟩⊗···⊗|ψₙ⟩, where |ψⱼ⟩ refers to an arbitrary state of the
j-th qubit, and ⊗ represents the tensor product.

Controlled operations are the most common way of introducing
entanglement in quantum systems. The simplest such operation is the
controlled-not (cnot) operation; cnot takes two quantum
boolean values and:
\begin{itemize}
\item does nothing if the first value (called the control value) is
$\fc$, and
\item negates the second value (called the target value) otherwise.
\end{itemize}

This seems simple enough until we remember that the control qubit can
be simultaneously in a superposition of $\fc$ and $\tc$. In this case,
the cnot operation performs both actions simultaneously and the
resulting pair of qubits is entangled. For example consider the
situation when the control qubit is $1/\sqrt{2}(\fc+\tc)$ and the
target qubit is $\fc$. Since the control qubit is $\fc$ with a
non-zero probability, a possible output of the operation is the state
$(\fc,\fc)$. Also since the control qubit is $\tc$ with a non-zero
probability, a possible output of the operation is the state
$(\tc,\tc)$. No other outputs are possible. In other words applying
the cnot operation in this case produces a pair of entangled qubits
whose values are somehow "synchronized." The ``how'' of this
``synchronization'' really puzzled Einstein, Podolsky, and
Rosen~\cite{EPR1935} in the early days of quantum
mechanics. Specifically, they proposed a gedanken experiment that uses
entangled values in a manner that seemed to violate fundamental
principles of relativity. In their experiment, two entangled values
are separated by light years and one of them is observed on a distant
star. This immediately determines the state of the other qubit. The
mystery is to explain \emph{how} does the information about the
observed value flow to the other component? One idea that was proposed
early is that the qubits have ``hidden variables'' that determine
their future observation; this model has however been refuted by
theorems and experiments by Bell and
others~\cite{Appleby_2005,BELL_1966,Mermin_1993,Bell1964,Kochen1967-KOCTPO-3}. So
in fact, the ``how'' remains a big mystery to this day as the
following quote witnesses:

\begin{quote}
It is important to notice that there is no mechanism postulated in
this theory for how a wave function is sent into an eigenstate by an
observable. Just as mathematical logic need not demand causality
behind an implication between propositions, the logic of quantum
mechanics does not demand a specified cause behind an
observation\ldots \\ Nevertheless, the debate over the interpretation of
quantum theory has often led its participants into asserting that
causality has been demolished in physics.~\cite[p.6]{kauffman2002quantum}
\end{quote}

\paragraph{Our Contribution.}
% 

In this paper we are actually providing an interpretation of ``how''
entangled qubits are synchronized. The idea is simply to add a
functional dependency between the qubits. This does provide an
elegant, executable, specification that is appropriate for a
computational framework as we believe the computational
interpretations to be invaluable to at least provide a logical
framework for testing, reasoning, and deriving properties of
entanglement.

It would probably be presumptuous to assert that it has anything to do
with how Nature realizes entanglement. In particular, the idea that
value of each qubit is a function of the measured value of the other
qubit is quite old and can be refuted as an explanation of the quantum
phenomenon in Nature as follows. Consider a pair of entangled qubits:
we are assuming that whichever qubit is measured first communicates
its value to the other component which then updates its value. But as
Einstein, Podolsky, and Rosen noticed, this explanation violates the
principles of special relativity. The notion of one component being
measured ``first'' is not a well-defined notion as it depends on the
speed of the agent observing the measurement. In other words, it is
possible that one observer sees that the left component has been
measured first, while another observer sees that it is the right
component that has been measured first. In summary, the idea of
communicating a value from the first component to be measured to the
second component cannot be compatible with both observers, yet
experiments are invariant under change of observer.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A Two-Dimensional Pure Reversible Language}
\label{sec:pi}

Leibniz formulated the principle of \emph{identity of
  indiscernibles}. We write $A \equiv B$ for the proposition that two
objects are identifiable based on this principle, i.e., for two
objects that cannot be distinguished by any observation. Another
common relation between objects is \emph{isomorphism} or
\emph{equivalence} written as $A \simeq B$ which occurs when it is
possible to map back-and-forth between the two objects without losing
information. For example, we typically have that
$\mathbb{N} \times \mathbb{N} \simeq \mathbb{N}$ but not that
$\mathbb{N} \times \mathbb{N} \equiv \mathbb{N}$. Indeed pairs of
natural numbers are distinguishable from plain natural numbers but
there is an isomorphism between the two structures.

In a remarkable result, Voevodsky shows that it is consistent, in
intentional type theories, to consider the above two notions as
equivalent~\cite{univalence}; formally, it is consistent to postulate
the \emph{univalence axiom} $(A \equiv B) \simeq (A \simeq B)$. To
understand what this axiom entails consider the two isomorphisms
between the type $\bt$ of booleans and itself. One of the isomorphisms
is based on just the identity function and the other one uses boolean
negation to exchange $\tc$ and $\fc$. By the univalence axiom, each of
these equivalences corresponds to a proof of the proposition that $\bt
\equiv \bt$. And since it would certainly be inconsistent to identify
boolean negation with the identity function, it is therefore
inconsistent to identify the two proofs of the proposition that $\bt
\equiv \bt$. This situation keeps iterating at higher levels, with
proofs about proofs about proofs etc. needing to be distinguished,
exposing a rich combinatorial structure of the proofs of propositions
of the form $A \equiv B$.

Of particular interest to programming language specialists is that it
is possible to construct \emph{univalent subuniverses} with the
following properties. The universe would contain a chosen collection
of types of interest $A$, $B$, etc., a collection of proofs $P$, $Q$,
etc. asserting that certain types are equal $A \equiv B$, and a
collection of proofs $\alpha$, $\beta$, etc. asserting that certain
proofs are equal $P \equiv Q$. The proof structure then collapses
meaning that all proofs $\alpha$, $\beta$, etc. are equal. When
translated to the world of programming languages, such a univalent
subuniverse would induce a programming language with the following
properties~\cite{CARETTE20185}. The language would include a
collection of types $A$, $B$, etc., a collection of \emph{reversible
  programs} $P$, $Q$, etc. asserting that certain types are
isomorphic, and a collection of \emph{optimizations of programs}
$\alpha$, $\beta$, etc. asserting that certain programs are
equivalent. The collection of optimizations has no additional
structure in the sense that different ways of optimizing the same
program are not distinguished.

In the following we present, in detail, the syntax and semantics of
one such programming language~$\Pi$ which is built over the finite
types, i.e., the empty type (0), the unit type (1), the sum type
($+$), and the product type ($\times$). The original motivation for
the design of this language was to embrace the physical principle of
``conservation of information.''  This principle led to a first level
of reversible programs witnessing type
isomorphisms~\cite{James:2012:IE:2103656.2103667,rc2011,rc2012,theseus};
then the second level of reversible optimizations was developed by
appealing to a denotational semantics in symmetric rig
groupoids~\cite{DBLP:conf/esop/CaretteS16}; and finally the connection
to univalent universes was established after the
fact~\cite{CARETTE20185}.

%%%
\subsection{Types and Level-1 Combinators}

The following Agda code shows the definitions of the universe of
finite types $\AgdaDatatype{𝕌}$ and the full collection of level-1
reversible programs. A program $c : t_1 ↔ t_2$ denotes a type
isomorphism between the denotation of~$t_1$ and the denotation of
$t_2$. The collection of level-1 programs forms a \emph{commutative
  semiring} that is sound and complete with respect to type
isomorphisms~\cite{fiore-remarks}; programmatically the collection of
level-1 programs is complete for expressing combinational
circuits.

\medskip

\begin{AgdaMultiCode}
\begin{code}[hide]
--{-# OPTIONS --allow-unsolved-metas #-}
module SuperdenseCoding where
open import Data.Empty
open import Data.Unit
open import Data.Sum
open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality


module Pi where

  infix  70 _⊗_
  infix  60 _⊕_
  infix  40 _↔_
  infix  30 _⇔_
  infixr 50 _◎_
  infixr 50 _⊡_

  mutual
\end{code}
\begin{code}
    data 𝕌 : Set where
      𝟘     : 𝕌
      𝟙     : 𝕌
      _⊕_  : 𝕌 → 𝕌 → 𝕌
      _⊗_  : 𝕌 → 𝕌 → 𝕌

    data _↔_ : 𝕌 → 𝕌 → Set where
      -- additive monoid
      unite₊l   : {t : 𝕌} → 𝟘 ⊕ t ↔ t
      uniti₊l   : {t : 𝕌} → t ↔ 𝟘 ⊕ t
      unite₊r   : {t : 𝕌} → t ⊕ 𝟘 ↔ t
      uniti₊r   : {t : 𝕌} → t ↔ t ⊕ 𝟘
      swap₊     : {t₁ t₂ : 𝕌} → t₁ ⊕ t₂ ↔ t₂ ⊕ t₁
      assocl₊   : {t₁ t₂ t₃ : 𝕌} → t₁ ⊕ (t₂ ⊕ t₃) ↔ (t₁ ⊕ t₂) ⊕ t₃
      assocr₊   : {t₁ t₂ t₃ : 𝕌} → (t₁ ⊕ t₂) ⊕ t₃ ↔ t₁ ⊕ (t₂ ⊕ t₃)
      -- multiplicative monoid
      unite⋆l   : {t : 𝕌} → 𝟙 ⊗ t ↔ t
      uniti⋆l   : {t : 𝕌} → t ↔ 𝟙 ⊗ t
      unite⋆r   : {t : 𝕌} → t ⊗ 𝟙 ↔ t
      uniti⋆r   : {t : 𝕌} → t ↔ t ⊗ 𝟙
      swap⋆     : {t₁ t₂ : 𝕌} → t₁ ⊗ t₂ ↔ t₂ ⊗ t₁
      assocl⋆   : {t₁ t₂ t₃ : 𝕌} → t₁ ⊗ (t₂ ⊗ t₃) ↔ (t₁ ⊗ t₂) ⊗ t₃
      assocr⋆   : {t₁ t₂ t₃ : 𝕌} → (t₁ ⊗ t₂) ⊗ t₃ ↔ t₁ ⊗ (t₂ ⊗ t₃)
     -- distributivity
      absorbr   : {t : 𝕌} → 𝟘 ⊗ t ↔ 𝟘
      absorbl   : {t : 𝕌} → t ⊗ 𝟘 ↔ 𝟘
      factorzr  : {t : 𝕌} → 𝟘 ↔ t ⊗ 𝟘
      factorzl  : {t : 𝕌} → 𝟘 ↔ 𝟘 ⊗ t
      dist      : {t₁ t₂ t₃ : 𝕌} → (t₁ ⊕ t₂) ⊗ t₃ ↔ (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃)
      factor    : {t₁ t₂ t₃ : 𝕌} → (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃) ↔ (t₁ ⊕ t₂) ⊗ t₃
      distl     : {t₁ t₂ t₃ : 𝕌} → t₁ ⊗ (t₂ ⊕ t₃) ↔ (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃)
      factorl   : {t₁ t₂ t₃ : 𝕌 } → (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃) ↔ t₁ ⊗ (t₂ ⊕ t₃)
     -- identity and compositions
      id↔       : {t : 𝕌} → t ↔ t 
      _◎_       : {t₁ t₂ t₃ : 𝕌} → (t₁ ↔ t₂) → (t₂ ↔ t₃) → (t₁ ↔ t₃)
      _⊕_       : {t₁ t₂ t₃ t₄ : 𝕌} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊕ t₂ ↔ t₃ ⊕ t₄)
      _⊗_       : {t₁ t₂ t₃ t₄ : 𝕌} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊗ t₂ ↔ t₃ ⊗ t₄)
\end{code}
\end{AgdaMultiCode}
\medskip

We provide a few examples to gain some familiarity with programming in
$\Pi$. The examples assume a representation of the type of booleans
\bt as the disjoint union \ot \plusu \ot with the left injection
representing~$\fc$ written as \AgdaFunction{0₂} and the right
injection representing $\tc$ written as \AgdaFunction{1₂}. Given an
arbitrary reversible function \AgdaFunction{f} of type \AgdaBound{a}
\iso \AgdaBound{a}, we can build the reversible function
\AgdaFunction{controlled}~\AgdaFunction{f} that takes a pair of type
\bt~\AgdaOperator{⊗} \AgdaBound{a} and checks the incoming boolean;
if it is \AgdaFunction{0₂} (i.e., we are in the left injection), the
function behaves like the identity; otherwise the function applies
\AgdaFunction{f} to the second argument. The incoming boolean is then
reconstituted to maintain reversibility:

\medskip
\begin{center}
\begin{code}
  𝟚 : 𝕌
  𝟚 = 𝟙 ⊕ 𝟙

  controlled : ∀ {a} → (a ↔ a) → (𝟚 ⊗ a ↔ 𝟚 ⊗ a)
  controlled f = dist
               ◎ (id↔ ⊕ (id↔ ⊗ f))
               ◎ factor
\end{code}
\end{center}
{\small
\[\def\arraystretch{1.2}\begin{array}{lcll}

\AgdaFunction{controlled}  &:& \forall a.~ (a \iso a) \quad\rightarrow
                            & (\bt~\timeu a \iso ~\bt~\timeu a) \\
\AgdaFunction{controlled}~\AgdaFunction{f} &=&

  \bt ~\timeu a
    & \byiso{\idc} \\
&& (\ot \plusu \ot) \timeu a
    & \byiso{\AgdaInductiveConstructor{dist}} \\
&& (\ot~\timeu a) \plusu (\ot~\timeu a)
    & \byiso{\idc ~\oplusu (\idc \otimeu \AgdaFunction{f})} \\
&& (\ot~\timeu a) \plusu  (\ot \timeu a)
    & \byiso{\AgdaInductiveConstructor{factor}} \\
&& (\ot~\plusu \ot) \timeu a
    & \byiso{\idc} \\
&& \bt~\timeu a 
\end{array}
\]}

We show the code in two different styles: the bottom version uses the
``equational-reasoning style.'' In that style, the left column shows
the sequence of types that are visited during the computation; the
right column shows the names of the combinators that witness the
corresponding type isomorphism. Alternatively, one can directly use
the point-free combinator-style and keep the sequence of types that
are visited implicit as shown in the top version. Both styles are
equivalent as $\byiso{-}$ is defined in terms of the sequential
composition combinator \compose. The implementation of
\AgdaFunction{controlled}~\AgdaFunction{f} provides constructive
evidence (i.e., a program, a logic gate, or a hardware circuit) for an
automorphism on \bt \timeu \AgdaBound{a}: it can be read top-down or
bottom-up to go back and forth.

Continuing with the examples, the \AgdaFunction{not} function below is
just another name for \swapp\  which swaps the left and right injections
of a sum type. Using the \AgdaFunction{controlled} building block, we
can build a controlled-not (\AgdaFunction{cnot}) gate and a
controlled-controlled-not gate, also known as the
\AgdaFunction{toffoli} gate. The latter gate is a universal function
for combinational boolean
circuits~\cite{Toffoli:1980,fredkin1982conservative} thus showing the
expressiveness of the language:

%% {\small
%% \[\begin{array}{rcl}
%% \AgdaFunction{not} &:& \bt \iso \bt \\
%% \AgdaFunction{not} &=& \AgdaFunction{swap₊} \\
%% \\
%% \AgdaFunction{cnot} &:& \bt ~⊗ \bt \iso \bt ~⊗ \bt \\
%% \AgdaFunction{cnot} &=& \AgdaFunction{controlled}~\AgdaFunction{not} \\
%% \\
%% \AgdaFunction{toffoli} &:& \bt ~⊗ (\bt ~⊗ \bt)
%%                            \iso \bt ~⊗ (\bt ~⊗ \bt) \\
%% \AgdaFunction{toffoli} &=& \AgdaFunction{controlled}~\AgdaFunction{cnot} \\
%% \end{array}\]}

\medskip
\begin{code}
  not : 𝟚 ↔ 𝟚
  not = swap₊

  cnot : 𝟚 ⊗ 𝟚 ↔ 𝟚 ⊗ 𝟚
  cnot = controlled not

  toffoli : 𝟚 ⊗ (𝟚 ⊗ 𝟚) ↔ 𝟚 ⊗ (𝟚 ⊗ 𝟚)
  toffoli = controlled cnot
\end{code}
\medskip

%%%
\subsection{Semantics and Properties}

By design, the level-1 combinators model isomorphisms and hence each
combinator $c$ has an inverse $! c$ defined in the obvious way. We
show a few of the cases of the definition:

\begin{AgdaMultiCode}
\begin{code}
  ! : ∀ {t₁ t₂} → (t₁ ↔ t₂) → (t₂ ↔ t₁)
  ! swap₊ = swap₊
  ! swap⋆ = swap⋆
  ! (c₁ ◎ c₂) = ! c₂ ◎ ! c₁  -- note reverse order
  ! (c₁ ⊕ c₂) = ! c₁ ⊕ ! c₂
  ! (c₁ ⊗ c₂) = ! c₁ ⊗ ! c₂
  -- ...
\end{code}
\begin{code}[hide]
  ! unite₊l = uniti₊l
  ! uniti₊l = unite₊l
  ! unite₊r = uniti₊r
  ! uniti₊r = unite₊r
  ! assocl₊ = assocr₊
  ! assocr₊ = assocl₊
  ! unite⋆l = uniti⋆l
  ! uniti⋆l = unite⋆l
  ! unite⋆r = uniti⋆r
  ! uniti⋆r = unite⋆r
  ! assocl⋆ = assocr⋆
  ! assocr⋆ = assocl⋆
  ! absorbr = factorzl
  ! absorbl = factorzr
  ! factorzr = absorbl
  ! factorzl = absorbr
  ! dist = factor
  ! factor = dist
  ! distl = factorl
  ! factorl = distl
  ! id↔ = id↔

  !²≡ : {t₁ t₂ : 𝕌} → (c : t₁ ↔ t₂) → ! (! c) ≡ c
  !²≡ unite₊l = refl
  !²≡ uniti₊l = refl
  !²≡ unite₊r = refl
  !²≡ uniti₊r = refl
  !²≡ swap₊ = refl
  !²≡ assocl₊ = refl
  !²≡ assocr₊ = refl
  !²≡ unite⋆l = refl
  !²≡ uniti⋆l = refl
  !²≡ unite⋆r = refl
  !²≡ uniti⋆r = refl
  !²≡ swap⋆ = refl
  !²≡ assocl⋆ = refl
  !²≡ assocr⋆ = refl
  !²≡ absorbr = refl
  !²≡ absorbl = refl
  !²≡ factorzr = refl
  !²≡ factorzl = refl
  !²≡ dist = refl
  !²≡ factor = refl
  !²≡ distl = refl
  !²≡ factorl = refl
  !²≡ id↔ = refl
  !²≡ (c₁ ◎ c₂) rewrite (!²≡ c₁) | (!²≡ c₂) = refl
  !²≡ (c₁ ⊕ c₂) rewrite (!²≡ c₁) | (!²≡ c₂) = refl
  !²≡ (c₁ ⊗ c₂) rewrite (!²≡ c₁) | (!²≡ c₂) = refl
\end{code}

The language can be given several denotational semantics where a type
denotes a finite set and combinators denote permutations, or where a
type denotes wires and combinators denote circuits, or where a type
denotes a topological space and combinators denote continuous
deformations. More generally the semantics can be given in \emph{rig
  categories} (also known as \emph{bimonoidal
  categories})~\cite{kelly74,laplaza}. Here, we give a simple
operational semantics in Agda. Each $\Pi$-type denotes an Agda type;
the operational semantics takes a combinator $c : t_1 \iso t_2$ and a
value of the denotation of~$t_1$ and produces a value in the
denotation of $t_2$. The semantics can also be run backwards and the
properties of the language ensure that the evaluation relations are
inverses:

\medskip
\begin{code}
  ⟦_⟧ : 𝕌 → Set
  ⟦ 𝟘 ⟧ = ⊥
  ⟦ 𝟙 ⟧ = ⊤
  ⟦ t₁ ⊕ t₂ ⟧ = ⟦ t₁ ⟧ ⊎ ⟦ t₂ ⟧
  ⟦ t₁ ⊗ t₂ ⟧ = ⟦ t₁ ⟧ × ⟦ t₂ ⟧

  interp : ∀ {t₁ t₂} → (t₁ ↔ t₂) → ⟦ t₁ ⟧ → ⟦ t₂ ⟧
  interp swap₊ (inj₁ v) = inj₂ v
  interp swap₊ (inj₂ v) = inj₁ v
  interp swap⋆ (v₁ , v₂) = v₂ , v₁
  interp id↔ v = v
  interp (c₁ ◎ c₂) v = interp c₂ (interp c₁ v)
  interp (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interp c₁ v)
  interp (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interp c₂ v)
  interp (c₁ ⊗ c₂) (v₁ , v₂) = interp c₁ v₁ , interp c₂ v₂
  interp unite₊l (inj₁ ())
  interp unite₊l (inj₂ v) = v
  interp uniti₊l v = inj₂ v
  interp unite₊r (inj₁ v) = v
  interp unite₊r (inj₂ ())
  interp uniti₊r v = inj₁ v
  interp assocl₊ (inj₁ v) = inj₁ (inj₁ v)
  interp assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
  interp assocl₊ (inj₂ (inj₂ v)) = inj₂ v
  interp assocr₊ (inj₁ (inj₁ v)) = inj₁ v
  interp assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
  interp assocr₊ (inj₂ v) = inj₂ (inj₂ v)
  interp unite⋆l v = proj₂ v
  interp uniti⋆l v = tt , v
  interp unite⋆r v = proj₁ v
  interp uniti⋆r v = v , tt
  interp assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
  interp assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
  interp absorbr (() , v)
  interp absorbl (v , ())
  interp factorzr ()
  interp factorzl ()
  interp dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
  interp dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
  interp factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
  interp factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
  interp distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
  interp distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
  interp factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
  interp factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
\end{code}
\medskip

%%%
\subsection{Level-2 Combinators}

As is customary in any semantic perspective on programming languages,
one of the questions of interest is when two programs can be
considered ``equivalent.'' Consider the following six programs of type
\bt \iso \bt:

%% {\small
%% \[\def\arraystretch{1.2}\begin{array}{rcl}
%% \AgdaFunction{id₁}~\AgdaFunction{id₂}~\AgdaFunction{id₃}~
%%   \AgdaFunction{not₁}~\AgdaFunction{not₂}~\AgdaFunction{not₃} &:& \bt \iso \bt \\
%% \AgdaFunction{id₁} &=&
%%   \AgdaFunction{id} \odot_1 \AgdaFunction{id} \\
%% \AgdaFunction{id₂} &=&
%%   \AgdaFunction{not} \odot_1 \AgdaFunction{id} \odot_1 \AgdaFunction{not} \\
%% \AgdaFunction{id₃} &=&
%%   \AgdaFunction{uniti⋆} \odot_1 \AgdaFunction{swap⋆} \odot_1
%%                         (\AgdaFunction{id} \otimes \AgdaFunction{id}) \odot_1
%%                         \AgdaFunction{swap⋆} \odot_1
%%                         \AgdaFunction{unite⋆} \\
%% \AgdaFunction{not₁} &=&
%%   \AgdaFunction{id} \odot_1 \AgdaFunction{not} \\
%% \AgdaFunction{not₂} &=&
%%   \AgdaFunction{not} \odot_1 \AgdaFunction{not} \odot_1 \AgdaFunction{not} \\
%% \AgdaFunction{not₃} &=&
%%   \AgdaFunction{uniti⋆} \odot_1 \AgdaFunction{swap⋆} \odot_1
%%                         (\AgdaFunction{not} \otimes \AgdaFunction{id}) \odot_1
%%                         \AgdaFunction{swap⋆} \odot_1
%%                         \AgdaFunction{unite⋆}
%% \end{array}\]}

\medskip
\begin{code}
  id₁ id₂ id₃ not₁ not₂ not₃ : 𝟚 ↔ 𝟚
  id₁  = id↔ ◎ id↔
  id₂  = not ◎ id↔ ◎ not
  id₃  = uniti⋆l ◎ swap⋆ ◎ (id↔ ⊗ id↔) ◎ swap⋆ ◎ unite⋆l
  not₁ = id↔ ◎ not
  not₂ = not ◎ not ◎ not
  not₃ = uniti⋆l ◎ swap⋆ ◎ (not ⊗ id↔) ◎ swap⋆ ◎ unite⋆l
\end{code}
\medskip

The programs are all of the same type but this is clearly not a
sufficient condition for ``equivalence.'' Thinking extensionally,
i.e., by looking at all possible input-output pairs mapped by
\AgdaFunction{interp}, it is easy to verify that the six programs
split into two classes: one consisting of the first three programs
which are all equivalent to the identity function and the other
consisting of the remaining three programs which all equivalent to
boolean negation.

Extensional reasoning is less than ideal in this situation, however:
converting combinators to general functions and reasoning about their
equality happens outside the programming language and is
undecidable. In the case of $\Pi$ the denotational semantics in rig
categories and the connection to univalent universes both provide a
much more elegant approach for reasoning about equivalence of level-1
programs. In the categorical semantics, Laplaza provided an
internalization of the extensional equality of morphisms using a
(large) collection of \emph{coherence
  conditions}~\cite{laplaza}. These conditions can be turned into an
inductive definition for a second level of combinators that provides a
sound and complete system for reasoning about level-1 program
equivalence~\cite{DBLP:conf/esop/CaretteS16}. Similarly from the
connection to univalent universes, we know that the level-1 programs
correspond to 1-paths, that the equality of 1-paths is given by
higher-level 2-paths (up to level-3 equivalence which is trivial), and
that the 2-paths correspond exactly to Laplaza's coherence
conditions~\cite{CARETTE20185}.

Putting these ideas together, we can define a second level of
isomorphisms $\isotwo$ that relates level-1 programs. As alluded to
earlier, this collection is huge so we only include the constructors
actually used in this paper and refer the reader a recent paper by
Carette et al. for the complete list~\cite{DBLP:conf/esop/CaretteS16}.

\medskip
\begin{code}
  data _⇔_ :  {t₁ t₂ : 𝕌} → (t₁ ↔ t₂) → (t₁ ↔ t₂) → Set where
      assoc◎l    : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄} 
                 → (c₁ ◎ (c₂ ◎ c₃)) ⇔ ((c₁ ◎ c₂) ◎ c₃)
      assoc◎r    : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄} 
                 → ((c₁ ◎ c₂) ◎ c₃) ⇔ (c₁ ◎ (c₂ ◎ c₃))
      swapl⋆⇔    : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
                 → (swap⋆ ◎ (c₁ ⊗ c₂)) ⇔ ((c₂ ⊗ c₁) ◎ swap⋆)
      swapr⋆⇔    : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
                 → ((c₂ ⊗ c₁) ◎ swap⋆) ⇔ (swap⋆ ◎ (c₁ ⊗ c₂))
      swapl₊⇔    : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} 
                 → (swap₊ ◎ (c₁ ⊕ c₂)) ⇔ ((c₂ ⊕ c₁) ◎ swap₊)
      swapr₊⇔    : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
                 → ((c₂ ⊕ c₁) ◎ swap₊) ⇔ (swap₊ ◎ (c₁ ⊕ c₂))
      linv◎l     : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → (c ◎ ! c) ⇔ id↔
      linv◎r     : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → id↔ ⇔ (c ◎ ! c)
      rinv◎l  : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → (! c ◎ c) ⇔ id↔
      rinv◎r  : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → id↔ ⇔ (! c ◎ c) 
      idl◎l      : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → (id↔ ◎ c) ⇔ c
      idr◎l      : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → (c ◎ id↔) ⇔ c
      idl◎r      : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → c ⇔ id↔ ◎ c
      idr◎r      : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → c ⇔ (c ◎ id↔) 
      unitil⋆⇔l  :  {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂} 
                 →  (uniti⋆l ◎ (c₁ ⊗ c₂)) ⇔ (c₂ ◎ uniti⋆l)
      unitel⋆⇔l  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → (unite⋆l ◎ c₂) ⇔ ((c₁ ⊗ c₂) ◎ unite⋆l)
      uniter⋆⇔l  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → ((c₁ ⊗ c₂) ◎ unite⋆l) ⇔ (unite⋆l ◎ c₂)
      unitir⋆⇔l  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → (c₂ ◎ uniti⋆l) ⇔ (uniti⋆l ◎ (c₁ ⊗ c₂))
      unitel⋆⇔r  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → (unite⋆r ◎ c₂) ⇔ ((c₂ ⊗ c₁) ◎ unite⋆r)
      uniter⋆⇔r  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → ((c₂ ⊗ c₁) ◎ unite⋆r) ⇔ (unite⋆r ◎ c₂)
      unitil⋆⇔r  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → (uniti⋆r ◎ (c₂ ⊗ c₁)) ⇔ (c₂ ◎ uniti⋆r)
      unitir⋆⇔r  : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                 → (c₂ ◎ uniti⋆r) ⇔ (uniti⋆r ◎ (c₂ ⊗ c₁))
      _⊡_        : {t₁ t₂ t₃ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₁ ↔ t₂} {c₄ : t₂ ↔ t₃} 
                 → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ◎ c₂) ⇔ (c₃ ◎ c₄)
      trans⇔     : {t₁ t₂ : 𝕌} {c₁ c₂ c₃ : t₁ ↔ t₂}
                 → (c₁ ⇔ c₂) → (c₂ ⇔ c₃) → (c₁ ⇔ c₃)
      sym⇔       : {t₁ t₂ : 𝕌} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → c₂ ⇔ c₁
      id⇔        : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → c ⇔ c
     -- ...
\end{code}
\begin{code}[hide]
      assocl⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊) ⇔ (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃))
      assocl⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃)) ⇔ ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊)
      assocl⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆) ⇔ (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃))
      assocl⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃)) ⇔ ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆)
      assocr⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊) ⇔ (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃)))
      assocr⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃))) ⇔ (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆)
      assocr⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆) ⇔ (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃)))
      assocr⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃))) ⇔ (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊)
      dist⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → ((a ⊕ b) ⊗ c) ◎ dist ⇔ dist ◎ ((a ⊗ c) ⊕ (b ⊗ c))
      dist⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → dist ◎ ((a ⊗ c) ⊕ (b ⊗ c)) ⇔ ((a ⊕ b) ⊗ c) ◎ dist
      distl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → (a ⊗ (b ⊕ c)) ◎ distl ⇔ distl ◎ ((a ⊗ b) ⊕ (a ⊗ c))
      distl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → distl ◎ ((a ⊗ b) ⊕ (a ⊗ c)) ⇔ (a ⊗ (b ⊕ c)) ◎ distl
      factor⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor ⇔ factor ◎ ((a ⊕ b) ⊗ c)
      factor⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → factor ◎ ((a ⊕ b) ⊗ c) ⇔ ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor
      factorl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl ⇔ factorl ◎ (a ⊗ (b ⊕ c))
      factorl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → factorl ◎ (a ⊗ (b ⊕ c)) ⇔ ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl
      unite₊l⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊l ◎ c₂) ⇔ ((c₁ ⊕ c₂) ◎ unite₊l)
      unite₊l⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊕ c₂) ◎ unite₊l) ⇔ (unite₊l ◎ c₂)
      uniti₊l⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊l ◎ (c₁ ⊕ c₂)) ⇔ (c₂ ◎ uniti₊l)
      uniti₊l⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊l) ⇔ (uniti₊l ◎ (c₁ ⊕ c₂))
      unite₊r⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊r ◎ c₂) ⇔ ((c₂ ⊕ c₁) ◎ unite₊r)
      unite₊r⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊕ c₁) ◎ unite₊r) ⇔ (unite₊r ◎ c₂)
      uniti₊r⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊r ◎ (c₂ ⊕ c₁)) ⇔ (c₂ ◎ uniti₊r)
      uniti₊r⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊r) ⇔ (uniti₊r ◎ (c₂ ⊕ c₁))
      resp⊕⇔  : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊕ c₂) ⇔ (c₃ ⊕ c₄)
      resp⊗⇔  : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊗ c₂) ⇔ (c₃ ⊗ c₄)
    -- below are the combinators added for the RigCategory structure
      id↔⊕id↔⇔ : {t₁ t₂ : 𝕌} → (id↔ {t₁} ⊕ id↔ {t₂}) ⇔ id↔
      split⊕-id↔ : {t₁ t₂ : 𝕌} → (id↔ {t₁ ⊕ t₂}) ⇔ (id↔ ⊕ id↔)
      hom⊕◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄)) ⇔ ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄))
      hom◎⊕⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄)) ⇔ ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄))
      id↔⊗id↔⇔ : {t₁ t₂ : 𝕌} → (id↔ {t₁} ⊗ id↔ {t₂}) ⇔ id↔
      split⊗-id↔ : {t₁ t₂ : 𝕌} → (id↔ {t₁ ⊗ t₂}) ⇔ (id↔ ⊗ id↔)
      hom⊗◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄)) ⇔ ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄))
      hom◎⊗⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄)) ⇔ ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄))
      -- associativity triangle
      triangle⊕l : {t₁ t₂ : 𝕌} → (unite₊r {t₁} ⊕ id↔ {t₂}) ⇔ assocr₊ ◎ (id↔ ⊕ unite₊l)
      triangle⊕r : {t₁ t₂ : 𝕌} → assocr₊ ◎ (id↔ {t₁} ⊕ unite₊l {t₂}) ⇔ (unite₊r ⊕ id↔)
      triangle⊗l : {t₁ t₂ : 𝕌} → ((unite⋆r {t₁}) ⊗ id↔ {t₂}) ⇔ assocr⋆ ◎ (id↔ ⊗ unite⋆l)
      triangle⊗r : {t₁ t₂ : 𝕌} → (assocr⋆ ◎ (id↔ {t₁} ⊗ unite⋆l {t₂})) ⇔ (unite⋆r ⊗ id↔)
      pentagon⊕l : {t₁ t₂ t₃ t₄ : 𝕌}
                 → assocr₊ ◎ (assocr₊ {t₁} {t₂} {t₃ ⊕ t₄})
                 ⇔ ((assocr₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
      pentagon⊕r : {t₁ t₂ t₃ t₄ : 𝕌}
                 → ((assocr₊ {t₁} {t₂} {t₃} ⊕ id↔ {t₄}) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
                 ⇔ assocr₊ ◎ assocr₊
      pentagon⊗l : {t₁ t₂ t₃ t₄ : 𝕌}
                 → assocr⋆ ◎ (assocr⋆ {t₁} {t₂} {t₃ ⊗ t₄})
                 ⇔ ((assocr⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
      pentagon⊗r : {t₁ t₂ t₃ t₄ : 𝕌}
                 → ((assocr⋆ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
                 ⇔ assocr⋆ ◎ assocr⋆
      -- from the braiding
      -- unit coherence
      unite₊l-coh-l : {t₁ : 𝕌} → unite₊l {t₁} ⇔ swap₊ ◎ unite₊r
      unite₊l-coh-r : {t₁ : 𝕌} → swap₊ ◎ unite₊r ⇔ unite₊l {t₁}
      unite⋆l-coh-l : {t₁ : 𝕌} → unite⋆l {t₁} ⇔ swap⋆ ◎ unite⋆r
      unite⋆l-coh-r : {t₁ : 𝕌} → swap⋆ ◎ unite⋆r ⇔ unite⋆l {t₁}
      hexagonr⊕l : {t₁ t₂ t₃ : 𝕌}
                 → (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
                 ⇔ ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
      hexagonr⊕r : {t₁ t₂ t₃ : 𝕌}
                 → ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
                 ⇔ (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
      hexagonl⊕l : {t₁ t₂ t₃ : 𝕌}
                 → (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
      hexagonl⊕r : {t₁ t₂ t₃ : 𝕌}
                 → ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
                 ⇔ (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
      hexagonr⊗l : {t₁ t₂ t₃ : 𝕌}
                 → (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
                 ⇔ ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
      hexagonr⊗r : {t₁ t₂ t₃ : 𝕌}
                 → ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
                 ⇔ (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
      hexagonl⊗l : {t₁ t₂ t₃ : 𝕌}
                 → (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
      hexagonl⊗r : {t₁ t₂ t₃ : 𝕌}
                 → ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
                 ⇔ (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
      absorbl⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                → (c₁ ⊗ id↔ {𝟘}) ◎ absorbl ⇔ absorbl ◎ id↔ {𝟘}
      absorbl⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                → absorbl ◎ id↔ {𝟘} ⇔ (c₁ ⊗ id↔ {𝟘}) ◎ absorbl
      absorbr⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                → (id↔ {𝟘} ⊗ c₁) ◎ absorbr ⇔ absorbr ◎ id↔ {𝟘}
      absorbr⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                → absorbr ◎ id↔ {𝟘} ⇔ (id↔ {𝟘} ⊗ c₁) ◎ absorbr
      factorzl⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzl ⇔ factorzl ◎ (id↔ ⊗ c₁)
      factorzl⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                 → factorzl ◎ (id↔ {𝟘} ⊗ c₁) ⇔ id↔ {𝟘} ◎ factorzl
      factorzr⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzr ⇔ factorzr ◎ (c₁ ⊗ id↔)
      factorzr⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ↔ t₂}
                 → factorzr ◎ (c₁ ⊗ id↔) ⇔ id↔ ◎ factorzr
      -- from the coherence conditions of RigCategory
      swap₊distl⇔l : {t₁ t₂ t₃ : 𝕌}
                   → (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl ⇔ distl ◎ swap₊
      swap₊distl⇔r : {t₁ t₂ t₃ : 𝕌}
                   → distl ◎ swap₊ ⇔ (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl
      dist-swap⋆⇔l : {t₁ t₂ t₃ : 𝕌}
                   → dist {t₁} {t₂} {t₃} ◎ (swap⋆ ⊕ swap⋆) ⇔ swap⋆ ◎ distl
      dist-swap⋆⇔r : {t₁ t₂ t₃ : 𝕌}
                   → swap⋆ ◎ distl {t₁} {t₂} {t₃} ⇔ dist ◎ (swap⋆ ⊕ swap⋆)
      assocl₊-dist-dist⇔l : {t₁ t₂ t₃ t₄ : 𝕌}
                          → ((assocl₊ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ dist) ◎ (dist ⊕ id↔)
                          ⇔ (dist ◎ (id↔ ⊕ dist)) ◎ assocl₊
      assocl₊-dist-dist⇔r : {t₁ t₂ t₃ t₄ : 𝕌}
                          → (dist {t₁} ◎ (id↔ ⊕ dist {t₂} {t₃} {t₄})) ◎ assocl₊
                          ⇔ ((assocl₊ ⊗ id↔) ◎ dist) ◎ (dist ⊕ id↔)
      assocl⋆-distl⇔l : {t₁ t₂ t₃ t₄ : 𝕌}
                      → assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}
                      ⇔ ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
      assocl⋆-distl⇔r : {t₁ t₂ t₃ t₄ : 𝕌}
                      → ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
                      ⇔ assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}  
      absorbr0-absorbl0⇔ : absorbr {𝟘} ⇔ absorbl {𝟘}
      absorbl0-absorbr0⇔ : absorbl {𝟘} ⇔ absorbr {𝟘}
      absorbr⇔distl-absorb-unite : {t₁ t₂ : 𝕌}
                                 → absorbr ⇔ (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l
      distl-absorb-unite⇔absorbr : {t₁ t₂ : 𝕌}
                                 → (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l ⇔ absorbr
      unite⋆r0-absorbr1⇔ : unite⋆r ⇔ absorbr
      absorbr1-unite⋆r-⇔ : absorbr ⇔ unite⋆r
      absorbl≡swap⋆◎absorbr : {t₁ : 𝕌} → absorbl {t₁} ⇔ swap⋆ ◎ absorbr
      swap⋆◎absorbr≡absorbl : {t₁ : 𝕌} → swap⋆ ◎ absorbr ⇔ absorbl {t₁}
      absorbr⇔[assocl⋆◎[absorbr⊗id↔]]◎absorbr : {t₁ t₂ : 𝕌}
                                              → absorbr ⇔ (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr
      [assocl⋆◎[absorbr⊗id↔]]◎absorbr⇔absorbr : {t₁ t₂ : 𝕌}
                                              → (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr ⇔ absorbr
      [id↔⊗absorbr]◎absorbl⇔assocl⋆◎[absorbl⊗id↔]◎absorbr : {t₁ t₂ : 𝕌}
                                                          → (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
                                                          ⇔ (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
      assocl⋆◎[absorbl⊗id↔]◎absorbr⇔[id↔⊗absorbr]◎absorbl : {t₁ t₂ : 𝕌}
                                                          → (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
                                                          ⇔ (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
      elim⊥-A[0⊕B]⇔l : {t₁ t₂ : 𝕌}
                     → (id↔ {t₁} ⊗ unite₊l {t₂})
                     ⇔ (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
      elim⊥-A[0⊕B]⇔r : {t₁ t₂ : 𝕌}
                     → (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
                     ⇔ (id↔ {t₁} ⊗ unite₊l {t₂})
      elim⊥-1[A⊕B]⇔l : {t₁ t₂ : 𝕌}
                     → unite⋆l ⇔ distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂})
      elim⊥-1[A⊕B]⇔r : {t₁ t₂ : 𝕌} → distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂}) ⇔ unite⋆l
      fully-distribute⇔l : {t₁ t₂ t₃ t₄ : 𝕌}
                         → (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
                         ⇔ ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
      fully-distribute⇔r : {t₁ t₂ t₃ t₄ : 𝕌}
                         → ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
                         ⇔ (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
      -- make things easier
      cong! : {t₁ t₂ : 𝕌} {c₁ c₂ : t₁ ↔ t₂} → (c₁ ⇔ c₂) → (! c₁ ⇔ ! c₂)

  notOpt : not₃  ⇔  not
  notOpt = trans⇔ (id⇔ ⊡ assoc◎l) 
           (trans⇔ (id⇔ ⊡ swapl⋆⇔ ⊡ id⇔)
           (trans⇔ (id⇔ ⊡ assoc◎r) 
           (trans⇔ (id⇔ ⊡ id⇔ ⊡ assoc◎l) 
           (trans⇔ (id⇔ ⊡ id⇔ ⊡ linv◎l ⊡ id⇔) 
           (trans⇔ (id⇔ ⊡ id⇔ ⊡ idl◎l) 
           (trans⇔ assoc◎l 
           (trans⇔ (unitil⋆⇔l ⊡ id⇔) 
           (trans⇔ assoc◎r 
           (trans⇔ (id⇔ ⊡ linv◎l) idr◎l)))))))))
\end{code}
\end{AgdaMultiCode}

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[scale=0.9,every node/.style={scale=0.9}]
  \draw (1,2) ellipse (0.5cm and 0.5cm);
  \draw[fill] (1,2) circle [radius=0.025];
  \node[below] at (1,2) {};

  \draw (0,0) ellipse (0.5cm and 1cm);
  \draw[fill] (0,0.5) circle [radius=0.025];
  \node[below] at (0,0.5) {\AgdaFunction{0₂}};
  \draw[fill] (0,-0.5) circle [radius=0.025];
  \node[below] at (0,-0.5) {\AgdaFunction{1₂}};

  \draw     (1,2)    -- (2,2)      ; %% ()
  \draw     (0,0.5)  -- (2,0.5)    ; %% F
  \draw     (0,-0.5) -- (2,-0.5)   ; %% T

  \draw     (2,2)    -- (3,-0.5)   ;
  \draw     (2,0.5)  -- (3,2)      ;
  \draw     (2,-0.5) -- (3,1)      ;

  \draw     (3,2)    -- (3.5,2)    ;
  \draw     (3,1)    -- (3.5,1)    ;
  \draw     (3,-0.5) -- (3.5,-0.5) ;

  \draw     (3.5,2)    -- (4.5,1)    ;
  \draw     (3.5,1)    -- (4.5,2)    ;
  \draw     (3.5,-0.5) -- (4.5,-0.5) ;

  \draw     (4.5,2)    -- (5,2)    ;
  \draw     (4.5,1)    -- (5,1)    ;
  \draw     (4.5,-0.5) -- (5,-0.5) ;

  \draw     (5,2)    -- (6,0.5)  ;
  \draw     (5,1)    -- (6,-0.5) ;
  \draw     (5,-0.5) -- (6,2)    ;

  \draw     (6,2)    -- (7,2)    ;
  \draw     (6,0.5)  -- (8,0.5)  ;
  \draw     (6,-0.5) -- (8,-0.5) ;

  \draw (7,2) ellipse (0.5cm and 0.5cm);
  \draw[fill] (7,2) circle [radius=0.025];
  \node[below] at (7,2) {};

  \draw (8,0) ellipse (0.5cm and 1cm);
  \draw[fill] (8,0.5) circle [radius=0.025];
  \node[below] at (8,0.5) {\AgdaFunction{0₂}};
  \draw[fill] (8,-0.5) circle [radius=0.025];
  \node[below] at (8,-0.5) {\AgdaFunction{1₂}};

\end{tikzpicture}
\end{center}
\caption{\label{fig:not}Graphical representation of {\small\AgdaFunction{not₃}}}
\end{figure}

To gain some familiarity with this new level of combinators, we show
how to write a reversible level-2 program of type \isotwo\ that
manipulates lower level reversible level-1 programs of type \iso.  In
particular, we show that \AgdaFunction{not₃} is equivalent to
\AgdaFunction{not}. For illustration, the program for
\AgdaFunction{not₃} is depicted in Fig.~\ref{fig:not} and we encourage
the reader to map the steps below to manipulations on the diagram that
would incrementally simplify it:

{\small
\[\def\arraystretch{1.2}\begin{array}{rcll}
\AgdaFunction{notOpt} &:& \AgdaFunction{not₃} \isotwo \AgdaFunction{not} \\
\AgdaFunction{notOpt} &=&
  \AgdaFunction{uniti⋆} \compose (\AgdaFunction{swap⋆} \compose
                        ((\AgdaFunction{not} \otimeu \AgdaFunction{id↔}) \compose
                        (\AgdaFunction{swap⋆} \compose \AgdaFunction{unite⋆})))
 & \quad\byisotwo{\idctwo \boxdot \AgdaInductiveConstructor{assoc◎l}} \\
&& \AgdaFunction{uniti⋆} \compose (\AgdaFunction{swap⋆} \compose
                        (\AgdaFunction{not} \otimeu \AgdaFunction{id↔})) \compose
                        (\AgdaFunction{swap⋆} \compose \AgdaFunction{unite⋆})
 & \quad\byisotwo{\idctwo \boxdot (\AgdaInductiveConstructor{swapl⋆⇔}
                                  \boxdot \idctwo)} \\
&& \AgdaFunction{uniti⋆} \compose ((\AgdaFunction{id↔} \otimeu \AgdaFunction{not})
                      \compose \AgdaFunction{swap⋆}) \compose
                        (\AgdaFunction{swap⋆} \compose \AgdaFunction{unite⋆})
 & \quad\byisotwo{\idctwo \boxdot \AgdaInductiveConstructor{assoc◎r}} \\
&& \AgdaFunction{uniti⋆} \compose ((\AgdaFunction{id↔} \otimeu \AgdaFunction{not})
                      \compose (\AgdaFunction{swap⋆} \compose
                        (\AgdaFunction{swap⋆} \compose \AgdaFunction{unite⋆})))
 & \quad\byisotwo{\idctwo \boxdot (\idctwo
                                  \boxdot \AgdaInductiveConstructor{assoc◎l})} \\
&& \AgdaFunction{uniti⋆} \compose ((\AgdaFunction{id↔} \otimeu \AgdaFunction{not})
                      \compose ((\AgdaFunction{swap⋆} \compose
                      \AgdaFunction{swap⋆}) \compose \AgdaFunction{unite⋆}))
 & \quad\byisotwo{\idctwo \boxdot (\idctwo
                                  \boxdot (\AgdaInductiveConstructor{linv◎l} \boxdot \idctwo))} \\
&& \AgdaFunction{uniti⋆} \compose ((\AgdaFunction{id↔} \otimeu \AgdaFunction{not})
                      \compose (\AgdaFunction{id↔} \compose \AgdaFunction{unite⋆}))
 & \quad\byisotwo{\idctwo \boxdot (\idctwo
                                  \boxdot \AgdaInductiveConstructor{idl◎l})} \\
&& \AgdaFunction{uniti⋆} \compose ((\AgdaFunction{id↔} \otimeu \AgdaFunction{not})
                      \compose \AgdaFunction{unite⋆})
 & \quad\byisotwo{\AgdaInductiveConstructor{assoc◎l}} \\
&& (\AgdaFunction{uniti⋆} \compose (\AgdaFunction{id↔} \otimeu \AgdaFunction{not}))
                      \compose \AgdaFunction{unite⋆}
 & \quad\byisotwo{\AgdaInductiveConstructor{unitil⋆⇔l} \boxdot \idctwo} \\
&& (\AgdaFunction{not} \compose \AgdaFunction{uniti⋆}) \compose \AgdaFunction{unite⋆}
 & \quad\byisotwo{\AgdaInductiveConstructor{assoc◎r}} \\
&& \AgdaFunction{not} \compose (\AgdaFunction{uniti⋆} \compose \AgdaFunction{unite⋆})
 & \quad\byisotwo{\idctwo \boxdot \AgdaInductiveConstructor{linv◎l}} \\
&& \AgdaFunction{not} \compose \AgdaFunction{id↔}
 & \quad\byisotwo{\AgdaInductiveConstructor{idr◎l}} \\
&& \AgdaFunction{not}
\end{array}\]}

\noindent It is worthwhile mentioning that the above derivation could
also be drawn as one (large!) commutative diagram in an appropriate
category, with each $\byisotwo{-}$ as a $2$-arrow (and representing a
natural isomorphism).  See Shulman's draft book~\cite{shulman} for
that interpretation. To get a feel for that interpretation and to gain
even more intuition about this second level of combinators, let's
consider a small example consisting of two deformations between the
types $A \plusu B$ and $C \plusu D$:

\begin{center}
\begin{tikzpicture}[scale=0.4,every node/.style={scale=0.5}]
  \draw[>=latex,<->,double,red,thick] (2.25,-1.2) -- (2.25,-2.9) ;
  \draw (-2,-2) ellipse (0.9cm and 1cm);
  \draw[fill] (-2,-1.5) circle [radius=0.025];
  \node[left] at (-2.1,-1.5) {$A$};
  \draw[fill] (-2,-2.5) circle [radius=0.025];
  \node[left] at (-2.1,-2.5) {$B$};

  \draw (6.5,-2) ellipse (0.9cm and 1cm);
  \draw[fill] (6.5,-1.5) circle [radius=0.025];
  \node[right] at (6.7,-1.5) {$C$};
  \draw[fill] (6.5,-2.5) circle [radius=0.025];
  \node[right] at (6.7,-2.5) {$D$};

  \draw[-] (-2,-1.5) to[bend left] (1,0.5) ;
  \draw[-] (-2,-2.5) to[bend left] (1,-0.5) ;
  \draw[-] (3.5,0.5) to[bend left] (6.5,-1.45) ;
  \draw[-] (3.5,-0.5) to[bend left] (6.5,-2.45) ;

  \draw[-] (-2,-1.5) to[bend right] (1,-3.5) ;
  \draw[-] (-2,-2.5) to[bend right] (1,-4.5) ;
  \draw[-] (3.5,-3.5) to[bend right] (6.5,-1.55) ;
  \draw[-] (3.5,-4.5) to[bend right] (6.5,-2.55) ;

  \draw     (2.5,-3)  -- (3.5,-3) -- (3.5,-4) -- (2.5,-4) -- cycle ;
  \draw     (2.5,-4)  -- (3.5,-4) -- (3.5,-5) -- (2.5,-5) -- cycle ;

  \draw     (1,1)  -- (2,1) -- (2,0) -- (1,0) -- cycle ;
  \draw     (1,0)  -- (2,0) -- (2,-1) -- (1,-1) -- cycle ;

  \node at (3,-3.5) {$c_2$};
  \node at (3,-4.5) {$c_1$};

  \node at (1.5,0.5) {$c_1$};
  \node at (1.5,-0.5) {$c_2$};

  \draw     (2,0.5)  -- (2.5,0.5)  ;
  \draw     (2,-0.5) -- (2.5,-0.5) ;

  \draw     (2.5,0.5)  -- (3.5,-0.5)  ;
  \draw     (2.5,-0.5) -- (3.5,0.5) ;

  \draw     (1,-3.5)  -- (2,-4.5)    ;
  \draw     (1,-4.5) -- (2,-3.5)   ;

  \draw     (2,-3.5)  -- (2.5,-3.5)    ;
  \draw     (2,-4.5) -- (2.5,-4.5)   ;

\end{tikzpicture}
\end{center}

\noindent The top path is the $\Pi$ program
$(c_1~\oplus~c_2)~\odot~\swapp$ which deforms the type $A$ by $c_1$,
deforms the type~$B$ by~$c_2$, and deforms the resulting space by a
twist that exchanges the two injections into the sum type. The bottom
path performs the twist first and then deforms the type $A$ by $c_1$
and the type~$B$ by $c_2$ as before. If one could imagine the paths
are physical wires and the deformations $c_1$ and $c_2$ as arbitrary
deformations on these wires then, holding the points $A$, $B$, $C$,
and $D$ fixed, it is possible to rotate the top part of the diagram to
become identical to the bottom one. That rotation can be undone
(reversed), which takes the bottom part of the diagram into the top
part.  In other words, there exists a deformation of the program
($c_1$~\oplusu~$c_2$)~\compose~\swapp\ to the program \swapp~\compose~
($c_2$~\oplusu~$c_1$).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A Dependent Type for Waves}
\label{sec:dpair}

The conventional $\Sigma$-type of dependent pairs is defined as
follows. Let $A$ be a type and let $P$ is a type family indexed by $A$
mapping each element $a : A$ to a type in the universe of types
\AgdaPrimitive{Set}, then one can form dependent pairs $(a , b)$ such
that $a : A$ and $b : P~a$. In a language which natively supports
reversible functions such as $\Pi$ defined above, we could define a
different flavor of dependent pairs in which the function $P : A
\rightarrow \AgdaPrimitive{Set}$ is replaced by a combinator $c :
A~\iso~B$. The elements of this type would be pairs $\wv{a}{b}$ such as
$a : A$, $b : B$, and $b = \AgdaFunction{interp}~c~a$. In a
conventional $\Sigma$-type of dependent pairs, the type of the second
component \emph{depends} on the value of the first component; in the
new type which we write as $\fd{t_1}{c}{t_2}$, the type of the entire
pair $\wv{a}{b}$ \emph{depends} on a program $c$ that relates the two
components. In such a pair, the component $b$ is completely determined
by $a$ and~$c$, and since $c$ is reversible, the component $a$ is also
completely determined by $b$ and $c$. In other words, even though the
pair has two components (particles) that can be independently
manipulated, the dependency encoded in $c$ ties the two components
into a single (wave-like) entity. In the next section, we will exploit
this duality by giving two denotations for this new kind of pair,
first as a ``particle''~$\bullet a$ that stores only one component and
computes the second as needed and then as a ``wave'' $\wv{a}{b}$ that
uses redundant storage but provides more efficient access to the
components.

We begin by extending the $\Pi$-types with the new type
$\fd{t_1}{c}{t_2}$ which we call the type of ``waves generated by
$c$.'' (Because the type of $c$ mentions $t_2$, we initially define a
type constructor $\mathbb{W}~t_1~t_2~c$ that binds $t_2$ before $c$
and then introduce a syntax-declaration that re-orders the parameters
and displays them as $\fd{t_1}{c}{t_2}$.) We call the resulting
language $\Pi^{\mathbb{W}}$.

\medskip
\begin{AgdaMultiCode}
\begin{code}[hide]
module Dpair where
  infix  70 _⊛_
  infix  70 _⊗_
  infix  60 _⊕_
  infix  40 _↔_
  infix  30 _⇔_
  infixr 50 _◎_
  infixr 50 _⊡_

  mutual
\end{code}
\begin{code}
    data 𝕌ʷ : Set where
      𝟘    : 𝕌ʷ
      𝟙    : 𝕌ʷ
      _⊕_  : 𝕌ʷ → 𝕌ʷ → 𝕌ʷ
      _⊗_  : 𝕌ʷ → 𝕌ʷ → 𝕌ʷ
      𝕎    : (t₁ t₂ : 𝕌ʷ) → (t₁ ↔ t₂) → 𝕌ʷ
\end{code}
\medskip

We also extend the level-1 combinators with new combinators to operate
on the type $\mathbb{W}~t_1~t_2~c$. First we have the following two combinators:

\medskip
\begin{code}
    data _↔_ : 𝕌ʷ → 𝕌ʷ → Set where
\end{code}
\begin{code}[hide]
      unite₊l : {t : 𝕌ʷ} → 𝟘 ⊕ t ↔ t
      uniti₊l : {t : 𝕌ʷ} → t ↔ 𝟘 ⊕ t
      unite₊r : {t : 𝕌ʷ} → t ⊕ 𝟘 ↔ t
      uniti₊r : {t : 𝕌ʷ} → t ↔ t ⊕ 𝟘
      swap₊   : {t₁ t₂ : 𝕌ʷ} → t₁ ⊕ t₂ ↔ t₂ ⊕ t₁
      assocl₊ : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊕ (t₂ ⊕ t₃) ↔ (t₁ ⊕ t₂) ⊕ t₃
      assocr₊ : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊕ t₂) ⊕ t₃ ↔ t₁ ⊕ (t₂ ⊕ t₃)
      unite⋆l : {t : 𝕌ʷ} → 𝟙 ⊗ t ↔ t
      uniti⋆l : {t : 𝕌ʷ} → t ↔ 𝟙 ⊗ t
      unite⋆r : {t : 𝕌ʷ} → t ⊗ 𝟙 ↔ t
      uniti⋆r : {t : 𝕌ʷ} → t ↔ t ⊗ 𝟙
      swap⋆   : {t₁ t₂ : 𝕌ʷ} → t₁ ⊗ t₂ ↔ t₂ ⊗ t₁
      assocl⋆ : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊗ (t₂ ⊗ t₃) ↔ (t₁ ⊗ t₂) ⊗ t₃
      assocr⋆ : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊗ t₂) ⊗ t₃ ↔ t₁ ⊗ (t₂ ⊗ t₃)
      absorbr : {t : 𝕌ʷ} → 𝟘 ⊗ t ↔ 𝟘
      absorbl : {t : 𝕌ʷ} → t ⊗ 𝟘 ↔ 𝟘
      factorzr : {t : 𝕌ʷ} → 𝟘 ↔ t ⊗ 𝟘
      factorzl : {t : 𝕌ʷ} → 𝟘 ↔ 𝟘 ⊗ t
      dist    : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊕ t₂) ⊗ t₃ ↔ (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃)
      factor  : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃) ↔ (t₁ ⊕ t₂) ⊗ t₃
      distl   : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊗ (t₂ ⊕ t₃) ↔ (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃)
      factorl : {t₁ t₂ t₃ : 𝕌ʷ } → (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃) ↔ t₁ ⊗ (t₂ ⊕ t₃)
      id↔     : {t : 𝕌ʷ} → t ↔ t
      _◎_     : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ↔ t₂) → (t₂ ↔ t₃) → (t₁ ↔ t₃)
      _⊕_     : {t₁ t₂ t₃ t₄ : 𝕌ʷ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊕ t₂ ↔ t₃ ⊕ t₄)
      _⊗_     : {t₁ t₂ t₃ t₄ : 𝕌ʷ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊗ t₂ ↔ t₃ ⊗ t₄)
\end{code}
\begin{code}
      wave       : {t₁ t₂ : 𝕌ʷ} (c : t₁ ↔ t₂) → (t₁ ↔ 𝕎 t₁ t₂ c)
      particle₁  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (𝕎 t₁ t₂ c ↔ t₁)
\end{code}
\medskip

which mediate between values of type $t_1$ and waves generated by $c :
t_1 \iso t_2$. Indeed, no matter what their representation is, values
of type $\mathbb{W}~t_1~t_2~c$ are completely determined by values of
type $t_1$ and vice-versa.

We also add a combinator that formalizes that the program $c$ in the
type $\mathbb{W}~t_1~t_2~c$ is not determined by its syntax but rather by the
equivalence relation generated by the level-2 programs. In other
words, two types $\mathbb{W}~t_1~t_2~c_1$ and $\mathbb{W}~t_1~t_2~c_2$ are considered
equivalent whenever $c_1 \Leftrightarrow c_2$:

\medskip
\begin{code}
      rewrite⇔ : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → (c₁ ⇔ c₂) → (𝕎 t₁ t₂ c₁ ↔ 𝕎 t₁ t₂ c₂)
\end{code}
\medskip

And finally we have a combinator that allows us to operate on each
component of a pair of type $\mathbb{W}~t_1~t_2~c$ by applying a
different combinator to each component. In the resulting pair, the
components are still connected but by a composite program, possibly of
a new type, that records both new actions and the original program:

\medskip
\begin{code}
      _⊛_ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c : t₁ ↔ t₂} (c₁ : t₁ ↔ t₃) (c₂ : t₂ ↔ t₄) → (𝕎 t₁ t₂ c ↔ 𝕎 t₃ t₄ (! c₁ ◎ c ◎ c₂))
\end{code}
\medskip

\begin{code}[hide]
    data _⇔_ : {t₁ t₂ : 𝕌ʷ} → (t₁ ↔ t₂) → (t₁ ↔ t₂) → Set where
      assoc◎l : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → (c₁ ◎ (c₂ ◎ c₃)) ⇔ ((c₁ ◎ c₂) ◎ c₃)
      assoc◎r : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → ((c₁ ◎ c₂) ◎ c₃) ⇔ (c₁ ◎ (c₂ ◎ c₃))
      assocl⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊) ⇔ (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃))
      assocl⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃)) ⇔ ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊)
      assocl⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆) ⇔ (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃))
      assocl⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃)) ⇔ ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆)
      assocr⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊) ⇔ (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃)))
      assocr⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃))) ⇔ (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆)
      assocr⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆) ⇔ (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃)))
      assocr⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃))) ⇔ (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊)
      dist⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → ((a ⊕ b) ⊗ c) ◎ dist ⇔ dist ◎ ((a ⊗ c) ⊕ (b ⊗ c))
      dist⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → dist ◎ ((a ⊗ c) ⊕ (b ⊗ c)) ⇔ ((a ⊕ b) ⊗ c) ◎ dist
      distl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → (a ⊗ (b ⊕ c)) ◎ distl ⇔ distl ◎ ((a ⊗ b) ⊕ (a ⊗ c))
      distl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → distl ◎ ((a ⊗ b) ⊕ (a ⊗ c)) ⇔ (a ⊗ (b ⊕ c)) ◎ distl
      factor⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor ⇔ factor ◎ ((a ⊕ b) ⊗ c)
      factor⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → factor ◎ ((a ⊕ b) ⊗ c) ⇔ ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor
      factorl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl ⇔ factorl ◎ (a ⊗ (b ⊕ c))
      factorl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → factorl ◎ (a ⊗ (b ⊕ c)) ⇔ ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl
      idl◎l   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (id↔ ◎ c) ⇔ c
      idl◎r   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ id↔ ◎ c
      idr◎l   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (c ◎ id↔) ⇔ c
      idr◎r   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ (c ◎ id↔) 
      linv◎l  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (c ◎ ! c) ⇔ id↔
      linv◎r  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → id↔ ⇔ (c ◎ ! c) 
      rinv◎l  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (! c ◎ c) ⇔ id↔
      rinv◎r  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → id↔ ⇔ (! c ◎ c) 
      unite₊l⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊l ◎ c₂) ⇔ ((c₁ ⊕ c₂) ◎ unite₊l)
      unite₊l⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊕ c₂) ◎ unite₊l) ⇔ (unite₊l ◎ c₂)
      uniti₊l⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊l ◎ (c₁ ⊕ c₂)) ⇔ (c₂ ◎ uniti₊l)
      uniti₊l⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊l) ⇔ (uniti₊l ◎ (c₁ ⊕ c₂))
      unite₊r⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊r ◎ c₂) ⇔ ((c₂ ⊕ c₁) ◎ unite₊r)
      unite₊r⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊕ c₁) ◎ unite₊r) ⇔ (unite₊r ◎ c₂)
      uniti₊r⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊r ◎ (c₂ ⊕ c₁)) ⇔ (c₂ ◎ uniti₊r)
      uniti₊r⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊r) ⇔ (uniti₊r ◎ (c₂ ⊕ c₁))
      swapl₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap₊ ◎ (c₁ ⊕ c₂)) ⇔ ((c₂ ⊕ c₁) ◎ swap₊)
      swapr₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊕ c₁) ◎ swap₊) ⇔ (swap₊ ◎ (c₁ ⊕ c₂))
      unitel⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆l ◎ c₂) ⇔ ((c₁ ⊗ c₂) ◎ unite⋆l)
      uniter⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊗ c₂) ◎ unite⋆l) ⇔ (unite⋆l ◎ c₂)
      unitil⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆l ◎ (c₁ ⊗ c₂)) ⇔ (c₂ ◎ uniti⋆l)
      unitir⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆l) ⇔ (uniti⋆l ◎ (c₁ ⊗ c₂))
      unitel⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆r ◎ c₂) ⇔ ((c₂ ⊗ c₁) ◎ unite⋆r)
      uniter⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊗ c₁) ◎ unite⋆r) ⇔ (unite⋆r ◎ c₂)
      unitil⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆r ◎ (c₂ ⊗ c₁)) ⇔ (c₂ ◎ uniti⋆r)
      unitir⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆r) ⇔ (uniti⋆r ◎ (c₂ ⊗ c₁))
      swapl⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap⋆ ◎ (c₁ ⊗ c₂)) ⇔ ((c₂ ⊗ c₁) ◎ swap⋆)
      swapr⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊗ c₁) ◎ swap⋆) ⇔ (swap⋆ ◎ (c₁ ⊗ c₂))
      id⇔     : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ c
      sym⇔    : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → c₂ ⇔ c₁
      trans⇔  : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ c₃ : t₁ ↔ t₂}
              → (c₁ ⇔ c₂) → (c₂ ⇔ c₃) → (c₁ ⇔ c₃)
      _⊡_  : {t₁ t₂ t₃ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₁ ↔ t₂} {c₄ : t₂ ↔ t₃}
           → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ◎ c₂) ⇔ (c₃ ◎ c₄)
      resp⊕⇔  : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊕ c₂) ⇔ (c₃ ⊕ c₄)
      resp⊗⇔  : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊗ c₂) ⇔ (c₃ ⊗ c₄)
      -- below are the combinators added for the RigCategory structure
      id↔⊕id↔⇔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁} ⊕ id↔ {t₂}) ⇔ id↔
      split⊕-id↔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁ ⊕ t₂}) ⇔ (id↔ ⊕ id↔)
      hom⊕◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄)) ⇔ ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄))
      hom◎⊕⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄)) ⇔ ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄))
      id↔⊗id↔⇔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁} ⊗ id↔ {t₂}) ⇔ id↔
      split⊗-id↔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁ ⊗ t₂}) ⇔ (id↔ ⊗ id↔)
      hom⊗◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄)) ⇔ ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄))
      hom◎⊗⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄)) ⇔ ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄))
      -- associativity triangle
      triangle⊕l : {t₁ t₂ : 𝕌ʷ} → (unite₊r {t₁} ⊕ id↔ {t₂}) ⇔ assocr₊ ◎ (id↔ ⊕ unite₊l)
      triangle⊕r : {t₁ t₂ : 𝕌ʷ} → assocr₊ ◎ (id↔ {t₁} ⊕ unite₊l {t₂}) ⇔ (unite₊r ⊕ id↔)
      triangle⊗l : {t₁ t₂ : 𝕌ʷ} → ((unite⋆r {t₁}) ⊗ id↔ {t₂}) ⇔ assocr⋆ ◎ (id↔ ⊗ unite⋆l)
      triangle⊗r : {t₁ t₂ : 𝕌ʷ} → (assocr⋆ ◎ (id↔ {t₁} ⊗ unite⋆l {t₂})) ⇔ (unite⋆r ⊗ id↔)
      pentagon⊕l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → assocr₊ ◎ (assocr₊ {t₁} {t₂} {t₃ ⊕ t₄})
                 ⇔ ((assocr₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
      pentagon⊕r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → ((assocr₊ {t₁} {t₂} {t₃} ⊕ id↔ {t₄}) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
                 ⇔ assocr₊ ◎ assocr₊
      pentagon⊗l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → assocr⋆ ◎ (assocr⋆ {t₁} {t₂} {t₃ ⊗ t₄})
                 ⇔ ((assocr⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
      pentagon⊗r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → ((assocr⋆ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
                 ⇔ assocr⋆ ◎ assocr⋆
      -- from the braiding
      -- unit coherence
      unite₊l-coh-l : {t₁ : 𝕌ʷ} → unite₊l {t₁} ⇔ swap₊ ◎ unite₊r
      unite₊l-coh-r : {t₁ : 𝕌ʷ} → swap₊ ◎ unite₊r ⇔ unite₊l {t₁}
      unite⋆l-coh-l : {t₁ : 𝕌ʷ} → unite⋆l {t₁} ⇔ swap⋆ ◎ unite⋆r
      unite⋆l-coh-r : {t₁ : 𝕌ʷ} → swap⋆ ◎ unite⋆r ⇔ unite⋆l {t₁}
      hexagonr⊕l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
                 ⇔ ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
      hexagonr⊕r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
                 ⇔ (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
      hexagonl⊕l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
      hexagonl⊕r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
                 ⇔ (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
      hexagonr⊗l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
                 ⇔ ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
      hexagonr⊗r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
                 ⇔ (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
      hexagonl⊗l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
      hexagonl⊗r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
                 ⇔ (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
      absorbl⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → (c₁ ⊗ id↔ {𝟘}) ◎ absorbl ⇔ absorbl ◎ id↔ {𝟘}
      absorbl⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → absorbl ◎ id↔ {𝟘} ⇔ (c₁ ⊗ id↔ {𝟘}) ◎ absorbl
      absorbr⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → (id↔ {𝟘} ⊗ c₁) ◎ absorbr ⇔ absorbr ◎ id↔ {𝟘}
      absorbr⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → absorbr ◎ id↔ {𝟘} ⇔ (id↔ {𝟘} ⊗ c₁) ◎ absorbr
      factorzl⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzl ⇔ factorzl ◎ (id↔ ⊗ c₁)
      factorzl⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → factorzl ◎ (id↔ {𝟘} ⊗ c₁) ⇔ id↔ {𝟘} ◎ factorzl
      factorzr⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzr ⇔ factorzr ◎ (c₁ ⊗ id↔)
      factorzr⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → factorzr ◎ (c₁ ⊗ id↔) ⇔ id↔ ◎ factorzr
      -- from the coherence conditions of RigCategory
      swap₊distl⇔l : {t₁ t₂ t₃ : 𝕌ʷ}
                   → (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl ⇔ distl ◎ swap₊
      swap₊distl⇔r : {t₁ t₂ t₃ : 𝕌ʷ}
                   → distl ◎ swap₊ ⇔ (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl
      dist-swap⋆⇔l : {t₁ t₂ t₃ : 𝕌ʷ}
                   → dist {t₁} {t₂} {t₃} ◎ (swap⋆ ⊕ swap⋆) ⇔ swap⋆ ◎ distl
      dist-swap⋆⇔r : {t₁ t₂ t₃ : 𝕌ʷ}
                   → swap⋆ ◎ distl {t₁} {t₂} {t₃} ⇔ dist ◎ (swap⋆ ⊕ swap⋆)
      assocl₊-dist-dist⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                          → ((assocl₊ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ dist) ◎ (dist ⊕ id↔)
                          ⇔ (dist ◎ (id↔ ⊕ dist)) ◎ assocl₊
      assocl₊-dist-dist⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                          → (dist {t₁} ◎ (id↔ ⊕ dist {t₂} {t₃} {t₄})) ◎ assocl₊
                          ⇔ ((assocl₊ ⊗ id↔) ◎ dist) ◎ (dist ⊕ id↔)
      assocl⋆-distl⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                      → assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}
                      ⇔ ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
      assocl⋆-distl⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                      → ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
                      ⇔ assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}  
      absorbr0-absorbl0⇔ : absorbr {𝟘} ⇔ absorbl {𝟘}
      absorbl0-absorbr0⇔ : absorbl {𝟘} ⇔ absorbr {𝟘}
      absorbr⇔distl-absorb-unite : {t₁ t₂ : 𝕌ʷ}
                                 → absorbr ⇔ (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l
      distl-absorb-unite⇔absorbr : {t₁ t₂ : 𝕌ʷ}
                                 → (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l ⇔ absorbr
      unite⋆r0-absorbr1⇔ : unite⋆r ⇔ absorbr
      absorbr1-unite⋆r-⇔ : absorbr ⇔ unite⋆r
      absorbl≡swap⋆◎absorbr : {t₁ : 𝕌ʷ} → absorbl {t₁} ⇔ swap⋆ ◎ absorbr
      swap⋆◎absorbr≡absorbl : {t₁ : 𝕌ʷ} → swap⋆ ◎ absorbr ⇔ absorbl {t₁}
      absorbr⇔[assocl⋆◎[absorbr⊗id↔]]◎absorbr : {t₁ t₂ : 𝕌ʷ}
                                              → absorbr ⇔ (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr
      [assocl⋆◎[absorbr⊗id↔]]◎absorbr⇔absorbr : {t₁ t₂ : 𝕌ʷ}
                                              → (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr ⇔ absorbr
      [id↔⊗absorbr]◎absorbl⇔assocl⋆◎[absorbl⊗id↔]◎absorbr : {t₁ t₂ : 𝕌ʷ}
                                                          → (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
                                                          ⇔ (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
      assocl⋆◎[absorbl⊗id↔]◎absorbr⇔[id↔⊗absorbr]◎absorbl : {t₁ t₂ : 𝕌ʷ}
                                                          → (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
                                                          ⇔ (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
      elim⊥-A[0⊕B]⇔l : {t₁ t₂ : 𝕌ʷ}
                     → (id↔ {t₁} ⊗ unite₊l {t₂})
                     ⇔ (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
      elim⊥-A[0⊕B]⇔r : {t₁ t₂ : 𝕌ʷ}
                     → (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
                     ⇔ (id↔ {t₁} ⊗ unite₊l {t₂})
      elim⊥-1[A⊕B]⇔l : {t₁ t₂ : 𝕌ʷ}
                     → unite⋆l ⇔ distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂})
      elim⊥-1[A⊕B]⇔r : {t₁ t₂ : 𝕌ʷ} → distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂}) ⇔ unite⋆l
      fully-distribute⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                         → (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
                         ⇔ ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
      fully-distribute⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                         → ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
                         ⇔ (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
      -- make things easier
      cong! : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → (c₁ ⇔ c₂) → (! c₁ ⇔ ! c₂)
  
    ! : {t₁ t₂ : 𝕌ʷ} → t₁ ↔ t₂ → t₂ ↔ t₁
    ! unite₊l = uniti₊l
    ! uniti₊l = unite₊l
    ! unite₊r = uniti₊r
    ! uniti₊r = unite₊r
    ! swap₊ = swap₊
    ! assocl₊ = assocr₊
    ! assocr₊ = assocl₊
    ! unite⋆l = uniti⋆l
    ! uniti⋆l = unite⋆l
    ! unite⋆r = uniti⋆r
    ! uniti⋆r = unite⋆r
    ! swap⋆ = swap⋆
    ! assocl⋆ = assocr⋆
    ! assocr⋆ = assocl⋆
    ! absorbr = factorzl
    ! absorbl = factorzr
    ! factorzr = absorbl
    ! factorzl = absorbr
    ! dist = factor
    ! factor = dist
    ! distl = factorl
    ! factorl = distl
    ! id↔ = id↔
    ! (c₁ ◎ c₂) = ! c₂ ◎ ! c₁
    ! (c₁ ⊕ c₂) = ! c₁ ⊕ ! c₂
    ! (c₁ ⊗ c₂) = ! c₁ ⊗ ! c₂
\end{code}
\medskip

As the following definition illustrates, the new combinators are still
reversible so the whole language is still reversible:

\medskip
\begin{code}
    ! (wave _)          = particle₁
    ! particle₁         = wave _
    ! (rewrite⇔ c₁⇔c₂)  = rewrite⇔ (sym⇔ c₁⇔c₂)
    ! (c₁ ⊛ c₂)         =   (! c₁ ⊛ ! c₂) ◎ 
                            rewrite⇔ (   trans⇔ assoc◎l
                                        (trans⇔ (assoc◎l ⊡ id⇔)
                                        (trans⇔ ((rinv◎l ⊡ id⇔) ⊡ id⇔)
                                        (trans⇔ (idl◎l ⊡ id⇔)
                                        (trans⇔ assoc◎r
                                        (trans⇔ (id⇔ ⊡ linv◎l) idr◎l))))))
\end{code}
\end{AgdaMultiCode}
\medskip

Note the arguments to \AgdaInductiveConstructor{rewrite⇔} which
provide the evidence that the new combinator is semantically
equivalent to the inverse of the original combinator.

We conclude this section with a few examples written in
$\Pi^{\mathbb{W}}$. We start by introducing our syntax for the waves
generated by a combinator:

\medskip
\begin{code}
    syntax 𝕎 t₁ t₂ c = ⟨ t₁ ⤙ c ⤚ t₂ ⟩
\end{code}
\medskip

As a first example, here is a simple combinator that maps a boolean
value to a ``trivial'' wave of booleans:

\begin{AgdaMultiCode}
\medskip
\begin{code}[hide]
  𝟚 : 𝕌ʷ
  𝟚 = 𝟙 ⊕ 𝟙

  not : 𝟚 ↔ 𝟚
  not = swap₊
\end{code}
\begin{code}
  Ex1 : 𝟚 ↔ ⟨ 𝟚 ⤙ id↔ ⤚ 𝟚 ⟩
  Ex1 = wave id↔
  \end{code}
\medskip

When applied to a boolean value $v$, the above combinator produces a
pair consisting of two copies of $v$ connected by
$\AgdaInductiveConstructor{id↔}$. Given that the combinators relating
the components are identified up to the equivalence relation induced
by the level-2 combinators, we could have written the above example
as:
\medskip
\begin{code}
  Ex2 : 𝟚 ↔ ⟨ 𝟚 ⤙ not ◎ not ⤚ 𝟚 ⟩
  Ex2 = wave id↔ ◎ rewrite⇔ rinv◎r
\end{code}
\medskip

We now manipulate the pair by applying \AgdaInductiveConstructor{id↔}
to the first component and \AgdaFunction{not} to the second
component. After simplification, the two components are just related
by \AgdaFunction{not}:

\medskip
\begin{code}
  Ex3 : 𝟚 ↔ ⟨ 𝟚 ⤙ not ⤚ 𝟚 ⟩
  Ex3 = Ex2 ◎ (id↔ ⊛ not) ◎ (rewrite⇔ eq⇔)
    where
    eq⇔ : (! id↔) ◎ (not ◎ not) ◎ not ⇔ not
    eq⇔ = trans⇔ idl◎l (trans⇔ (rinv◎l ⊡ id⇔) idl◎l)
\end{code}
\medskip

We can easily extract the first component of the pair constructed above:

\medskip
\begin{code}
  Ex4 : ⟨ 𝟚 ⤙ not ⤚ 𝟚 ⟩ ↔ 𝟚
  Ex4 = particle₁
\end{code}
\medskip

In fact since each component is determined by the other, we can also
extract the second component after writing a generic swapping operation:

\medskip
\begin{code}
  swapʷ : ∀ {t₁ t₂} {c : t₁ ↔ t₂} → ⟨ t₁ ⤙ c ⤚ t₂ ⟩ ↔ ⟨ t₂ ⤙ ! c ⤚ t₁ ⟩
  swapʷ {t₁} {t₂} {c} = (c ⊛ ! c) ◎ rewrite⇔ eq⇔
    where
    eq⇔ : (! c) ◎ c ◎ (! c) ⇔ ! c
    eq⇔ = trans⇔ (id⇔ ⊡ linv◎l) idr◎l

  particle₂ : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → ⟨ t₁ ⤙ c ⤚ t₂ ⟩ ↔ t₂
  particle₂ = swapʷ ◎ particle₁
\end{code}
\medskip

So far, all our ``waves'' consisted of exactly two values. It is
possible to nest the waves to build larger and larger ones. Generally,
since the combinator that ties the values is reversible, we need $n-1$
combinators to uniquely determine a type of waves of
$n$-components. In the next section, we will directly introduce such
waves of $n$-components for efficiency. Here we show the simple case
of $n=3$ encoded as two nested waves:

\begin{code}
  𝕎3 : (t₁ t₂ t₃ : 𝕌ʷ) (c₁ : t₁ ↔ t₂) (c₂ : t₂ ↔ t₃) → 𝕌ʷ
  𝕎3 t₁ t₂ t₃ c₁ c₂ = ⟨ t₁ ⤙ c₁ ◎ wave c₂ ⤚ ⟨ t₂ ⤙ c₂ ⤚ t₃ ⟩ ⟩
  syntax 𝕎3 t₁ t₂ t₃ c₁ c₂ = ⟨ t₁ ⤙ c₁ ⤚⤙ t₂ ⤚⤙ c₂ ⤚ t₃ ⟩
\end{code}
\end{AgdaMultiCode}
\medskip

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Operational Semantics}
\label{sec:opsem}

As motivated in the previous section, values of type
$\fd{t_1}{c}{t_2}$ can be represented by values of either $t_1$ or
$t_2$ since each is recoverable from the other. 
% And of course, it is also possible to store both values in the runtime representation. 
We present two variants and develop a corresponding operational
semantics for each in this section.

%%%
\subsection{Particle-Style Semantics}

The first denotation we present for the type $\fd{t_1}{c}{t_2}$ is
simply the denotation of $t_1$. For the sake of clarity, , we add a
tag $\bullet$ to values to distinguish them from plain values of type
$t_1$.

\begin{AgdaMultiCode}
% The interpretation of type $\fd{t_1}{c}{t_2}$ is suppose to be all the
% dependent pairs $\wv{v_1}{v_2}$, so it should be $⟦ \fd{t_1}{c}{t_2}
% ⟧ʷ = \{ \wv{v_1}{v_2} ∈ ⟦ t₁ ⟧ʷ × ⟦ t₂ ⟧ʷ |
% \AgdaFunction{interp𝕌ʷ}~c~v_1 ≡ v_2 \}$ where $c : t₁ ↔ t₂$, but since
% $c$ is reversible and a witness that $t_1$ and $t_2$ have the same
% amount of elements so this is equal to \[ \{
% \wv{v_1}{\AgdaFunction{interp𝕌ʷ}~c~v_1} | v_1 ∈ ⟦ t₁ ⟧ʷ \} = \{
% \wv{\AgdaFunction{interp𝕌ʷ}~!c~v_2}{v_2} | v_2 ∈ ⟦ t₂ ⟧ʷ \} \] since
% fix an element $v_i ∈ ⟦ t_i ⟧ʷ$, $v_{1-i}$ is uniquely determined so
% unlike the normal product we only need to keep one element of the
% pair, in this paper we choose to pick $⟦ \fd{t_1}{c}{t_2} ⟧ʷ = ⟦ t₁
% ⟧ʷ$.  %% since we know that relation between %% two values and we only
% need one to deduce the other, so the interpretation of
% $\AgdaInductiveConstructor{𝕎}_c$ %% is just ⟦ t₁ ⟧ʷ where $c : t₁ ↔
% t₂$.  To make the representation more clear in Agda code, we add a tag
% to indicate the value of dependent pair:

\medskip
\begin{code}
  mutual 
    data Particle : (t₁ t₂ : 𝕌ʷ) (c : t₁ ↔ t₂) → Set where 
     ● : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → ⟦ t₁ ⟧ʷ → Particle t₁ t₂ c

    ⟦_⟧ʷ : 𝕌ʷ → Set
    ⟦ 𝟘 ⟧ʷ = ⊥
    ⟦ 𝟙 ⟧ʷ = ⊤
    ⟦ t₁ ⊗ t₂ ⟧ʷ = ⟦ t₁ ⟧ʷ × ⟦ t₂ ⟧ʷ
    ⟦ t₁ ⊕ t₂ ⟧ʷ = ⟦ t₁ ⟧ʷ ⊎ ⟦ t₂ ⟧ʷ
    ⟦ 𝕎 t₁ t₂ c ⟧ʷ = Particle t₁ t₂ c
\end{code}
\begin{code}[hide]
  0₂ 1₂ : ⟦ 𝟚 ⟧ʷ
  0₂ = inj₁ tt
  1₂ = inj₂ tt
\end{code}
\medskip

For example, there are two values of type
$\fd{\mathbb{2}}{\AgdaFunction{not}}{\mathbb{2}}$ corresponding to the two values of
type $\mathbb{2}$ on the left:
\begin{code}
  Ex5 Ex5' : ⟦ ⟨ 𝟚 ⤙ not ⤚ 𝟚 ⟩ ⟧ʷ
  Ex5   = ● 0₂
  Ex5'  = ● 1₂
\end{code}

The operational semantics for the new combinators is then given as follows:
\medskip
\begin{code}
  interp𝕌ʷ : {t₁ t₂ : 𝕌ʷ} → (t₁ ↔ t₂) → ⟦ t₁ ⟧ʷ → ⟦ t₂ ⟧ʷ
  interp𝕌ʷ (wave _) v          = ● v
  interp𝕌ʷ particle₁ (● v)     = v 
  interp𝕌ʷ (rewrite⇔ _) (● v)  = (● v) 
  interp𝕌ʷ (c₁ ⊛ c₂) (● v)     = ● (interp𝕌ʷ c₁ v)
\end{code}
\begin{code}[hide]
  interp𝕌ʷ unite₊l (inj₁ ())
  interp𝕌ʷ unite₊l (inj₂ v) = v
  interp𝕌ʷ uniti₊l v = inj₂ v
  interp𝕌ʷ unite₊r (inj₁ v) = v
  interp𝕌ʷ unite₊r (inj₂ ())
  interp𝕌ʷ uniti₊r v = inj₁ v
  interp𝕌ʷ swap₊ (inj₁ v) = inj₂ v
  interp𝕌ʷ swap₊ (inj₂ v) = inj₁ v
  interp𝕌ʷ assocl₊ (inj₁ v) = inj₁ (inj₁ v)
  interp𝕌ʷ assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
  interp𝕌ʷ assocl₊ (inj₂ (inj₂ v)) = inj₂ v
  interp𝕌ʷ assocr₊ (inj₁ (inj₁ v)) = inj₁ v
  interp𝕌ʷ assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
  interp𝕌ʷ assocr₊ (inj₂ v) = inj₂ (inj₂ v)
  interp𝕌ʷ unite⋆l v = proj₂ v
  interp𝕌ʷ uniti⋆l v = tt , v
  interp𝕌ʷ unite⋆r v = proj₁ v
  interp𝕌ʷ uniti⋆r v = v , tt
  interp𝕌ʷ swap⋆ (v₁ , v₂) = v₂ , v₁
  interp𝕌ʷ assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
  interp𝕌ʷ assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
  interp𝕌ʷ absorbr (() , v)
  interp𝕌ʷ absorbl (v , ())
  interp𝕌ʷ factorzr ()
  interp𝕌ʷ factorzl ()
  interp𝕌ʷ dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
  interp𝕌ʷ dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
  interp𝕌ʷ factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
  interp𝕌ʷ factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
  interp𝕌ʷ distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
  interp𝕌ʷ distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
  interp𝕌ʷ factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
  interp𝕌ʷ factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
  interp𝕌ʷ id↔ v = v
  interp𝕌ʷ (c₁ ◎ c₂) v = interp𝕌ʷ c₂ (interp𝕌ʷ c₁ v)
  interp𝕌ʷ (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interp𝕌ʷ c₁ v)
  interp𝕌ʷ (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interp𝕌ʷ c₂ v)
  interp𝕌ʷ (c₁ ⊗ c₂) (v₁ , v₂) = interp𝕌ʷ c₁ v₁ , interp𝕌ʷ c₂ v₂
\end{code}
\medskip

The first two clauses are essentially the identity except that we add
and remove the tag. The semantics of rewriting only changes the
dependency which is part of the type and has no effect on the runtime
representation. In the last clause, we completely ignore the impact of
$c_2$ on the runtime representation. Note however that it is not
completely forgotten as it is part of the type. Given this semantics,
we can now run the examples in the previous section and get the expected
results:

\medskip
\begin{code}
  t1 t2 t3 : ⟦ 𝟚 ⟧ʷ

  t1 = interp𝕌ʷ (Ex1 ◎ particle₁) 0₂   -- evaluates to 0₂
  t2 = interp𝕌ʷ (Ex3 ◎ particle₁) 0₂   -- evaluates to 0₂
  t3 = interp𝕌ʷ (Ex3 ◎ particle₂) 0₂   -- evaluates to 1₂
\end{code}
\medskip

% \AgdaFunction{Ex6} creates a dependent triple $\langle b \Yleft \idc \Yright b \Yleft \AgdaFunction{not} \Yright \overline{b} \rangle$:
% \begin{code}
%   Ex6 : 𝟚 ↔ 𝔻triple (id↔ {𝟚}) not
%   Ex6 = wave ((id↔ ◎ wave not))

%   test1 : interp𝕌ʷ (Ex6 ◎ particle₁) 0₂ ≡ 0₂
%   test1 = refl
%   test2 : interp𝕌ʷ (Ex6 ◎ particle₂ ◎ particle₁) 0₂ ≡ 0₂
%   test2 = refl
%   test3 : interp𝕌ʷ (Ex6 ◎ particle₂ ◎ particle₂) 0₂ ≡ 1₂
%   test3 = refl 

%   t1 : Particle 𝟚 𝟚 id↔
%   t1 = interp𝕌ʷ Ex1 0₂   -- evaluates to ● 0₂

%   t2 : Particle 𝟚 𝟚 not
%   t2 = interp𝕌ʷ Ex3 0₂   -- evaluates to ● 0₂

% \end{code}
\end{AgdaMultiCode}

%% C-x 8 U+227A U+227B

%%%
\subsection{Wave-Style Semantics}

The semantics in the previous section is easy to understand and
implement. It is however rather inefficient if the program requests
the second component of the pair instead of the first especially if
this happens after a long sequence of applications. And the situation
gets worse and worse if waves are nested. To illustrate the problem,
let's start with a value of type $\fd{t_1}{c}{t_2}$, i.e., with a
(tagged) value of type $t_1$, and represent it graphically as follows:

\begin{center}
\begin{tikzcd}
  {\color{blue}v} \arrow[d]\\
  ? \arrow[u, "c"']
\end{tikzcd}
\end{center}

The graphical notation shows the placeholders for both components even
though only one of them has a runtime representation. If we apply
$c_1$ to the left known component and $c_1'$ to the right unknown
component, the diagram becomes:

\begin{center}
\begin{tikzcd}
  \arrow[d] \arrow[r, "c_1"] & {\color{blue}v_1} \arrow[l] \\
  \arrow[u, "c"'] \arrow[r, "c_1'"'] & ? \arrow[l]
\end{tikzcd}
\end{center}
where the known value has been updated by applying $c_1$ to $v$ and no
action was taken on the value the value that is not explicitly stored. 

After a potentially long sequence of applications, we reach the
following configuration:

\begin{center}
\begin{tikzcd}
  \arrow[d] \arrow[r, "c_1"] & … \arrow[l] \arrow[r, "c_n"] & {\color{blue}v_n} \arrow[l] \\
  \arrow[u, "c"'] \arrow[r, "c_1'"'] & … \arrow[l] \arrow[r, "c_n'"'] & ? \arrow[l]
\end{tikzcd}
\end{center}
Note that because level-1 combinators are determined up to the
equivalence relation generated by level-2 programs, it is in principle
possible to simplify the sequence of combinators to a shorter, more
direct, sequence. This is however a problem that is as difficult as
checking the equivalence of combinational circuits which in the
general case is a co-NP problem. 

Leaving the sequence combinators unoptimized, the runtime
representation includes $\color{blue}{v_n}$ and the type includes all
the relevant $c_i$ and $c'_i$ combinators. Producing the first
projection is trivial since it is exactly the stored runtime
representation. Calculating the second projection is however quite
inefficient. It requires essentially the red computation below:

\begin{center}
\begin{tikzcd}
  \arrow[d] \arrow[d, color=red, shift left] \arrow[r, "c_1"] & … \arrow[l, color=red, shift left] \arrow[l] \arrow[r, "c_n"]
  & {\color{blue}v_n} \arrow[l] \arrow[l, color=red, shift left] \\
  \arrow[u, "c"] \arrow[r, "c_1'"'] \arrow[r, color=red, shift left]
  & … \arrow[l] \arrow[r, "c_n'"'] \arrow[r, color=red, shift left] & {\color{blue}v_n'} \arrow[l]
\end{tikzcd}
\end{center}
The net effect of the top red path is to recover the original value as
the computation \AgdaBound{c₁} \compose … \compose \AgdaBound{cₙ}
\compose \AgdaBound{!cₙ} \compose … \compose \AgdaBound{!c₁} is just
the identity \idc. Once the second component has been calculated, the
runtime representation changes to:

\begin{center}
\begin{tikzcd}
  \arrow[d] \arrow[r, "c_1'"] & … \arrow[l] \arrow[r, "c_n'"] & {\color{blue}v_n'} \arrow[l] \\
  \arrow[u, "c"'] \arrow[r, "c_1"'] & … \arrow[l] \arrow[r, "c_n"'] & ? \arrow[l]
\end{tikzcd}
\end{center}
which of course is susceptible to same inefficiency after another
sequence of computations that concludes with a request for the first
component.

Things only get worse if the waves are nested and we ask for the
$n$-th projection of a $n$-tuple:

\begin{center}
\begin{tikzcd}
  \arrow[d] \arrow[d, color=red, shift left] \arrow[r] & … \arrow[l, color=red, shift left] \arrow[l] \arrow[r]
  & {\color{blue}v_n} \arrow[l] \arrow[l, color=red, shift left] \\
  \arrow[u] \arrow[d] \arrow[d, color=red, shift left] \arrow[r] \arrow[r, color=red, shift left]
  & … \arrow[r, color=red, shift left] \arrow[l, color=red, shift left] \arrow[l] \arrow[r]
  & \arrow[l] \arrow[l, color=red, shift left] \\
  ⋮ \arrow[u] \arrow[d] \arrow[d, color=red, shift left] \\
  \arrow[u] \arrow[d] \arrow[d, color=red, shift left] \arrow[r] \arrow[r, color=red, shift left]
  & … \arrow[r, color=red, shift left] \arrow[l, color=red, shift left] \arrow[l] \arrow[r]
  & \arrow[l] \arrow[l, color=red, shift left] \\
  \arrow[u] \arrow[r] \arrow[r, color=red, shift left]
  & … \arrow[l] \arrow[r] \arrow[r, color=red, shift left] & {\color{blue}v_n'} \arrow[l]
\end{tikzcd}
\end{center}

One way to improve the situation is to store the two values explicitly
and keep updating them, however since we only project one of them in
the end so half of the computation is unnecessary (if we never project, 
then the whole computation is unnecessary).
A better approach to avoid redundancy is to store the two sequences 
of combinators as part of the runtime representation. 
That way, no unnecessary computation is
performed until one of the components is requested, and only that
branch is evaluated at that time. In the diagram below, the runtime
representation holds the original value and the two sequences of
combinators. If the first component is requested we follow the top
path to compute it; if the second component is requested we follow the
red path to compute it:

\begin{center}
\begin{tikzcd}
   {\color{blue}v} \arrow[d] \arrow[d, color=red, shift left] \arrow[r, "c_1"] & … \arrow[l] \arrow[r, "c_n"] & \arrow[l] \\
  \arrow[u, "c"] \arrow[r, "c_1'"'] \arrow[r, color=red, shift left]
  & … \arrow[l] \arrow[r, "c_n'"'] \arrow[r, color=red, shift left] & {\color{blue}v_n'} \arrow[l]
\end{tikzcd}
\end{center}

Of course repeated requests will require repeated computations and
more sophisticated representations might be desirable in such
situations.

\begin{AgdaMultiCode}
\begin{code}[hide]
module Opsem where
  infix  70 _⊗_
  infix  60 _⊕_
  infix  40 _↔_
  infix  30 _⇔_
  infixr 50 _◎_
  infixr 50 _⊡_

  mutual
    data 𝕌ʷ : Set where
      𝟘 : 𝕌ʷ
      𝟙 : 𝕌ʷ
      _⊕_ : 𝕌ʷ → 𝕌ʷ → 𝕌ʷ
      _⊗_ : 𝕌ʷ → 𝕌ʷ → 𝕌ʷ
      𝕎 : (t₁ t₂ : 𝕌ʷ) → t₁ ↔ t₂ → 𝕌ʷ

    data _↔_ : 𝕌ʷ → 𝕌ʷ → Set where
      unite₊l : {t : 𝕌ʷ} → 𝟘 ⊕ t ↔ t
      uniti₊l : {t : 𝕌ʷ} → t ↔ 𝟘 ⊕ t
      unite₊r : {t : 𝕌ʷ} → t ⊕ 𝟘 ↔ t
      uniti₊r : {t : 𝕌ʷ} → t ↔ t ⊕ 𝟘
      swap₊   : {t₁ t₂ : 𝕌ʷ} → t₁ ⊕ t₂ ↔ t₂ ⊕ t₁
      assocl₊ : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊕ (t₂ ⊕ t₃) ↔ (t₁ ⊕ t₂) ⊕ t₃
      assocr₊ : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊕ t₂) ⊕ t₃ ↔ t₁ ⊕ (t₂ ⊕ t₃)
      unite⋆l : {t : 𝕌ʷ} → 𝟙 ⊗ t ↔ t
      uniti⋆l : {t : 𝕌ʷ} → t ↔ 𝟙 ⊗ t
      unite⋆r : {t : 𝕌ʷ} → t ⊗ 𝟙 ↔ t
      uniti⋆r : {t : 𝕌ʷ} → t ↔ t ⊗ 𝟙
      swap⋆   : {t₁ t₂ : 𝕌ʷ} → t₁ ⊗ t₂ ↔ t₂ ⊗ t₁
      assocl⋆ : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊗ (t₂ ⊗ t₃) ↔ (t₁ ⊗ t₂) ⊗ t₃
      assocr⋆ : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊗ t₂) ⊗ t₃ ↔ t₁ ⊗ (t₂ ⊗ t₃)
      absorbr : {t : 𝕌ʷ} → 𝟘 ⊗ t ↔ 𝟘
      absorbl : {t : 𝕌ʷ} → t ⊗ 𝟘 ↔ 𝟘
      factorzr : {t : 𝕌ʷ} → 𝟘 ↔ t ⊗ 𝟘
      factorzl : {t : 𝕌ʷ} → 𝟘 ↔ 𝟘 ⊗ t
      dist    : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊕ t₂) ⊗ t₃ ↔ (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃)
      factor  : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃) ↔ (t₁ ⊕ t₂) ⊗ t₃
      distl   : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊗ (t₂ ⊕ t₃) ↔ (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃)
      factorl : {t₁ t₂ t₃ : 𝕌ʷ } → (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃) ↔ t₁ ⊗ (t₂ ⊕ t₃)
      id↔     : {t : 𝕌ʷ} → t ↔ t
      _◎_     : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ↔ t₂) → (t₂ ↔ t₃) → (t₁ ↔ t₃)
      _⊕_     : {t₁ t₂ t₃ t₄ : 𝕌ʷ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊕ t₂ ↔ t₃ ⊕ t₄)
      _⊗_     : {t₁ t₂ t₃ t₄ : 𝕌ʷ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊗ t₂ ↔ t₃ ⊗ t₄)
      wave : {t₁ t₂ : 𝕌ʷ} (c : t₁ ↔ t₂) → t₁ ↔ 𝕎 t₁ t₂ c
      particle₁ : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → 𝕎 t₁ t₂ c ↔ t₁
      rewrite⇔ : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → 𝕎 t₁ t₂ c₁ ↔ 𝕎 t₁ t₂ c₂
      _⊛_ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c : t₁ ↔ t₂} (c₁ : t₁ ↔ t₃) (c₂ : t₂ ↔ t₄) → 𝕎 t₁ t₂ c ↔ 𝕎 t₃ t₄ (! c₁ ◎ c ◎ c₂)
\end{code}

Formally we proceed as follows. First, instead of the inefficient
definition of \AgdaInductiveConstructor{swapʷ}, we will add
\AgdaInductiveConstructor{swapʷ} as a primitive combinator and give it
an efficient implementation:

\medskip
\begin{code}
      swapʷ     : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → 𝕎 t₁ t₂ c ↔ 𝕎 t₂ t₁ (! c)
\end{code}

\begin{code}[hide]
    data _⇔_ : {t₁ t₂ : 𝕌ʷ} → (t₁ ↔ t₂) → (t₁ ↔ t₂) → Set where
      assoc◎l : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → (c₁ ◎ (c₂ ◎ c₃)) ⇔ ((c₁ ◎ c₂) ◎ c₃)
      assoc◎r : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → ((c₁ ◎ c₂) ◎ c₃) ⇔ (c₁ ◎ (c₂ ◎ c₃))
      assocl⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊) ⇔ (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃))
      assocl⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃)) ⇔ ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊)
      assocl⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆) ⇔ (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃))
      assocl⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃)) ⇔ ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆)
      assocr⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊) ⇔ (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃)))
      assocr⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃))) ⇔ (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆)
      assocr⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆) ⇔ (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃)))
      assocr⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃))) ⇔ (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊)
      dist⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → ((a ⊕ b) ⊗ c) ◎ dist ⇔ dist ◎ ((a ⊗ c) ⊕ (b ⊗ c))
      dist⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → dist ◎ ((a ⊗ c) ⊕ (b ⊗ c)) ⇔ ((a ⊕ b) ⊗ c) ◎ dist
      distl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → (a ⊗ (b ⊕ c)) ◎ distl ⇔ distl ◎ ((a ⊗ b) ⊕ (a ⊗ c))
      distl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → distl ◎ ((a ⊗ b) ⊕ (a ⊗ c)) ⇔ (a ⊗ (b ⊕ c)) ◎ distl
      factor⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor ⇔ factor ◎ ((a ⊕ b) ⊗ c)
      factor⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → factor ◎ ((a ⊕ b) ⊗ c) ⇔ ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor
      factorl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl ⇔ factorl ◎ (a ⊗ (b ⊕ c))
      factorl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → factorl ◎ (a ⊗ (b ⊕ c)) ⇔ ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl
      idl◎l   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (id↔ ◎ c) ⇔ c
      idl◎r   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ id↔ ◎ c
      idr◎l   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (c ◎ id↔) ⇔ c
      idr◎r   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ (c ◎ id↔) 
      linv◎l  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (c ◎ ! c) ⇔ id↔
      linv◎r  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → id↔ ⇔ (c ◎ ! c) 
      rinv◎l  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (! c ◎ c) ⇔ id↔
      rinv◎r  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → id↔ ⇔ (! c ◎ c) 
      unite₊l⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊l ◎ c₂) ⇔ ((c₁ ⊕ c₂) ◎ unite₊l)
      unite₊l⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊕ c₂) ◎ unite₊l) ⇔ (unite₊l ◎ c₂)
      uniti₊l⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊l ◎ (c₁ ⊕ c₂)) ⇔ (c₂ ◎ uniti₊l)
      uniti₊l⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊l) ⇔ (uniti₊l ◎ (c₁ ⊕ c₂))
      unite₊r⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊r ◎ c₂) ⇔ ((c₂ ⊕ c₁) ◎ unite₊r)
      unite₊r⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊕ c₁) ◎ unite₊r) ⇔ (unite₊r ◎ c₂)
      uniti₊r⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊r ◎ (c₂ ⊕ c₁)) ⇔ (c₂ ◎ uniti₊r)
      uniti₊r⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊r) ⇔ (uniti₊r ◎ (c₂ ⊕ c₁))
      swapl₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap₊ ◎ (c₁ ⊕ c₂)) ⇔ ((c₂ ⊕ c₁) ◎ swap₊)
      swapr₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊕ c₁) ◎ swap₊) ⇔ (swap₊ ◎ (c₁ ⊕ c₂))
      unitel⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆l ◎ c₂) ⇔ ((c₁ ⊗ c₂) ◎ unite⋆l)
      uniter⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊗ c₂) ◎ unite⋆l) ⇔ (unite⋆l ◎ c₂)
      unitil⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆l ◎ (c₁ ⊗ c₂)) ⇔ (c₂ ◎ uniti⋆l)
      unitir⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆l) ⇔ (uniti⋆l ◎ (c₁ ⊗ c₂))
      unitel⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆r ◎ c₂) ⇔ ((c₂ ⊗ c₁) ◎ unite⋆r)
      uniter⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊗ c₁) ◎ unite⋆r) ⇔ (unite⋆r ◎ c₂)
      unitil⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆r ◎ (c₂ ⊗ c₁)) ⇔ (c₂ ◎ uniti⋆r)
      unitir⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆r) ⇔ (uniti⋆r ◎ (c₂ ⊗ c₁))
      swapl⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap⋆ ◎ (c₁ ⊗ c₂)) ⇔ ((c₂ ⊗ c₁) ◎ swap⋆)
      swapr⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊗ c₁) ◎ swap⋆) ⇔ (swap⋆ ◎ (c₁ ⊗ c₂))
      id⇔     : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ c
      sym⇔    : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → c₂ ⇔ c₁
      trans⇔  : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ c₃ : t₁ ↔ t₂}
              → (c₁ ⇔ c₂) → (c₂ ⇔ c₃) → (c₁ ⇔ c₃)
      _⊡_  : {t₁ t₂ t₃ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₁ ↔ t₂} {c₄ : t₂ ↔ t₃}
           → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ◎ c₂) ⇔ (c₃ ◎ c₄)
      resp⊕⇔  : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊕ c₂) ⇔ (c₃ ⊕ c₄)
      resp⊗⇔  : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊗ c₂) ⇔ (c₃ ⊗ c₄)
      -- below are the combinators added for the RigCategory structure
      id↔⊕id↔⇔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁} ⊕ id↔ {t₂}) ⇔ id↔
      split⊕-id↔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁ ⊕ t₂}) ⇔ (id↔ ⊕ id↔)
      hom⊕◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄)) ⇔ ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄))
      hom◎⊕⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄)) ⇔ ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄))
      id↔⊗id↔⇔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁} ⊗ id↔ {t₂}) ⇔ id↔
      split⊗-id↔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁ ⊗ t₂}) ⇔ (id↔ ⊗ id↔)
      hom⊗◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄)) ⇔ ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄))
      hom◎⊗⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄)) ⇔ ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄))
      -- associativity triangle
      triangle⊕l : {t₁ t₂ : 𝕌ʷ} → (unite₊r {t₁} ⊕ id↔ {t₂}) ⇔ assocr₊ ◎ (id↔ ⊕ unite₊l)
      triangle⊕r : {t₁ t₂ : 𝕌ʷ} → assocr₊ ◎ (id↔ {t₁} ⊕ unite₊l {t₂}) ⇔ (unite₊r ⊕ id↔)
      triangle⊗l : {t₁ t₂ : 𝕌ʷ} → ((unite⋆r {t₁}) ⊗ id↔ {t₂}) ⇔ assocr⋆ ◎ (id↔ ⊗ unite⋆l)
      triangle⊗r : {t₁ t₂ : 𝕌ʷ} → (assocr⋆ ◎ (id↔ {t₁} ⊗ unite⋆l {t₂})) ⇔ (unite⋆r ⊗ id↔)
      pentagon⊕l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → assocr₊ ◎ (assocr₊ {t₁} {t₂} {t₃ ⊕ t₄})
                 ⇔ ((assocr₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
      pentagon⊕r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → ((assocr₊ {t₁} {t₂} {t₃} ⊕ id↔ {t₄}) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
                 ⇔ assocr₊ ◎ assocr₊
      pentagon⊗l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → assocr⋆ ◎ (assocr⋆ {t₁} {t₂} {t₃ ⊗ t₄})
                 ⇔ ((assocr⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
      pentagon⊗r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → ((assocr⋆ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
                 ⇔ assocr⋆ ◎ assocr⋆
      -- from the braiding
      -- unit coherence
      unite₊l-coh-l : {t₁ : 𝕌ʷ} → unite₊l {t₁} ⇔ swap₊ ◎ unite₊r
      unite₊l-coh-r : {t₁ : 𝕌ʷ} → swap₊ ◎ unite₊r ⇔ unite₊l {t₁}
      unite⋆l-coh-l : {t₁ : 𝕌ʷ} → unite⋆l {t₁} ⇔ swap⋆ ◎ unite⋆r
      unite⋆l-coh-r : {t₁ : 𝕌ʷ} → swap⋆ ◎ unite⋆r ⇔ unite⋆l {t₁}
      hexagonr⊕l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
                 ⇔ ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
      hexagonr⊕r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
                 ⇔ (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
      hexagonl⊕l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
      hexagonl⊕r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
                 ⇔ (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
      hexagonr⊗l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
                 ⇔ ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
      hexagonr⊗r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
                 ⇔ (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
      hexagonl⊗l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
      hexagonl⊗r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
                 ⇔ (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
      absorbl⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → (c₁ ⊗ id↔ {𝟘}) ◎ absorbl ⇔ absorbl ◎ id↔ {𝟘}
      absorbl⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → absorbl ◎ id↔ {𝟘} ⇔ (c₁ ⊗ id↔ {𝟘}) ◎ absorbl
      absorbr⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → (id↔ {𝟘} ⊗ c₁) ◎ absorbr ⇔ absorbr ◎ id↔ {𝟘}
      absorbr⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → absorbr ◎ id↔ {𝟘} ⇔ (id↔ {𝟘} ⊗ c₁) ◎ absorbr
      factorzl⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzl ⇔ factorzl ◎ (id↔ ⊗ c₁)
      factorzl⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → factorzl ◎ (id↔ {𝟘} ⊗ c₁) ⇔ id↔ {𝟘} ◎ factorzl
      factorzr⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzr ⇔ factorzr ◎ (c₁ ⊗ id↔)
      factorzr⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → factorzr ◎ (c₁ ⊗ id↔) ⇔ id↔ ◎ factorzr
      -- from the coherence conditions of RigCategory
      swap₊distl⇔l : {t₁ t₂ t₃ : 𝕌ʷ}
                   → (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl ⇔ distl ◎ swap₊
      swap₊distl⇔r : {t₁ t₂ t₃ : 𝕌ʷ}
                   → distl ◎ swap₊ ⇔ (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl
      dist-swap⋆⇔l : {t₁ t₂ t₃ : 𝕌ʷ}
                   → dist {t₁} {t₂} {t₃} ◎ (swap⋆ ⊕ swap⋆) ⇔ swap⋆ ◎ distl
      dist-swap⋆⇔r : {t₁ t₂ t₃ : 𝕌ʷ}
                   → swap⋆ ◎ distl {t₁} {t₂} {t₃} ⇔ dist ◎ (swap⋆ ⊕ swap⋆)
      assocl₊-dist-dist⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                          → ((assocl₊ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ dist) ◎ (dist ⊕ id↔)
                          ⇔ (dist ◎ (id↔ ⊕ dist)) ◎ assocl₊
      assocl₊-dist-dist⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                          → (dist {t₁} ◎ (id↔ ⊕ dist {t₂} {t₃} {t₄})) ◎ assocl₊
                          ⇔ ((assocl₊ ⊗ id↔) ◎ dist) ◎ (dist ⊕ id↔)
      assocl⋆-distl⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                      → assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}
                      ⇔ ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
      assocl⋆-distl⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                      → ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
                      ⇔ assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}  
      absorbr0-absorbl0⇔ : absorbr {𝟘} ⇔ absorbl {𝟘}
      absorbl0-absorbr0⇔ : absorbl {𝟘} ⇔ absorbr {𝟘}
      absorbr⇔distl-absorb-unite : {t₁ t₂ : 𝕌ʷ}
                                 → absorbr ⇔ (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l
      distl-absorb-unite⇔absorbr : {t₁ t₂ : 𝕌ʷ}
                                 → (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l ⇔ absorbr
      unite⋆r0-absorbr1⇔ : unite⋆r ⇔ absorbr
      absorbr1-unite⋆r-⇔ : absorbr ⇔ unite⋆r
      absorbl≡swap⋆◎absorbr : {t₁ : 𝕌ʷ} → absorbl {t₁} ⇔ swap⋆ ◎ absorbr
      swap⋆◎absorbr≡absorbl : {t₁ : 𝕌ʷ} → swap⋆ ◎ absorbr ⇔ absorbl {t₁}
      absorbr⇔[assocl⋆◎[absorbr⊗id↔]]◎absorbr : {t₁ t₂ : 𝕌ʷ}
                                              → absorbr ⇔ (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr
      [assocl⋆◎[absorbr⊗id↔]]◎absorbr⇔absorbr : {t₁ t₂ : 𝕌ʷ}
                                              → (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr ⇔ absorbr
      [id↔⊗absorbr]◎absorbl⇔assocl⋆◎[absorbl⊗id↔]◎absorbr : {t₁ t₂ : 𝕌ʷ}
                                                          → (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
                                                          ⇔ (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
      assocl⋆◎[absorbl⊗id↔]◎absorbr⇔[id↔⊗absorbr]◎absorbl : {t₁ t₂ : 𝕌ʷ}
                                                          → (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
                                                          ⇔ (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
      elim⊥-A[0⊕B]⇔l : {t₁ t₂ : 𝕌ʷ}
                     → (id↔ {t₁} ⊗ unite₊l {t₂})
                     ⇔ (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
      elim⊥-A[0⊕B]⇔r : {t₁ t₂ : 𝕌ʷ}
                     → (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
                     ⇔ (id↔ {t₁} ⊗ unite₊l {t₂})
      elim⊥-1[A⊕B]⇔l : {t₁ t₂ : 𝕌ʷ}
                     → unite⋆l ⇔ distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂})
      elim⊥-1[A⊕B]⇔r : {t₁ t₂ : 𝕌ʷ} → distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂}) ⇔ unite⋆l
      fully-distribute⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                         → (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
                         ⇔ ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
      fully-distribute⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                         → ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
                         ⇔ (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
      -- make things easier
      cong! : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → (c₁ ⇔ c₂) → (! c₁ ⇔ ! c₂)
  
    ! : {t₁ t₂ : 𝕌ʷ} → t₁ ↔ t₂ → t₂ ↔ t₁
    ! unite₊l = uniti₊l
    ! uniti₊l = unite₊l
    ! unite₊r = uniti₊r
    ! uniti₊r = unite₊r
    ! swap₊ = swap₊
    ! assocl₊ = assocr₊
    ! assocr₊ = assocl₊
    ! unite⋆l = uniti⋆l
    ! uniti⋆l = unite⋆l
    ! unite⋆r = uniti⋆r
    ! uniti⋆r = unite⋆r
    ! swap⋆ = swap⋆
    ! assocl⋆ = assocr⋆
    ! assocr⋆ = assocl⋆
    ! absorbr = factorzl
    ! absorbl = factorzr
    ! factorzr = absorbl
    ! factorzl = absorbr
    ! dist = factor
    ! factor = dist
    ! distl = factorl
    ! factorl = distl
    ! id↔ = id↔
    ! (c₁ ◎ c₂) = ! c₂ ◎ ! c₁
    ! (c₁ ⊕ c₂) = ! c₁ ⊕ ! c₂
    ! (c₁ ⊗ c₂) = ! c₁ ⊗ ! c₂
    ! (wave c) = particle₁
    ! particle₁ = wave _
    ! (rewrite⇔ c₁⇔c₂) = rewrite⇔ (sym⇔ c₁⇔c₂)
    ! (c₁ ⊛ c₂) = (! c₁ ⊛ ! c₂) ◎ rewrite⇔ (trans⇔ assoc◎l
                                            (trans⇔ (assoc◎l ⊡ id⇔)
                                            (trans⇔ ((rinv◎l ⊡ id⇔) ⊡ id⇔)
                                            (trans⇔ (idl◎l ⊡ id⇔)
                                            (trans⇔ assoc◎r
                                            (trans⇔ (id⇔ ⊡ linv◎l) idr◎l))))))
\end{code}
\medskip

The \AgdaInductiveConstructor{swapʷ} combinator is its own inverse up
to a use of \AgdaInductiveConstructor{rewrite⇔}:

\medskip
\begin{code}
    ! swapʷ = swapʷ ◎ rewrite⇔ (   trans⇔ idl◎r
                                  (trans⇔ (linv◎r ⊡ id⇔)
                                  (trans⇔ assoc◎r
                                  (trans⇔ (id⇔ ⊡ linv◎l) idr◎l))))
\end{code}
\medskip

To enable the efficient implementation of
\AgdaInductiveConstructor{swapʷ}, we change the runtime representation
of the type $\fd{t_1}{c}{t_2}$. The representation $\wv{t_1}{t_2}$ has
as elements, the first component of the pair and all the relevant
combinators:

\medskip
\begin{code}
  mutual
    data ⟅_,_⟆  : (t₁ t₂ : 𝕌ʷ) → Set where
      [_↢_↣_⊙_] : {t₁ t₂ t₃ t₄ : 𝕌ʷ} →
                    (t₁ ↔ t₃) → ⟦ t₁ ⟧ʷ → (t₁ ↔ t₂) → (t₂ ↔ t₄) → ⟅ t₃ , t₄ ⟆ 
\end{code}

\begin{code}[hide]
    syntax 𝕎 t₁ t₂ c = ⟨ t₁ ⤙ c ⤚ t₂ ⟩

    ⟦_⟧ʷ : 𝕌ʷ → Set
    ⟦ 𝟘 ⟧ʷ = ⊥
    ⟦ 𝟙 ⟧ʷ = ⊤
    ⟦ t₁ ⊗ t₂ ⟧ʷ = ⟦ t₁ ⟧ʷ × ⟦ t₂ ⟧ʷ
    ⟦ t₁ ⊕ t₂ ⟧ʷ = ⟦ t₁ ⟧ʷ ⊎ ⟦ t₂ ⟧ʷ
\end{code}
\begin{code}
    ⟦ 𝕎 t₁ t₂ c ⟧ʷ = ⟅ t₁ , t₂ ⟆

\end{code}

\begin{code}[hide]
  {-# TERMINATING #-}
  interp𝕌ʷ : {t₁ t₂ : 𝕌ʷ} → (t₁ ↔ t₂) → ⟦ t₁ ⟧ʷ → ⟦ t₂ ⟧ʷ
  interp𝕌ʷ unite₊l (inj₁ ())
  interp𝕌ʷ unite₊l (inj₂ v) = v
  interp𝕌ʷ uniti₊l v = inj₂ v
  interp𝕌ʷ unite₊r (inj₁ v) = v
  interp𝕌ʷ unite₊r (inj₂ ())
  interp𝕌ʷ uniti₊r v = inj₁ v
  interp𝕌ʷ swap₊ (inj₁ v) = inj₂ v
  interp𝕌ʷ swap₊ (inj₂ v) = inj₁ v
  interp𝕌ʷ assocl₊ (inj₁ v) = inj₁ (inj₁ v)
  interp𝕌ʷ assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
  interp𝕌ʷ assocl₊ (inj₂ (inj₂ v)) = inj₂ v
  interp𝕌ʷ assocr₊ (inj₁ (inj₁ v)) = inj₁ v
  interp𝕌ʷ assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
  interp𝕌ʷ assocr₊ (inj₂ v) = inj₂ (inj₂ v)
  interp𝕌ʷ unite⋆l v = proj₂ v
  interp𝕌ʷ uniti⋆l v = tt , v
  interp𝕌ʷ unite⋆r v = proj₁ v
  interp𝕌ʷ uniti⋆r v = v , tt
  interp𝕌ʷ swap⋆ (v₁ , v₂) = v₂ , v₁
  interp𝕌ʷ assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
  interp𝕌ʷ assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
  interp𝕌ʷ absorbr (() , v)
  interp𝕌ʷ absorbl (v , ())
  interp𝕌ʷ factorzr ()
  interp𝕌ʷ factorzl ()
  interp𝕌ʷ dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
  interp𝕌ʷ dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
  interp𝕌ʷ factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
  interp𝕌ʷ factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
  interp𝕌ʷ distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
  interp𝕌ʷ distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
  interp𝕌ʷ factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
  interp𝕌ʷ factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
  interp𝕌ʷ id↔ v = v
  interp𝕌ʷ (c₁ ◎ c₂) v = interp𝕌ʷ c₂ (interp𝕌ʷ c₁ v)
  interp𝕌ʷ (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interp𝕌ʷ c₁ v)
  interp𝕌ʷ (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interp𝕌ʷ c₂ v)
  interp𝕌ʷ (c₁ ⊗ c₂) (v₁ , v₂) = interp𝕌ʷ c₁ v₁ , interp𝕌ʷ c₂ v₂
\end{code}

The interpreter clauses are then adjusted as follows:

\begin{code}
  interp𝕌ʷ (wave c) v                         = [ id↔ ↢ v ↣ c ⊙ id↔ ]
  interp𝕌ʷ particle₁ ([ c₁ ↢ v ↣ c ⊙ c₂ ])    = interp𝕌ʷ c₁ v
  interp𝕌ʷ swapʷ ([ c₁ ↢ v ↣ c ⊙ c₂ ])        = [ c₂ ↢ (interp𝕌ʷ c v) ↣ (! c) ⊙ c₁ ]
  interp𝕌ʷ (rewrite⇔ _) v                     = v
  interp𝕌ʷ (c₁' ⊛ c₂') ([ c₁ ↢ v ↣ c ⊙ c₂ ])  = [ (c₁ ◎ c₁') ↢ v ↣ c ⊙ (c₂ ◎ c₂') ]
\end{code}
\medskip 

In the first clause, the interpretation of
\AgdaInductiveConstructor{wave} creates a package including the value
$v$, the combinator~$c$, and two trivial computations on either
side. When we project the first component, the stored computation for
the first component is performed. If we want to project the second
component, we should swap instead: as the third clause shows, the
effect of swapping is to compute the other projection and exchange the
stored computations on each side. The interpretation of
\AgdaInductiveConstructor{rewrite⇔} is unchanged. Finally, the
interpretation of
\AgdaInductiveConstructor{\AgdaUnderscore{}⊛\AgdaUnderscore{}} does
not perform any computation, it just extends the stored combinators.

\end{AgdaMultiCode}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Groubits and Quantum Protocols}
\label{sec:gbit}

Although entanglement is arguably one of the peculiar characteristics
of quantum mechanics and although it has been considered as the reason
quantum computing might be more efficient than classical computing,
there has been arguments that entanglement is not an exclusively
quantum
phenomenon~\cite{PhysRevA.65.032321,Spreeuw1998,DBLP:conf/calco/ReutterV17}. The
Reutter and Vicary paper~\cite{DBLP:conf/calco/ReutterV17} is
particularly interesting from a programming language perspective as it
gives distills the aspects of entanglement needed to realize various
quantum communication protocols into a novel datatype of
\emph{groubits} axiomatized by five primitive operations.

Groubits were motivated by higher-category theory, specifically by
``monoidal dagger pivotal 2-categories.'' One instance of such
categories is the 2-category \textbf{2Hilb} whose morphisms are
matrices of Hilbert spaces. This is the standard way to model quantum
computation. Another instance of such categories is the 2-category
\textbf{GpdAct} whose objects are finite groupoids and whose morphisms
and 2-morphisms are functors and spans that are compatible with group
actions. A groubit is a particular binunitary in this category, which
means it is a morphism that is unitary under both vertical and
horizontal composition in the 2-category. As Rutter and Vicary show,
groubits can be used to model some aspects of entanglement. There is,
a priori, no reason to believe that such a datatype of groubits can be
defined in a conventional programming language but this is exactly
what we will do using $\Pi^{\mathbb{W}}$.


%%%
\subsection{The Groubit Definition}

A groubit is represented by two classical bits $(A_L, A_I)$: a logical
bit~$A_L$ and a internal bit~$A_I$. Only the logical bit can be
accessed directly; the internal bit can only be accessed after a swap
operation. This pattern is already reminiscent of the type
$\fd{t_1}{c}{t_2}$ with \AgdaInductiveConstructor{particle₁} directly
accessible and \AgdaFunction{particle₂} only accessible after a
swap. To be precise, a groubit implementation must support the
following five operations with the following specification:

\begin{enumerate}
\item \textbf{Init} = $(\textbf{Rand},0)$
\item \textbf{Write}$(B) = (B,\textbf{Rand})$
\item \textbf{Swap}$(A_L,A_I) = (A_I,A_L)$
\item \textbf{Read}$(A_L,A_I) = A_L$
\item \textbf{Tick}$((A_L,A_I),(B_L,B_I)) = ((A_L,A_I⊕B_L),(B_L,B_I⊕A_L))$
\end{enumerate}

A groubit can be created using either the \textbf{Init} or
\textbf{Write} operations. In the first case, the logical bit gets a
random value; in the second case, the internal bit gets a random
value. The \textbf{Swap} exchanges the two bits. The \textbf{Read}
operation returns the value of the logical bit. The most interesting
operation is the \textbf{Tick} operation that connects a pair of
groubits; its action is to flip the internal bit of each groubit just
when the other groubit's logical bit is 1. 

%%%
\subsection{Implementing the Groubit Datatype} 

We will model a groubit in $\Pi^{\mathbb{W}}$ as the following type:
\[
  \fd{\mathtt{2}}{\iso}{\mathbb{2}}  \oplus 
  \fd{\mathtt{2}}{\AgdaFunction{not}}{\mathbb{2}}  
\]
where the two summands model the two possible relations between the
logical and internal bit. 

With this model, we can directly implement the \textbf{Swap} and
\textbf{Tick} operations which model the \emph{evolution} of a
groubit.

Generally, and according to the postulates of quantum mechanics, the
evolution of a quantum system, is a unitary operation. In particular,
the evolution is completely reversible which is well-suited with
$\Pi^{\mathbb{W}}$. The \emph{preparation} of a quantum state and the
\emph{measurement} of a quantum state are however both
\emph{irreversible} operations. For the groubit model, these
irreversible operations are \textbf{Init}, \textbf{Read}, and
\textbf{Write}. In order to model these operations, we need to extend
$\Pi^{\mathbb{W}}$ with input-output, irreversible, operations.

Formally, we assume a global input $\mathbb{R}$ modeling a random
bit. We will also add two new primitive operations: an operation
\AgdaInductiveConstructor{const₀}, to model \textbf{Init}, that
returns a constant bit initialized to $\mathbb{0}_2$, and an
operation \AgdaInductiveConstructor{eraseInj}, to model \textbf{Read},
that forgets the particular injection (from random bit) into the sum type:

\begin{AgdaMultiCode}
\begin{code}[hide]
module Gbit where

  infix 70 _⊗_
  infix 60 _⊕_
  infix 40 _↔_
  infix 30 _⇔_
  infixr 50 _◎_
  infixr 50 _⊡_

  mutual
    data 𝕌ʷ : Set where
      𝟘 : 𝕌ʷ
      𝟙 : 𝕌ʷ
      _⊕_ : 𝕌ʷ → 𝕌ʷ → 𝕌ʷ
      _⊗_ : 𝕌ʷ → 𝕌ʷ → 𝕌ʷ
      𝕎 : (t₁ t₂ : 𝕌ʷ) → t₁ ↔ t₂ → 𝕌ʷ

    𝟚 : 𝕌ʷ
    𝟚 = 𝟙 ⊕ 𝟙
\end{code}

\medskip
\begin{code}
    ℝ : 𝕌ʷ
    ℝ = 𝟚
\end{code}
\begin{code}[hide]    
    data _↔_ : 𝕌ʷ → 𝕌ʷ → Set where
      unite₊l : {t : 𝕌ʷ} → 𝟘 ⊕ t ↔ t
      uniti₊l : {t : 𝕌ʷ} → t ↔ 𝟘 ⊕ t
      unite₊r : {t : 𝕌ʷ} → t ⊕ 𝟘 ↔ t
      uniti₊r : {t : 𝕌ʷ} → t ↔ t ⊕ 𝟘
      swap₊   : {t₁ t₂ : 𝕌ʷ} → t₁ ⊕ t₂ ↔ t₂ ⊕ t₁
      assocl₊ : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊕ (t₂ ⊕ t₃) ↔ (t₁ ⊕ t₂) ⊕ t₃
      assocr₊ : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊕ t₂) ⊕ t₃ ↔ t₁ ⊕ (t₂ ⊕ t₃)
      unite⋆l : {t : 𝕌ʷ} → 𝟙 ⊗ t ↔ t
      uniti⋆l : {t : 𝕌ʷ} → t ↔ 𝟙 ⊗ t
      unite⋆r : {t : 𝕌ʷ} → t ⊗ 𝟙 ↔ t
      uniti⋆r : {t : 𝕌ʷ} → t ↔ t ⊗ 𝟙
      swap⋆   : {t₁ t₂ : 𝕌ʷ} → t₁ ⊗ t₂ ↔ t₂ ⊗ t₁
      assocl⋆ : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊗ (t₂ ⊗ t₃) ↔ (t₁ ⊗ t₂) ⊗ t₃
      assocr⋆ : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊗ t₂) ⊗ t₃ ↔ t₁ ⊗ (t₂ ⊗ t₃)
      absorbr : {t : 𝕌ʷ} → 𝟘 ⊗ t ↔ 𝟘
      absorbl : {t : 𝕌ʷ} → t ⊗ 𝟘 ↔ 𝟘
      factorzr : {t : 𝕌ʷ} → 𝟘 ↔ t ⊗ 𝟘
      factorzl : {t : 𝕌ʷ} → 𝟘 ↔ 𝟘 ⊗ t
      dist    : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊕ t₂) ⊗ t₃ ↔ (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃)
      factor  : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ⊗ t₃) ⊕ (t₂ ⊗ t₃) ↔ (t₁ ⊕ t₂) ⊗ t₃
      distl   : {t₁ t₂ t₃ : 𝕌ʷ} → t₁ ⊗ (t₂ ⊕ t₃) ↔ (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃)
      factorl : {t₁ t₂ t₃ : 𝕌ʷ } → (t₁ ⊗ t₂) ⊕ (t₁ ⊗ t₃) ↔ t₁ ⊗ (t₂ ⊕ t₃)
      id↔     : {t : 𝕌ʷ} → t ↔ t
      _◎_     : {t₁ t₂ t₃ : 𝕌ʷ} → (t₁ ↔ t₂) → (t₂ ↔ t₃) → (t₁ ↔ t₃)
      _⊕_     : {t₁ t₂ t₃ t₄ : 𝕌ʷ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊕ t₂ ↔ t₃ ⊕ t₄)
      _⊗_     : {t₁ t₂ t₃ t₄ : 𝕌ʷ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ⊗ t₂ ↔ t₃ ⊗ t₄)
      wave : {t₁ t₂ : 𝕌ʷ} (c : t₁ ↔ t₂) → t₁ ↔ 𝕎 t₁ t₂ c
      particle₁ : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → 𝕎 t₁ t₂ c ↔ t₁
      rewrite⇔ : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → 𝕎 t₁ t₂ c₁ ↔ 𝕎 t₁ t₂ c₂
      _⊛_ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c : t₁ ↔ t₂} (c₁ : t₁ ↔ t₃) (c₂ : t₂ ↔ t₄) → 𝕎 t₁ t₂ c ↔ 𝕎 t₃ t₄ (! c₁ ◎ c ◎ c₂)
      !const₀   : 𝟚 ↔ 𝟙
      !eraseInj : {t : 𝕌ʷ} → t ↔ t ⊕ t
\end{code}
\begin{code}
      const₀    : 𝟙 ↔ 𝟚
      eraseInj  : {t : 𝕌ʷ} → t ⊕ t ↔ t
\end{code}
\begin{code}[hide]      
    data _⇔_ : {t₁ t₂ : 𝕌ʷ} → (t₁ ↔ t₂) → (t₁ ↔ t₂) → Set where
      assoc◎l : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → (c₁ ◎ (c₂ ◎ c₃)) ⇔ ((c₁ ◎ c₂) ◎ c₃)
      assoc◎r : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → ((c₁ ◎ c₂) ◎ c₃) ⇔ (c₁ ◎ (c₂ ◎ c₃))
      assocl⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊) ⇔ (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃))
      assocl⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃)) ⇔ ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊)
      assocl⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆) ⇔ (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃))
      assocl⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃)) ⇔ ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆)
      assocr⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊) ⇔ (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃)))
      assocr⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃))) ⇔ (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆)
      assocr⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆) ⇔ (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃)))
      assocr⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃))) ⇔ (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊)
      dist⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → ((a ⊕ b) ⊗ c) ◎ dist ⇔ dist ◎ ((a ⊗ c) ⊕ (b ⊗ c))
      dist⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → dist ◎ ((a ⊗ c) ⊕ (b ⊗ c)) ⇔ ((a ⊕ b) ⊗ c) ◎ dist
      distl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → (a ⊗ (b ⊕ c)) ◎ distl ⇔ distl ◎ ((a ⊗ b) ⊕ (a ⊗ c))
      distl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → distl ◎ ((a ⊗ b) ⊕ (a ⊗ c)) ⇔ (a ⊗ (b ⊕ c)) ◎ distl
      factor⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor ⇔ factor ◎ ((a ⊕ b) ⊗ c)
      factor⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → factor ◎ ((a ⊕ b) ⊗ c) ⇔ ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor
      factorl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl ⇔ factorl ◎ (a ⊗ (b ⊕ c))
      factorl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → factorl ◎ (a ⊗ (b ⊕ c)) ⇔ ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl
      idl◎l   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (id↔ ◎ c) ⇔ c
      idl◎r   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ id↔ ◎ c
      idr◎l   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (c ◎ id↔) ⇔ c
      idr◎r   : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ (c ◎ id↔) 
      linv◎l  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (c ◎ ! c) ⇔ id↔
      linv◎r  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → id↔ ⇔ (c ◎ ! c) 
      rinv◎l  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → (! c ◎ c) ⇔ id↔
      rinv◎r  : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → id↔ ⇔ (! c ◎ c) 
      unite₊l⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊l ◎ c₂) ⇔ ((c₁ ⊕ c₂) ◎ unite₊l)
      unite₊l⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊕ c₂) ◎ unite₊l) ⇔ (unite₊l ◎ c₂)
      uniti₊l⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊l ◎ (c₁ ⊕ c₂)) ⇔ (c₂ ◎ uniti₊l)
      uniti₊l⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊l) ⇔ (uniti₊l ◎ (c₁ ⊕ c₂))
      unite₊r⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊r ◎ c₂) ⇔ ((c₂ ⊕ c₁) ◎ unite₊r)
      unite₊r⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊕ c₁) ◎ unite₊r) ⇔ (unite₊r ◎ c₂)
      uniti₊r⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊r ◎ (c₂ ⊕ c₁)) ⇔ (c₂ ◎ uniti₊r)
      uniti₊r⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊r) ⇔ (uniti₊r ◎ (c₂ ⊕ c₁))
      swapl₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap₊ ◎ (c₁ ⊕ c₂)) ⇔ ((c₂ ⊕ c₁) ◎ swap₊)
      swapr₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊕ c₁) ◎ swap₊) ⇔ (swap₊ ◎ (c₁ ⊕ c₂))
      unitel⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆l ◎ c₂) ⇔ ((c₁ ⊗ c₂) ◎ unite⋆l)
      uniter⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊗ c₂) ◎ unite⋆l) ⇔ (unite⋆l ◎ c₂)
      unitil⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆l ◎ (c₁ ⊗ c₂)) ⇔ (c₂ ◎ uniti⋆l)
      unitir⋆⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆l) ⇔ (uniti⋆l ◎ (c₁ ⊗ c₂))
      unitel⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆r ◎ c₂) ⇔ ((c₂ ⊗ c₁) ◎ unite⋆r)
      uniter⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊗ c₁) ◎ unite⋆r) ⇔ (unite⋆r ◎ c₂)
      unitil⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆r ◎ (c₂ ⊗ c₁)) ⇔ (c₂ ◎ uniti⋆r)
      unitir⋆⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆r) ⇔ (uniti⋆r ◎ (c₂ ⊗ c₁))
      swapl⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap⋆ ◎ (c₁ ⊗ c₂)) ⇔ ((c₂ ⊗ c₁) ◎ swap⋆)
      swapr⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊗ c₁) ◎ swap⋆) ⇔ (swap⋆ ◎ (c₁ ⊗ c₂))
      id⇔     : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → c ⇔ c
      sym⇔    : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → c₂ ⇔ c₁
      trans⇔  : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ c₃ : t₁ ↔ t₂}
              → (c₁ ⇔ c₂) → (c₂ ⇔ c₃) → (c₁ ⇔ c₃)
      _⊡_  : {t₁ t₂ t₃ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₁ ↔ t₂} {c₄ : t₂ ↔ t₃}
           → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ◎ c₂) ⇔ (c₃ ◎ c₄)
      resp⊕⇔  : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊕ c₂) ⇔ (c₃ ⊕ c₄)
      resp⊗⇔  : {t₁ t₂ t₃ t₄ : 𝕌ʷ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊗ c₂) ⇔ (c₃ ⊗ c₄)
      -- below are the combinators added for the RigCategory structure
      id↔⊕id↔⇔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁} ⊕ id↔ {t₂}) ⇔ id↔
      split⊕-id↔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁ ⊕ t₂}) ⇔ (id↔ ⊕ id↔)
      hom⊕◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄)) ⇔ ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄))
      hom◎⊕⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄)) ⇔ ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄))
      id↔⊗id↔⇔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁} ⊗ id↔ {t₂}) ⇔ id↔
      split⊗-id↔ : {t₁ t₂ : 𝕌ʷ} → (id↔ {t₁ ⊗ t₂}) ⇔ (id↔ ⊗ id↔)
      hom⊗◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄)) ⇔ ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄))
      hom◎⊗⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ʷ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄)) ⇔ ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄))
      -- associativity triangle
      triangle⊕l : {t₁ t₂ : 𝕌ʷ} → (unite₊r {t₁} ⊕ id↔ {t₂}) ⇔ assocr₊ ◎ (id↔ ⊕ unite₊l)
      triangle⊕r : {t₁ t₂ : 𝕌ʷ} → assocr₊ ◎ (id↔ {t₁} ⊕ unite₊l {t₂}) ⇔ (unite₊r ⊕ id↔)
      triangle⊗l : {t₁ t₂ : 𝕌ʷ} → ((unite⋆r {t₁}) ⊗ id↔ {t₂}) ⇔ assocr⋆ ◎ (id↔ ⊗ unite⋆l)
      triangle⊗r : {t₁ t₂ : 𝕌ʷ} → (assocr⋆ ◎ (id↔ {t₁} ⊗ unite⋆l {t₂})) ⇔ (unite⋆r ⊗ id↔)
      pentagon⊕l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → assocr₊ ◎ (assocr₊ {t₁} {t₂} {t₃ ⊕ t₄})
                 ⇔ ((assocr₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
      pentagon⊕r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → ((assocr₊ {t₁} {t₂} {t₃} ⊕ id↔ {t₄}) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
                 ⇔ assocr₊ ◎ assocr₊
      pentagon⊗l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → assocr⋆ ◎ (assocr⋆ {t₁} {t₂} {t₃ ⊗ t₄})
                 ⇔ ((assocr⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
      pentagon⊗r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                 → ((assocr⋆ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
                 ⇔ assocr⋆ ◎ assocr⋆
      -- from the braiding
      -- unit coherence
      unite₊l-coh-l : {t₁ : 𝕌ʷ} → unite₊l {t₁} ⇔ swap₊ ◎ unite₊r
      unite₊l-coh-r : {t₁ : 𝕌ʷ} → swap₊ ◎ unite₊r ⇔ unite₊l {t₁}
      unite⋆l-coh-l : {t₁ : 𝕌ʷ} → unite⋆l {t₁} ⇔ swap⋆ ◎ unite⋆r
      unite⋆l-coh-r : {t₁ : 𝕌ʷ} → swap⋆ ◎ unite⋆r ⇔ unite⋆l {t₁}
      hexagonr⊕l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
                 ⇔ ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
      hexagonr⊕r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
                 ⇔ (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
      hexagonl⊕l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
      hexagonl⊕r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
                 ⇔ (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
      hexagonr⊗l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
                 ⇔ ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
      hexagonr⊗r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
                 ⇔ (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
      hexagonl⊗l : {t₁ t₂ t₃ : 𝕌ʷ}
                 → (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
      hexagonl⊗r : {t₁ t₂ t₃ : 𝕌ʷ}
                 → ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
                 ⇔ (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
      absorbl⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → (c₁ ⊗ id↔ {𝟘}) ◎ absorbl ⇔ absorbl ◎ id↔ {𝟘}
      absorbl⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → absorbl ◎ id↔ {𝟘} ⇔ (c₁ ⊗ id↔ {𝟘}) ◎ absorbl
      absorbr⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → (id↔ {𝟘} ⊗ c₁) ◎ absorbr ⇔ absorbr ◎ id↔ {𝟘}
      absorbr⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                → absorbr ◎ id↔ {𝟘} ⇔ (id↔ {𝟘} ⊗ c₁) ◎ absorbr
      factorzl⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzl ⇔ factorzl ◎ (id↔ ⊗ c₁)
      factorzl⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → factorzl ◎ (id↔ {𝟘} ⊗ c₁) ⇔ id↔ {𝟘} ◎ factorzl
      factorzr⇔l : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzr ⇔ factorzr ◎ (c₁ ⊗ id↔)
      factorzr⇔r : {t₁ t₂ : 𝕌ʷ} {c₁ : t₁ ↔ t₂}
                 → factorzr ◎ (c₁ ⊗ id↔) ⇔ id↔ ◎ factorzr
      -- from the coherence conditions of RigCategory
      swap₊distl⇔l : {t₁ t₂ t₃ : 𝕌ʷ}
                   → (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl ⇔ distl ◎ swap₊
      swap₊distl⇔r : {t₁ t₂ t₃ : 𝕌ʷ}
                   → distl ◎ swap₊ ⇔ (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl
      dist-swap⋆⇔l : {t₁ t₂ t₃ : 𝕌ʷ}
                   → dist {t₁} {t₂} {t₃} ◎ (swap⋆ ⊕ swap⋆) ⇔ swap⋆ ◎ distl
      dist-swap⋆⇔r : {t₁ t₂ t₃ : 𝕌ʷ}
                   → swap⋆ ◎ distl {t₁} {t₂} {t₃} ⇔ dist ◎ (swap⋆ ⊕ swap⋆)
      assocl₊-dist-dist⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                          → ((assocl₊ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ dist) ◎ (dist ⊕ id↔)
                          ⇔ (dist ◎ (id↔ ⊕ dist)) ◎ assocl₊
      assocl₊-dist-dist⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                          → (dist {t₁} ◎ (id↔ ⊕ dist {t₂} {t₃} {t₄})) ◎ assocl₊
                          ⇔ ((assocl₊ ⊗ id↔) ◎ dist) ◎ (dist ⊕ id↔)
      assocl⋆-distl⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                      → assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}
                      ⇔ ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
      assocl⋆-distl⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                      → ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
                      ⇔ assocl⋆ {t₁} {t₂} ◎ distl {t₁ ⊗ t₂} {t₃} {t₄}  
      absorbr0-absorbl0⇔ : absorbr {𝟘} ⇔ absorbl {𝟘}
      absorbl0-absorbr0⇔ : absorbl {𝟘} ⇔ absorbr {𝟘}
      absorbr⇔distl-absorb-unite : {t₁ t₂ : 𝕌ʷ}
                                 → absorbr ⇔ (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l
      distl-absorb-unite⇔absorbr : {t₁ t₂ : 𝕌ʷ}
                                 → (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l ⇔ absorbr
      unite⋆r0-absorbr1⇔ : unite⋆r ⇔ absorbr
      absorbr1-unite⋆r-⇔ : absorbr ⇔ unite⋆r
      absorbl≡swap⋆◎absorbr : {t₁ : 𝕌ʷ} → absorbl {t₁} ⇔ swap⋆ ◎ absorbr
      swap⋆◎absorbr≡absorbl : {t₁ : 𝕌ʷ} → swap⋆ ◎ absorbr ⇔ absorbl {t₁}
      absorbr⇔[assocl⋆◎[absorbr⊗id↔]]◎absorbr : {t₁ t₂ : 𝕌ʷ}
                                              → absorbr ⇔ (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr
      [assocl⋆◎[absorbr⊗id↔]]◎absorbr⇔absorbr : {t₁ t₂ : 𝕌ʷ}
                                              → (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr ⇔ absorbr
      [id↔⊗absorbr]◎absorbl⇔assocl⋆◎[absorbl⊗id↔]◎absorbr : {t₁ t₂ : 𝕌ʷ}
                                                          → (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
                                                          ⇔ (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
      assocl⋆◎[absorbl⊗id↔]◎absorbr⇔[id↔⊗absorbr]◎absorbl : {t₁ t₂ : 𝕌ʷ}
                                                          → (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
                                                          ⇔ (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
      elim⊥-A[0⊕B]⇔l : {t₁ t₂ : 𝕌ʷ}
                     → (id↔ {t₁} ⊗ unite₊l {t₂})
                     ⇔ (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
      elim⊥-A[0⊕B]⇔r : {t₁ t₂ : 𝕌ʷ}
                     → (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
                     ⇔ (id↔ {t₁} ⊗ unite₊l {t₂})
      elim⊥-1[A⊕B]⇔l : {t₁ t₂ : 𝕌ʷ}
                     → unite⋆l ⇔ distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂})
      elim⊥-1[A⊕B]⇔r : {t₁ t₂ : 𝕌ʷ} → distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂}) ⇔ unite⋆l
      fully-distribute⇔l : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                         → (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
                         ⇔ ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
      fully-distribute⇔r : {t₁ t₂ t₃ t₄ : 𝕌ʷ}
                         → ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
                         ⇔ (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
      -- make things easier
      cong! : {t₁ t₂ : 𝕌ʷ} {c₁ c₂ : t₁ ↔ t₂} → (c₁ ⇔ c₂) → (! c₁ ⇔ ! c₂)
  
    ! : {t₁ t₂ : 𝕌ʷ} → t₁ ↔ t₂ → t₂ ↔ t₁
    ! unite₊l = uniti₊l
    ! uniti₊l = unite₊l
    ! unite₊r = uniti₊r
    ! uniti₊r = unite₊r
    ! swap₊ = swap₊
    ! assocl₊ = assocr₊
    ! assocr₊ = assocl₊
    ! unite⋆l = uniti⋆l
    ! uniti⋆l = unite⋆l
    ! unite⋆r = uniti⋆r
    ! uniti⋆r = unite⋆r
    ! swap⋆ = swap⋆
    ! assocl⋆ = assocr⋆
    ! assocr⋆ = assocl⋆
    ! absorbr = factorzl
    ! absorbl = factorzr
    ! factorzr = absorbl
    ! factorzl = absorbr
    ! dist = factor
    ! factor = dist
    ! distl = factorl
    ! factorl = distl
    ! id↔ = id↔
    ! (c₁ ◎ c₂) = ! c₂ ◎ ! c₁
    ! (c₁ ⊕ c₂) = ! c₁ ⊕ ! c₂
    ! (c₁ ⊗ c₂) = ! c₁ ⊗ ! c₂
    ! (wave c) = particle₁
    ! particle₁ = wave _
    ! (rewrite⇔ c₁⇔c₂) = rewrite⇔ (sym⇔ c₁⇔c₂)
    ! (c₁ ⊛ c₂) = (! c₁ ⊛ ! c₂) ◎ rewrite⇔ (trans⇔ assoc◎l
                                            (trans⇔ (assoc◎l ⊡ id⇔)
                                            (trans⇔ ((rinv◎l ⊡ id⇔) ⊡ id⇔)
                                            (trans⇔ (idl◎l ⊡ id⇔)
                                            (trans⇔ assoc◎r
                                            (trans⇔ (id⇔ ⊡ linv◎l) idr◎l))))))
    ! !const₀ = !eraseInj
    ! !eraseInj = eraseInj
    ! const₀ = !const₀
    ! eraseInj = !eraseInj

  syntax 𝕎 t₁ t₂ c = ⟨ t₁ ⤙ c ⤚ t₂ ⟩

  not : 𝟚 ↔ 𝟚
  not = swap₊
\end{code}
\end{AgdaMultiCode}

\begin{AgdaMultiCode}
\begin{code}[hide]
  swapʷ : {t₁ t₂ : 𝕌ʷ} {c : t₁ ↔ t₂} → 𝕎 t₁ t₂ c ↔ 𝕎 t₂ t₁ (! c)
  swapʷ {t₁} {t₂} {c} = (c ⊛ ! c) ◎ rewrite⇔ (trans⇔ (id⇔ ⊡ linv◎l) idr◎l)

  mutual
    data ⟅_,_⟆ : (t₁ t₂ : 𝕌ʷ) → Set where
      _ʷ : {t₁ t₂ : 𝕌ʷ} → ⟦ t₁ ⟧ʷ → ⟅ t₁ , t₂ ⟆ 
      
    ⟦_⟧ʷ : 𝕌ʷ → Set
    ⟦ 𝟘 ⟧ʷ = ⊥
    ⟦ 𝟙 ⟧ʷ = ⊤
    ⟦ t₁ ⊗ t₂ ⟧ʷ = ⟦ t₁ ⟧ʷ × ⟦ t₂ ⟧ʷ
    ⟦ t₁ ⊕ t₂ ⟧ʷ = ⟦ t₁ ⟧ʷ ⊎ ⟦ t₂ ⟧ʷ
    ⟦ 𝕎 t₁ t₂ c ⟧ʷ = ⟅ t₁ , t₂ ⟆

    0₂ 1₂ : ⟦ 𝟚 ⟧ʷ
    0₂ = inj₁ tt
    1₂ = inj₂ tt
    
  interp𝕌ʷ : {t₁ t₂ : 𝕌ʷ} → (t₁ ↔ t₂) → ⟦ t₁ ⟧ʷ → ⟦ t₂ ⟧ʷ
  interp𝕌ʷ (wave c) v = v ʷ
  interp𝕌ʷ (c₁ ⊛ c₂) (v ʷ) = interp𝕌ʷ c₁ v ʷ
  interp𝕌ʷ particle₁ (v ʷ) = v
  interp𝕌ʷ (rewrite⇔ _) v = v
  interp𝕌ʷ unite₊l (inj₁ ())
  interp𝕌ʷ unite₊l (inj₂ v) = v
  interp𝕌ʷ uniti₊l v = inj₂ v
  interp𝕌ʷ unite₊r (inj₁ v) = v
  interp𝕌ʷ unite₊r (inj₂ ())
  interp𝕌ʷ uniti₊r v = inj₁ v
  interp𝕌ʷ swap₊ (inj₁ v) = inj₂ v
  interp𝕌ʷ swap₊ (inj₂ v) = inj₁ v
  interp𝕌ʷ assocl₊ (inj₁ v) = inj₁ (inj₁ v)
  interp𝕌ʷ assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
  interp𝕌ʷ assocl₊ (inj₂ (inj₂ v)) = inj₂ v
  interp𝕌ʷ assocr₊ (inj₁ (inj₁ v)) = inj₁ v
  interp𝕌ʷ assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
  interp𝕌ʷ assocr₊ (inj₂ v) = inj₂ (inj₂ v)
  interp𝕌ʷ unite⋆l v = proj₂ v
  interp𝕌ʷ uniti⋆l v = tt , v
  interp𝕌ʷ unite⋆r v = proj₁ v
  interp𝕌ʷ uniti⋆r v = v , tt
  interp𝕌ʷ swap⋆ (v₁ , v₂) = v₂ , v₁
  interp𝕌ʷ assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
  interp𝕌ʷ assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
  interp𝕌ʷ absorbr (() , v)
  interp𝕌ʷ absorbl (v , ())
  interp𝕌ʷ factorzr ()
  interp𝕌ʷ factorzl ()
  interp𝕌ʷ dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
  interp𝕌ʷ dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
  interp𝕌ʷ factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
  interp𝕌ʷ factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
  interp𝕌ʷ distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
  interp𝕌ʷ distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
  interp𝕌ʷ factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
  interp𝕌ʷ factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
  interp𝕌ʷ id↔ v = v
  interp𝕌ʷ (c₁ ◎ c₂) v = interp𝕌ʷ c₂ (interp𝕌ʷ c₁ v)
  interp𝕌ʷ (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interp𝕌ʷ c₁ v)
  interp𝕌ʷ (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interp𝕌ʷ c₂ v)
  interp𝕌ʷ (c₁ ⊗ c₂) (v₁ , v₂) = interp𝕌ʷ c₁ v₁ , interp𝕌ʷ c₂ v₂
  interp𝕌ʷ !const₀ v = tt
  interp𝕌ʷ !eraseInj v = inj₁ v
\end{code}

\medskip
\begin{code}
  interp𝕌ʷ const₀ _           = 0₂
  interp𝕌ʷ eraseInj (inj₁ v)  = v
  interp𝕌ʷ eraseInj (inj₂ v)  = v
\end{code}
\end{AgdaMultiCode}
\begin{AgdaMultiCode}

\medskip
Given these new operations, we can model all five required operations on groubits: 

\medskip
\begin{code}  
  𝔾bit : 𝕌ʷ
  𝔾bit = ⟨ 𝟚 ⤙ id↔ ⤚ 𝟚 ⟩ ⊕ ⟨ 𝟚 ⤙ not ⤚ 𝟚 ⟩
\end{code}
\medskip
\begin{enumerate}
\item \textbf{Init} takes a random bit and uses \AgdaFunction{const₀}
to construct a groubit:
\medskip
\begin{code}
  Init : ℝ ↔ 𝔾bit
  Init = uniti⋆l ◎ distl ◎ ( (unite⋆l ◎ const₀ ◎ wave _ ◎ swapʷ)
                           ⊕ (unite⋆l ◎ const₀ ◎ wave _ ◎ swapʷ))
\end{code}
\medskip
\item \textbf{Swap} just uses the \AgdaFunction{swapʷ} operation we
built in Sec.~\ref{sec:dpair}:
\medskip
\begin{code}
  Swap : 𝔾bit ↔ 𝔾bit
  Swap = swapʷ ⊕ swapʷ
\end{code}
\medskip
\item \textbf{Read} uses \AgdaFunction{eraseInj} to destroy a groubit
and return a classical bit:
\medskip
\begin{code}
  Read : 𝔾bit ↔ 𝟚
  Read = (particle₁ ⊕ particle₁) ◎ eraseInj
\end{code}
\medskip
\item \textbf{Write} takes a random bit and a classical bit and constructs a groubit: 
\medskip
\begin{code}
  Write : 𝟚 ⊗ ℝ ↔ 𝔾bit
  Write = distl ◎ ( (unite⋆r ◎ (wave id↔))
                  ⊕ (unite⋆r ◎ (wave not)))
\end{code}
\medskip
\item \textbf{Tick} is the most complex one, first we construct \AgdaFunction{CNOT}
      which will flip the internal bit according to the control bit (\AgdaFunction{𝟚}).
\medskip
\begin{code}
  CNOT : 𝔾bit ⊗ 𝟚 ↔ 𝔾bit ⊗ 𝟚
  CNOT = distl
       ◎ ( id↔                                    -- no flip
         ⊕ ( ( ((id↔ ⊛ swap₊) ⊕ (id↔ ⊛ swap₊))    -- flip internal bit
             ◎ ( (rewrite⇔ (trans⇔ idl◎l idl◎l))
               ⊕ (rewrite⇔ (trans⇔ idl◎l linv◎l)))
             ◎ swap₊) 
           ⊗ id↔)) 
       ◎ factorl
\end{code}
\medskip
Using \AgdaFunction{CNOT} we can construct \textbf{Tick}.
For the input \AgdaFunction{𝔾bit} $A$ and $B$:
\begin{enumerate}
\item We distribute $B$ over $A$ so we obtain a value of type
      (\AgdaFunction{𝔾bit} \timeu \wc{𝟚}{id↔}{𝟚}) \plusu (\timeu \AgdaFunction{𝔾bit} \timeu \wc{𝟚}{not}{𝟚}).
\item Take projection of $B$ and use \AgdaFunction{CNOT} to flip the internal bit of $A$,
  then use \AgdaInductiveConstructor{wave} to rebuild $B$.
\item Factor to get back two \AgdaFunction{𝔾bit}s.
\item Distribute $A$ over $B$.
\item Take projection of $A$ and use \AgdaFunction{CNOT} to flip the internal bit of $B$,
  then use \AgdaInductiveConstructor{wave} to rebuild $A$.
\item Factor to get back two \AgdaFunction{𝔾bit}s.
\end{enumerate}
\medskip
\begin{code}
  Tick : 𝔾bit ⊗ 𝔾bit ↔ 𝔾bit ⊗ 𝔾bit
  Tick = distl
       ◎ ((id↔ ⊗ particle₁) ⊕ (id↔ ⊗ particle₁))  -- project B
       ◎ (CNOT ⊕ CNOT)
       ◎ ((id↔ ⊗ wave id↔) ⊕ (id↔ ⊗ wave not))    -- rebuild B
       ◎ factorl
       ◎ dist
       ◎ ((particle₁ ⊗ id↔) ⊕ (particle₁ ⊗ id↔))  -- project A
       ◎ (swap⋆ ⊕ swap⋆)
       ◎ (CNOT ⊕ CNOT)
       ◎ (swap⋆ ⊕ swap⋆)
       ◎ ((wave id↔ ⊗ id↔) ⊕ (wave not ⊗ id↔))    -- rebuild A
       ◎ factor
\end{code}
\end{enumerate}

It is straightforward to check that the above implementations satisfy
the desired specification of groubits. 

%%%
\subsection{Quantum Protocols} 

Once groubits are implemented, a variety of quantum protocols can be
realized. We briefly outline the implementation of the superdense
coding protocol and the teleportation protocol.\\

First, we build some simple derived operations in the paper~\cite{DBLP:conf/calco/ReutterV17}.
\medskip
\begin{code}
  CTick : 𝟚 ⊗ 𝔾bit ↔ 𝟚 ⊗ 𝔾bit
  CTick = swap⋆ ◎ CNOT ◎ swap⋆

  IRead : 𝔾bit ↔ 𝟚
  IRead = Swap ◎ Read 

  Entangle : ℝ ⊗ ℝ ↔ 𝔾bit ⊗ 𝔾bit
  Entangle = (Init ⊗ Init) ◎ Tick ◎ (id↔ ⊗ Swap)
\end{code}
\medskip
\AgdaFunction{CTick} is just \AgdaFunction{CNOT} with swap,
\AgdaFunction{IRead} reads the internal bit, and
\AgdaFunction{Entangle} uses two random bits to create a pair of
\AgdaFunction{𝔾bit}s with exactly the same state.\par

Now we can build the superdense coding protocol~\cite{PhysRevLett.69.2881} which is a protocol by which is it possible to transmit two classical bits by sending only one qubit.
Our implementation of superdense coding using groubits~\cite{DBLP:conf/calco/ReutterV17} works
as follows. 
\begin{enumerate}
\item Alice begins with two classical bits;
\item Use \AgdaFunction{Entangle} to create an entangled pair of groubits and give one to Alice and one to Bob.
\item Alice performs \AgdaFunction{CTick} with the groubit she received using her two classical bits.
\item Alice sends the resulting groubit to Bob.
\item Bob performs \AgdaFunction{Tick} and then uses \AgdaFunction{IRead} to recovery the two classical bis.
\end{enumerate}
\begin{center}
\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=0.7]
%uncomment if require: \path (0,300); %set diagram left start at 0, and has height of 300

\draw    (60,290) -- (90,290) ;


\draw    (60,260) -- (90,260) ;


\draw    (90, 250) rectangle (190, 300)   ;
\draw    (60,190) -- (240,190) ;


\draw    (60,170) -- (320,170) ;


\draw    (240, 180) rectangle (300, 220)   ;
\draw    [line width=0.5mm, red ](210,210) -- (240,210) ;


\draw    [line width=0.5mm, red ](210,210) -- (210,260) ;


\draw    [line width=0.5mm, red ](190,260) -- (210,260) ;


\draw    [line width=0.5mm, red ](190,290) -- (520,290) ;


\draw    (300,190) -- (320,190) ;


\draw    (320,170) -- (400,190) ;


\draw    (320,190) -- (400,170) ;


\draw    [line width=0.5mm, red ](380,210) -- (400,210) ;


\draw    (400, 180) rectangle (460, 220)   ;
\draw    (400,170) -- (480,170) ;


\draw    (460,190) -- (480,190) ;


\draw    [line width=0.5mm, red ](460,210) -- (480,210) ;


\draw    [line width=0.5mm, red ](480,210) -- (480,260) ;


\draw    [line width=0.5mm, red ](480,260) -- (520,260) ;


\draw    (510,190) -- (480,170) ;


\draw    (480,190) -- (510,170) ;


\draw    (510,170) -- (660,170) ;


\draw    (520, 250) rectangle (560, 300)   ;
\draw    [line width=0.5mm, red ](560,260) -- (580,260) ;


\draw    (580, 250) rectangle (640, 270)   ;
\draw    (580, 280) rectangle (640, 300)   ;
\draw    [line width=0.5mm, red ](560,290) -- (580,290) ;


\draw    (640,260) -- (660,260) ;


\draw    (640,290) -- (660,290) ;


\draw    (510,190) -- (660,190) ;


\draw    (320, 200) rectangle (380, 220)   ;
\draw    [line width=0.5mm, red ](300,210) -- (320,210) ;

\draw[line width=0.4mm,dotted] (70,155) -- (70,320) ;
\draw[line width=0.4mm,dotted] (220,155) -- (220,320) ;
\draw[line width=0.4mm,dotted] (463,155) -- (463,320) ;
\draw[line width=0.4mm,dotted] (487,155) -- (487,320) ;

\draw (270,200) node  [align=left] {CTick};
\draw (140,275) node  [align=left] {Entangle};
\draw (430,200) node  [align=left] {CTick};
\draw (40,170) node  [align=left] {$b_0$};
\draw (40,190) node  [align=left] {$b_1$};
\draw (680,170) node  [align=left] {$b_0$};
\draw (680,190) node  [align=left] {$b_1$};
\draw (540,275) node  [align=left] {Tick};
\draw (610,260) node  [align=left] {IRead};
\draw (610,290) node  [align=left] {IRead};
\draw (680,260) node  [align=left] {$b_0$};
\draw (680,290) node  [align=left] {$b_1$};
\draw (40,260) node  [align=left] {$r_0$};
\draw (40,290) node  [align=left] {$r_1$};
\draw (350,210) node  [align=left] {Swap};

\draw (40,150) node  [align=left] {(1)};
\draw (160,150) node  [align=left] {(2)};
\draw (340,150) node  [align=left] {(3)};
\draw (475,150) node  [align=left] {(4)};
\draw (550,150) node  [align=left] {(5)};
\end{tikzpicture}
\end{center}
\medskip
\begin{code}
  densecoding : (𝟚 ⊗ 𝟚) ⊗ (ℝ ⊗ ℝ) ↔ (𝟚 ⊗ 𝟚) ⊗ (𝟚 ⊗ 𝟚)
  densecoding = (id↔ ⊗ Entangle) ◎ assocl⋆ ◎ (assocr⋆ ⊗ id↔)                     -- step 2
              ◎ ((id↔ ⊗ CTick) ⊗ id↔) ◎ (assocl⋆ ⊗ id↔) ◎ ((swap⋆ ⊗ Swap) ⊗ id↔) -- step 3
              ◎ (assocr⋆ ⊗ id↔) ◎ ((id↔ ⊗ CTick) ⊗ id↔) ◎ (assocl⋆ ⊗ id↔)
              ◎ assocr⋆                                                          -- step 4
              ◎ (swap⋆ ⊗ Tick) ◎ (id↔ ⊗ (IRead ⊗ IRead))                         -- step 5
\end{code}
\medskip
The code directly follows the structure of the circuit. 


Contrary to superdense coding, quantum
teleportation~\cite{PhysRevLett.70.1895} uses two classical bits to
teleport a qubit to the other side.  The teleported qubit is destroyed
on Alice's side.  Our implementation of teleportation using
groubits~\cite{DBLP:conf/calco/ReutterV17} works as follows. 
\begin{enumerate} 
\item Alice
begins with one groubit
\item Use \AgdaFunction{Entangle} to create an entangled pair of groubits and give one to Alice and one to Bob.
\item Alice performs \AgdaFunction{Tick}
on her two groubits, and then uses \AgdaFunction{IRead}.  
\item Alice
sends the two classical bits to Bob.  
\item Bob performs
\AgdaFunction{CTick} with his groubit and the two received classical
bits.  After this, his groubit has exactly the same state as Alice's
original groubit.
\end{enumerate} 
\begin{center}

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=0.7]
%uncomment if require: \path (0,300); %set diagram left start at 0, and has height of 300

\draw    (60, 250) rectangle (180, 290)   ;
\draw    (40,260) -- (60,260) ;


\draw    (40,280) -- (60,280) ;


\draw    [line width=0.5mm, red ](190,260) -- (180,260) ;


\draw    [line width=0.5mm, red ](180,280) -- (400,280) ;


\draw    [line width=0.5mm, red ](40,180) -- (210,180) ;


\draw    [line width=0.5mm, red ](190,210) -- (190,260) ;


\draw    (210, 170) rectangle (270, 220)   ;
\draw    [line width=0.5mm, red ](210,210) -- (190,210) ;


\draw    (300, 170) rectangle (360, 190)   ;
\draw    (300, 200) rectangle (360, 220)   ;
\draw    [line width=0.5mm, red ](270,180) -- (300,180) ;


\draw    [line width=0.5mm, red ](270,210) -- (300,210) ;


\draw    (360,180) -- (380,180) ;


\draw    (360,210) -- (370,210) ;


\draw    (370,260) -- (370,210) ;


\draw    (380,180) -- (380,240) ;


\draw    (370,260) -- (400,260) ;


\draw    (380,240) -- (470,240) ;


\draw    (400, 250) rectangle (460, 290)   ;
\draw    (460,260) -- (470,260) ;


\draw    [line width=0.5mm, red ](470,280) -- (460,280) ;


\draw    (470, 270) rectangle (510, 290)   ;
\draw    (510,240) -- (470,260) ;


\draw    (470,240) -- (510,260) ;


\draw    (520, 250) rectangle (580, 290)   ;
\draw    (510,240) -- (590,240) ;


\draw    (510,260) -- (520,260) ;


\draw    [line width=0.5mm, red ](510,280) -- (520,280) ;


\draw    (580,260) -- (590,260) ;


\draw    [line width=0.5mm, red ](580,280) -- (630,280) ;


\draw    (590,240) -- (610,260) ;


\draw    (590,260) -- (610,240) ;


\draw    (630,240) -- (610,240) ;


\draw    (630,260) -- (610,260) ;



\draw (120,270) node  [align=left] {Entangle};
\draw (240,195) node  [align=left] {Tick};
\draw (330,180) node  [align=left] {IRead};
\draw (330,210) node  [align=left] {IRead};
\draw (430,270) node  [align=left] {CTick};
\draw (30,180) node  [align=left] {$\mathbb{G}_0$};
\draw (30,260) node  [align=left] {$r_0$};
\draw (30,280) node  [align=left] {$r_1$};
\draw (490,280) node  [align=left] {Swap};
\draw (550,270) node  [align=left] {CTick};
\draw (640,280) node  [align=left] {$\mathbb{G}_0$};

\draw[line width=0.4mm,dotted] (50,160) -- (50,300) ;
\draw[line width=0.4mm,dotted] (200,160) -- (200,300) ;
\draw[line width=0.4mm,dotted] (365,160) -- (365,300) ;
\draw[line width=0.4mm,dotted] (390,160) -- (390,300) ;
\draw (30,160) node  [align=left] {(1)};
\draw (140,160) node  [align=left] {(2)};
\draw (280,160) node  [align=left] {(3)};
\draw (377,160) node  [align=left] {(4)};
\draw (500,160) node  [align=left] {(5)};

\end{tikzpicture}
\end{center}
\begin{code}
  teleport : 𝔾bit ⊗ (ℝ ⊗ ℝ) ↔ (𝟚 ⊗ 𝟚) ⊗ 𝔾bit
  teleport = (id↔ ⊗ Entangle) ◎ assocl⋆                        -- step 2
           ◎ (Tick ⊗ id↔) ◎ ((IRead ⊗ IRead) ⊗ id↔)            -- step 3
           ◎ assocr⋆                                           -- step 4
           ◎ (id↔ ⊗ CTick) ◎ assocl⋆ ◎ (swap⋆ ⊗ Swap)          -- step 5
           ◎ assocr⋆ ◎ (id↔ ⊗ CTick) ◎ assocl⋆ ◎ (swap⋆ ⊗ id↔)
\end{code}
\end{AgdaMultiCode}
\bigskip

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion} 
\label{sec:con}

Our model of groubits in $\Pi^{\mathbb{W}}$ reinforces previous
evidence~\cite{PhysRevA.65.032321,Spreeuw1998,DBLP:conf/calco/ReutterV17}
that entanglement is not exclusively a quantum phenomenon. Yet
entanglement appears a fundamental quantum notion as entanglement is
\emph{necessary} for any quantum algorithm to offer exponential
speedups over a classical one~\cite{Jozsa2011}.  One way to reconcile
these facts, and to better understand the source of the claimed
quantum speedups, is to study and compare the complexity of classical
vs quantum interpreters over the dependent type
$\fd{t_1}{c}{t_2}$. More generally, it is more and more evident that
the edifice of quantum computing shares a lot more with classical
computing than was perhaps originally believed. Our thesis is that
teasing the common parts required, and will continue to require,
advances and new perspectives on classical computing, especially in
the fields of reversible computing, higher-category theory, and
homotopy type theory. Indeed Baez and Stay advocate for a related
thesis that higher-order category unifies some critical aspects of
physics, topology, logic, and computation~\cite{baez2011physics}, and
hence is a fruitful arena for the exploration of the classical-quantum
boundary~\cite{Qian:15}.

On the more traditional side of programming languages, it appears that
our dependent type $\fd{t_1}{c}{t_2}$ could be useful for modeling
various kind of ``functional dependencies'' that arise in
spreadsheets, databases, programming languages, and program analysis
frameworks. Having such dependencies represented using first-class
constructs with a precise semantics within the language would bring
them under direct programmer control and should enable more robust
transformations and analyses.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{cites}
\end{document}
