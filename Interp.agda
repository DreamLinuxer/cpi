module Interp where
open import Data.Bool
open import Data.Empty
open import Data.Unit
open import Data.Sum
open import Data.Product
open import Function
open import Category.Monad
open import Category.Monad.Identity
open import Category.Monad.Indexed
open import Category.Monad.State
open import Relation.Binary.PropositionalEquality

data 𝕌 : Set where
  𝟘 : 𝕌
  𝟙 : 𝕌
  𝕄 : 𝕌 → 𝕌
  _+ᵤ_ : 𝕌 → 𝕌 → 𝕌
  _×ᵤ_ : 𝕌 → 𝕌 → 𝕌

infix  70 _×ᵤ_
infix  60 _+ᵤ_
infix  40 _⟷_
infixr 50 _◎_

⟦_⟧ : 𝕌 → Set
⟦ 𝟘 ⟧ = ⊥
⟦ 𝟙 ⟧ = ⊤
⟦ t₁ +ᵤ t₂ ⟧ = ⟦ t₁ ⟧ ⊎ ⟦ t₂ ⟧
⟦ t₁ ×ᵤ t₂ ⟧ = ⟦ t₁ ⟧ × ⟦ t₂ ⟧
⟦ 𝕄 t ⟧ = Bool × State Bool ⟦ t ⟧

data _⟷_ : 𝕌 → 𝕌 → Set where
  unite₊l : {t : 𝕌} → 𝟘 +ᵤ t ⟷ t
  uniti₊l : {t : 𝕌} → t ⟷ 𝟘 +ᵤ t
  unite₊r : {t : 𝕌} → t +ᵤ 𝟘 ⟷ t
  uniti₊r : {t : 𝕌} → t ⟷ t +ᵤ 𝟘
  swap₊   : {t₁ t₂ : 𝕌} → t₁ +ᵤ t₂ ⟷ t₂ +ᵤ t₁
  assocl₊ : {t₁ t₂ t₃ : 𝕌} → t₁ +ᵤ (t₂ +ᵤ t₃) ⟷ (t₁ +ᵤ t₂) +ᵤ t₃
  assocr₊ : {t₁ t₂ t₃ : 𝕌} → (t₁ +ᵤ t₂) +ᵤ t₃ ⟷ t₁ +ᵤ (t₂ +ᵤ t₃)
  unite⋆l : {t : 𝕌} → 𝟙 ×ᵤ t ⟷ t
  uniti⋆l : {t : 𝕌} → t ⟷ 𝟙 ×ᵤ t
  unite⋆r : {t : 𝕌} → t ×ᵤ 𝟙 ⟷ t
  uniti⋆r : {t : 𝕌} → t ⟷ t ×ᵤ 𝟙
  swap⋆   : {t₁ t₂ : 𝕌} → t₁ ×ᵤ t₂ ⟷ t₂ ×ᵤ t₁
  assocl⋆ : {t₁ t₂ t₃ : 𝕌} → t₁ ×ᵤ (t₂ ×ᵤ t₃) ⟷ (t₁ ×ᵤ t₂) ×ᵤ t₃
  assocr⋆ : {t₁ t₂ t₃ : 𝕌} → (t₁ ×ᵤ t₂) ×ᵤ t₃ ⟷ t₁ ×ᵤ (t₂ ×ᵤ t₃)
  absorbr : {t : 𝕌} → 𝟘 ×ᵤ t ⟷ 𝟘
  absorbl : {t : 𝕌} → t ×ᵤ 𝟘 ⟷ 𝟘
  factorzr : {t : 𝕌} → 𝟘 ⟷ t ×ᵤ 𝟘
  factorzl : {t : 𝕌} → 𝟘 ⟷ 𝟘 ×ᵤ t
  dist    : {t₁ t₂ t₃ : 𝕌} → (t₁ +ᵤ t₂) ×ᵤ t₃ ⟷ (t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃)
  factor  : {t₁ t₂ t₃ : 𝕌} → (t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃) ⟷ (t₁ +ᵤ t₂) ×ᵤ t₃
  distl   : {t₁ t₂ t₃ : 𝕌} → t₁ ×ᵤ (t₂ +ᵤ t₃) ⟷ (t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃)
  factorl : {t₁ t₂ t₃ : 𝕌 } → (t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃) ⟷ t₁ ×ᵤ (t₂ +ᵤ t₃)
  id⟷     : {t : 𝕌} → t ⟷ t
  _◎_     : {t₁ t₂ t₃ : 𝕌} → (t₁ ⟷ t₂) → (t₂ ⟷ t₃) → (t₁ ⟷ t₃)
  _⊕_     : {t₁ t₂ t₃ t₄ : 𝕌} → (t₁ ⟷ t₃) → (t₂ ⟷ t₄) → (t₁ +ᵤ t₂ ⟷ t₃ +ᵤ t₄)
  _⊗_     : {t₁ t₂ t₃ t₄ : 𝕌} → (t₁ ⟷ t₃) → (t₂ ⟷ t₄) → (t₁ ×ᵤ t₂ ⟷ t₃ ×ᵤ t₄)
  _⊝_     : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → (t₁ ⟷ t₂) → (𝕄 t₁ ⟷ 𝕄 t₂)
  idₓₒᵣ   : {t : 𝕌} → 𝕄 t ⟷ 𝕄 t
  ret     : {t : 𝕌} → t ⟷ 𝕄 t
  ext     : {t : 𝕌} → 𝕄 t ⟷ t

_>>=_ = RawIMonad._>>=_ (StateTMonad Bool IdentityMonad)
return = RawIMonad.return (StateTMonad Bool IdentityMonad)

interpF : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → ⟦ t₁ ⟧ → ⟦ t₂ ⟧
interpF unite₊l (inj₁ ())
interpF unite₊l (inj₂ v) = v
interpF uniti₊l v = inj₂ v
interpF unite₊r (inj₁ v) = v
interpF unite₊r (inj₂ ())
interpF uniti₊r v = inj₁ v
interpF swap₊ (inj₁ v) = inj₂ v
interpF swap₊ (inj₂ v) = inj₁ v
interpF assocl₊ (inj₁ v) = inj₁ (inj₁ v)
interpF assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
interpF assocl₊ (inj₂ (inj₂ v)) = inj₂ v
interpF assocr₊ (inj₁ (inj₁ v)) = inj₁ v
interpF assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
interpF assocr₊ (inj₂ v) = inj₂ (inj₂ v)
interpF unite⋆l (tt , v₂) = v₂
interpF uniti⋆l v = tt , v
interpF unite⋆r (v , tt) = v
interpF uniti⋆r v = v , tt
interpF swap⋆ (v₁ , v₂) = v₂ , v₁
interpF assocl⋆ (v₁ , (v₂ , v₃)) = (v₁ , v₂) , v₃
interpF assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
interpF absorbr (() , v)
interpF absorbl (v , ())
interpF factorzr ()
interpF factorzl ()
interpF dist (inj₁ v₁ , v₂) = inj₁ (v₁ , v₂)
interpF dist (inj₂ v₁ , v₂) = inj₂ (v₁ , v₂)
interpF factor (inj₁ (v₁ , v₂)) = inj₁ v₁ , v₂
interpF factor (inj₂ (v₁ , v₂)) = inj₂ v₁ , v₂
interpF distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
interpF distl (v₁ , inj₂ v₂) = inj₂ (v₁ , v₂)
interpF factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
interpF factorl (inj₂ (v₁ , v₂)) = v₁ , inj₂ v₂
interpF id⟷ v = v
interpF (c₁ ◎ c₂) v = interpF c₂ (interpF c₁ v)
interpF (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interpF c₁ v)
interpF (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interpF c₂ v)
interpF (c₁ ⊗ c₂) (v₁ , v₂) = interpF c₁ v₁ , interpF c₂ v₂
interpF (c₁ ⊝ c₂) (b , s) = b , (s >>= λ v → λ {true → (interpF c₁ v) , true ;
                                                false → (interpF c₂ v) , false})
interpF idₓₒᵣ (b , s) = not b , (s >>= λ v b → v , not b)
interpF ret v = true , return v
interpF ext (b , s) = proj₁ (s true)

_† : {t₁ t₂ : 𝕌} → t₁ ⟷ t₂ → t₂ ⟷ t₁
unite₊l † = uniti₊l
uniti₊l † = unite₊l
unite₊r † = uniti₊r
uniti₊r † = unite₊r
swap₊ † = swap₊
assocl₊ † = assocr₊
assocr₊ † = assocl₊
unite⋆l † = uniti⋆l
uniti⋆l † = unite⋆l
unite⋆r † = uniti⋆r
uniti⋆r † = unite⋆r
swap⋆ † = swap⋆
assocl⋆ † = assocr⋆
assocr⋆ † = assocl⋆
absorbr † = factorzl
absorbl † = factorzr
factorzr † = absorbl
factorzl † = absorbr
dist † = factor
factor † = dist
distl † = factorl
factorl † = distl
id⟷ † = id⟷
(C₁ ◎ C₂) † = (C₂ †) ◎ (C₁ †)
(C₁ ⊕ C₂) † = (C₁ †) ⊕ (C₂ †)
(C₁ ⊗ C₂) † = (C₁ †) ⊗ (C₂ †)
(C₁ ⊝ C₂) † = (C₁ †) ⊝ (C₂ †)
idₓₒᵣ † = idₓₒᵣ
ret † = ext
ext † = ret

interpB' : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → ⟦ t₁ ⟧ → ⟦ t₂ ⟧
interpB' unite₊l (inj₁ ())
interpB' unite₊l (inj₂ v) = v
interpB' uniti₊l v = inj₂ v
interpB' unite₊r (inj₁ v) = v
interpB' unite₊r (inj₂ ())
interpB' uniti₊r v = inj₁ v
interpB' swap₊ (inj₁ v) = inj₂ v
interpB' swap₊ (inj₂ v) = inj₁ v
interpB' assocl₊ (inj₁ v) = inj₁ (inj₁ v)
interpB' assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
interpB' assocl₊ (inj₂ (inj₂ v)) = inj₂ v
interpB' assocr₊ (inj₁ (inj₁ v)) = inj₁ v
interpB' assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
interpB' assocr₊ (inj₂ v) = inj₂ (inj₂ v)
interpB' unite⋆l (tt , v) = v
interpB' uniti⋆l v = tt , v
interpB' unite⋆r (v , tt) = v
interpB' uniti⋆r v = v , tt
interpB' swap⋆ (v₁ , v₂) = v₂ , v₁
interpB' assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
interpB' assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
interpB' absorbr (() , v)
interpB' absorbl (v , ())
interpB' factorzr ()
interpB' factorzl ()
interpB' dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
interpB' dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
interpB' factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
interpB' factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
interpB' distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
interpB' distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
interpB' factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
interpB' factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
interpB' id⟷ v = v
interpB' (c₁ ◎ c₂) v = interpB' c₂ (interpB' c₁ v)
interpB' (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interpB' c₁ v)
interpB' (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interpB' c₂ v)
interpB' (c₁ ⊗ c₂) (v₁ , v₂) = (interpB' c₁ v₁) , (interpB' c₂ v₂)
interpB' (c₁ ⊝ c₂) (b , s) = b , (s >>= λ v → λ {true → (interpB' c₁ v) , true ;
                                                 false → (interpB' c₂ v) , false})
interpB' idₓₒᵣ (b , s) = not b , (s >>= λ v b → v , not b)
interpB' ret v = true , return v
interpB' ext (b , s) = proj₁ (s b)

interpB : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → ⟦ t₂ ⟧ → ⟦ t₁ ⟧
interpB c v = interpB' (c †) v

𝟚 = 𝟙 +ᵤ 𝟙
c₁ : 𝟚 ×ᵤ 𝟚 ⟷ 𝟚 ×ᵤ 𝟚
c₁ = (ret ⊗ ret) ◎ (idₓₒᵣ ⊗ id⟷) ◎ ((swap₊ ⊝ id⟷)  ⊗ (swap₊ ⊝ id⟷)) ◎ (ext ⊗ ext)
v₁ : ⟦ 𝟚 ×ᵤ 𝟚 ⟧
v₁ = (inj₁ tt) , (inj₁ tt)

r₁ : interpF c₁ v₁ ≡ (inj₁ tt , inj₂ tt)
r₁ = refl
r₂ : interpB c₁ (inj₁ tt , inj₂ tt) ≡ v₁
r₂ = refl
