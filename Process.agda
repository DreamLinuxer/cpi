module Process where
open import Pi
open import Data.Empty
open import Data.Unit
open import Data.Sum
open import Data.Product
open import Function

⟦_⟧ : 𝕌 → Set
⟦ 𝟘 ⟧ = ⊥
⟦ 𝟙 ⟧ = ⊤
⟦ t₁ +ᵤ t₂ ⟧ = ⟦ t₁ ⟧ ⊎ ⟦ t₂ ⟧
⟦ t₁ ×ᵤ t₂ ⟧ = ⟦ t₁ ⟧ × ⟦ t₂ ⟧

mutual
  data Process : (t : 𝕌) →  Set where
    proc : {t₁ t₂ : 𝕌} → Process+ t₁ t₂ → Process t₂
    return : {t : 𝕌} → ⟦ t ⟧ → Process t

  Process+ : (t₁ t₂ : 𝕌) → Set
  Process+ t₁ t₂ = ⟦ t₁ ⟧ → Process t₂

data Trace : {t : 𝕌} → Process t → Set where
  [_] : {t : 𝕌} (x : ⟦ t ⟧) → Trace (return x)
  _::_ : {t₁ t₂ : 𝕌} {p : Process+ t₁ t₂} (x : ⟦ t₁ ⟧) → Trace (p x) → Trace (proc p)
infixr 50 _::_

_>>=_ : {t₁ t₂ : 𝕌} → Process t₁ → Process+ t₁ t₂ → Process t₂
_>>=_ {t₁} {t₂} (proc p₁) p₂ = proc (λ x → (p₁ x) >>= p₂)
_>>=_ {t₁} {t₂} (return x) p₂ = p₂ x

_>>='_ : {t₁ t₂ t₃ : 𝕌} → Process+ t₁ t₂ → Process+ t₂ t₃ → Process+ t₁ t₃
_>>='_ p₁ p₂ = λ x → p₁ x >>= p₂

delay : {t₁ t₂ : 𝕌} → Process+ t₁ t₂ → ⟦ t₁ ⟧ → Process+ 𝟙 t₂
delay p x = λ _ → p x

{-# TERMINATING #-}
_∥_ : {t₁ t₂ t₃ t₄ : 𝕌} → Process+ t₁ t₂ → Process+ t₃ t₄ → Process+ (𝟙 +ᵤ 𝟙) (t₂ ×ᵤ t₄)
_∥_ {t₁} {t₂} {t₃} {t₄} p₁ p₂ (inj₁ tt) = proc {t₁} p
  where
  p : Process+ t₁ (t₂ ×ᵤ t₄)
  p x with p₁ x
  p x | proc {t₁'} p' = proc (p' ∥ p₂)
  p x | return x' = proc (p₂ >>=' λ y → return (x' , y))
_∥_ {t₁} {t₂} {t₃} {t₄} p₁ p₂ (inj₂ tt) = proc {t₃} p
  where
  p : Process+ t₃ (t₂ ×ᵤ t₄)
  p y with p₂ y
  p y | proc p' = proc (p₁ ∥ p')
  p y | return y' = proc (p₁ >>=' λ x → return (x , y'))

_||_ : {t₁ t₂ t₃ t₄ : 𝕌} → Process+ t₁ t₂ → Process+ t₃ t₄ → Process+ (t₁ ×ᵤ t₃) (t₂ ×ᵤ t₄)
_||_ p₁ p₂ (x , y) = proc {𝟙 +ᵤ 𝟙} (delay p₁ x ∥ delay p₂ y)

⟦_⟧ₚ : {t₁ t₂ : 𝕌} → (c : t₁ ↔ t₂) → Process+ t₁ t₂
⟦_⟧ₚ {.(𝟘 +ᵤ t₂)} {t₂} unite₊l = λ {(inj₁ ()) ; (inj₂ x) → return x}
⟦_⟧ₚ {t₁} {.(𝟘 +ᵤ t₁)} uniti₊l = return ∘ inj₂
⟦_⟧ₚ {.(t₂ +ᵤ 𝟘)} {t₂} unite₊r = λ {(inj₁ x) → return x ; (inj₂ ())}
⟦_⟧ₚ {t₁} {.(t₁ +ᵤ 𝟘)} uniti₊r = return ∘ inj₁
⟦_⟧ₚ {.(_ +ᵤ _)} {.(_ +ᵤ _)} swap₊ (inj₁ x) = return (inj₂ x)
⟦_⟧ₚ {.(_ +ᵤ _)} {.(_ +ᵤ _)} swap₊ (inj₂ x) = return (inj₁ x)
⟦_⟧ₚ {.(_ +ᵤ (_ +ᵤ _))} {.((_ +ᵤ _) +ᵤ _)} assocl₊ (inj₁ x) = return (inj₁ (inj₁ x))
⟦_⟧ₚ {.(_ +ᵤ (_ +ᵤ _))} {.((_ +ᵤ _) +ᵤ _)} assocl₊ (inj₂ (inj₁ x)) = return (inj₁ (inj₂ x))
⟦_⟧ₚ {.(_ +ᵤ (_ +ᵤ _))} {.((_ +ᵤ _) +ᵤ _)} assocl₊ (inj₂ (inj₂ x)) = return (inj₂ x)
⟦_⟧ₚ {.((_ +ᵤ _) +ᵤ _)} {.(_ +ᵤ (_ +ᵤ _))} assocr₊ (inj₁ (inj₁ x)) = return (inj₁ x)
⟦_⟧ₚ {.((_ +ᵤ _) +ᵤ _)} {.(_ +ᵤ (_ +ᵤ _))} assocr₊ (inj₁ (inj₂ x)) = return (inj₂ (inj₁ x))
⟦_⟧ₚ {.((_ +ᵤ _) +ᵤ _)} {.(_ +ᵤ (_ +ᵤ _))} assocr₊ (inj₂ x) = return (inj₂ (inj₂ x))
⟦_⟧ₚ {.(𝟙 ×ᵤ t₂)} {t₂} unite⋆l = return ∘ proj₂
⟦_⟧ₚ {t₁} {.(𝟙 ×ᵤ t₁)} uniti⋆l x = return (tt , x)
⟦_⟧ₚ {.(t₂ ×ᵤ 𝟙)} {t₂} unite⋆r = return ∘ proj₁
⟦_⟧ₚ {t₁} {.(t₁ ×ᵤ 𝟙)} uniti⋆r x = return (x , tt)
⟦_⟧ₚ {.(_ ×ᵤ _)} {.(_ ×ᵤ _)} swap⋆ (x , y) = return (y , x)
⟦_⟧ₚ {.(_ ×ᵤ (_ ×ᵤ _))} {.((_ ×ᵤ _) ×ᵤ _)} assocl⋆ (x , (y , z)) = return ((x , y) , z)
⟦_⟧ₚ {.((_ ×ᵤ _) ×ᵤ _)} {.(_ ×ᵤ (_ ×ᵤ _))} assocr⋆ ((x , y) , z) = return (x , (y , z))
⟦_⟧ₚ {.(𝟘 ×ᵤ _)} {.𝟘} absorbr = return ∘ proj₁
⟦_⟧ₚ {.(_ ×ᵤ 𝟘)} {.𝟘} absorbl = return ∘ proj₂
⟦_⟧ₚ {.𝟘} {.(_ ×ᵤ 𝟘)} factorzr = λ ()
⟦_⟧ₚ {.𝟘} {.(𝟘 ×ᵤ _)} factorzl = λ ()
⟦_⟧ₚ {.((_ +ᵤ _) ×ᵤ _)} {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} dist (inj₁ x , y) = return (inj₁ (x , y))
⟦_⟧ₚ {.((_ +ᵤ _) ×ᵤ _)} {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} dist (inj₂ x , y) = return (inj₂ (x , y))
⟦_⟧ₚ {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} {.((_ +ᵤ _) ×ᵤ _)} factor (inj₁ (x , y)) = return ((inj₁ x) , y)
⟦_⟧ₚ {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} {.((_ +ᵤ _) ×ᵤ _)} factor (inj₂ (x , y)) = return ((inj₂ x) , y)
⟦_⟧ₚ {.(_ ×ᵤ (_ +ᵤ _))} {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} distl (x , inj₁ y) = return (inj₁ (x , y))
⟦_⟧ₚ {.(_ ×ᵤ (_ +ᵤ _))} {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} distl (x , inj₂ y) = return (inj₂ (x , y))
⟦_⟧ₚ {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} {.(_ ×ᵤ (_ +ᵤ _))} factorl (inj₁ (x , y)) = return (x , (inj₁ y))
⟦_⟧ₚ {.(_ ×ᵤ _ +ᵤ _ ×ᵤ _)} {.(_ ×ᵤ (_ +ᵤ _))} factorl (inj₂ (x , y)) = return (x , (inj₂ y))
⟦_⟧ₚ {t₁} {.t₁} id↔ = return
⟦_⟧ₚ {t₁} {t₂} (c₁ ◎ c₂) = ⟦ c₁ ⟧ₚ >>=' ⟦ c₂ ⟧ₚ
⟦_⟧ₚ {.(_ +ᵤ _)} {.(_ +ᵤ _)} (c₁ ⊕ c₂) = λ { (inj₁ x) → ⟦ c₁ ⟧ₚ x >>= (return ∘ inj₁)
                                           ; (inj₂ y) → ⟦ c₂ ⟧ₚ y >>= (return ∘ inj₂)}
⟦_⟧ₚ {.(_ ×ᵤ _)} {.(_ ×ᵤ _)} (c₁ ⊗ c₂) = ⟦ c₁ ⟧ₚ || ⟦ c₂ ⟧ₚ

module test where
  𝟚 = 𝟙 +ᵤ 𝟙
  C : (𝟚 ×ᵤ 𝟚) ↔ (𝟚 ×ᵤ 𝟚)
  C = swap₊ ⊗ swap₊

  tr₁ tr₂ : Trace (proc ⟦ C ⟧ₚ)
  tr₁ = (inj₁ tt , inj₁ tt) :: (inj₁ tt) :: tt :: tt :: [ (inj₂ tt , inj₂ tt) ]
  tr₂ = (inj₁ tt , inj₁ tt) :: (inj₂ tt) :: tt :: tt :: [ (inj₂ tt , inj₂ tt) ]
