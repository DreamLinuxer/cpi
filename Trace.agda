module Trace where
open import Data.Sum
open import Data.Product
open import Data.Unit
open import Data.Nat
open import Pi
open import Eval

infixr 50 _◎ₜᵣ_

data 𝕋𝕣 : (A : 𝕌) → (B : 𝕌) → Set where
  unite₊lₜᵣ : {t : 𝕌} → 𝕋𝕣 (𝟘 +ᵤ t) t
  uniti₊lₜᵣ : {t : 𝕌} → 𝕋𝕣 t (𝟘 +ᵤ t)
  unite₊rₜᵣ : {t : 𝕌} → 𝕋𝕣 (t +ᵤ 𝟘) t
  uniti₊rₜᵣ : {t : 𝕌} → 𝕋𝕣 t (t +ᵤ 𝟘)
  swap₊ₜᵣ   : {t₁ t₂ : 𝕌} → 𝕋𝕣 (t₁ +ᵤ t₂) (t₂ +ᵤ t₁)
  assocl₊ₜᵣ : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 (t₁ +ᵤ (t₂ +ᵤ t₃)) ((t₁ +ᵤ t₂) +ᵤ t₃)
  assocr₊ₜᵣ : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 ((t₁ +ᵤ t₂) +ᵤ t₃) (t₁ +ᵤ (t₂ +ᵤ t₃))
  unite⋆lₜᵣ : {t : 𝕌} → 𝕋𝕣 (𝟙 ×ᵤ t) t
  uniti⋆lₜᵣ : {t : 𝕌} → 𝕋𝕣 t (𝟙 ×ᵤ t)
  unite⋆rₜᵣ : {t : 𝕌} → 𝕋𝕣 (t ×ᵤ 𝟙) t
  uniti⋆rₜᵣ : {t : 𝕌} → 𝕋𝕣 t (t ×ᵤ 𝟙)
  swap⋆ₜᵣ   : {t₁ t₂ : 𝕌} → 𝕋𝕣 (t₁ ×ᵤ t₂) (t₂ ×ᵤ t₁)
  assocl⋆ₜᵣ : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 (t₁ ×ᵤ (t₂ ×ᵤ t₃)) ((t₁ ×ᵤ t₂) ×ᵤ t₃)
  assocr⋆ₜᵣ : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 ((t₁ ×ᵤ t₂) ×ᵤ t₃) (t₁ ×ᵤ (t₂ ×ᵤ t₃))
  absorbrₜᵣ : {t : 𝕌} → 𝕋𝕣 (𝟘 ×ᵤ t) 𝟘
  absorblₜᵣ : {t : 𝕌} → 𝕋𝕣 (t ×ᵤ 𝟘) 𝟘
  factorzrₜᵣ : {t : 𝕌} → 𝕋𝕣 𝟘 (t ×ᵤ 𝟘)
  factorzlₜᵣ : {t : 𝕌} → 𝕋𝕣 𝟘 (𝟘 ×ᵤ t)
  distₜᵣ    : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 ((t₁ +ᵤ t₂) ×ᵤ t₃) ((t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃))
  factorₜᵣ  : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 ((t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃)) ((t₁ +ᵤ t₂) ×ᵤ t₃)
  distlₜᵣ   : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 (t₁ ×ᵤ (t₂ +ᵤ t₃)) ((t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃))
  factorlₜᵣ : {t₁ t₂ t₃ : 𝕌 } → 𝕋𝕣 ((t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃)) (t₁ ×ᵤ (t₂ +ᵤ t₃))
  id↔ₜᵣ     : {t : 𝕌} → 𝕋𝕣 t t
  _◎ₜᵣ_     : {t₁ t₂ t₃ : 𝕌} → 𝕋𝕣 t₁ t₂ → 𝕋𝕣 t₂ t₃ → 𝕋𝕣 t₁ t₃
  ⊕lₜᵣ     : {t₁ t₂ t₃ t₄ : 𝕌} → 𝕋𝕣 t₁ t₃ → 𝕋𝕣 (t₁ +ᵤ t₂) (t₃ +ᵤ t₄)
  ⊕rₜᵣ     : {t₁ t₂ t₃ t₄ : 𝕌} → 𝕋𝕣 t₂ t₄ → 𝕋𝕣 (t₁ +ᵤ t₂) (t₃ +ᵤ t₄)
  ⊗lₜᵣ   : {t₁ t₂ t₃ t₄ : 𝕌} → 𝕋𝕣 t₁ t₃ → 𝕋𝕣 (t₁ ×ᵤ t₂) (t₃ ×ᵤ t₂)
  ⊗rₜᵣ   : {t₁ t₂ t₃ t₄ : 𝕌} → 𝕋𝕣 t₂ t₄ → 𝕋𝕣 (t₁ ×ᵤ t₂) (t₁ ×ᵤ t₄)

data 𝔼𝕍 : {t₁ t₂ : 𝕌} → t₁ ↔ t₂ → ⟦ t₁ ⟧ → ⟦ t₂ ⟧ → 𝕋𝕣 t₁ t₂ → Set where
  unite₊lₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 unite₊l (inj₂ a) a unite₊lₜᵣ
  uniti₊lₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 uniti₊l a (inj₂ a) uniti₊lₜᵣ
  unite₊rₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 unite₊r (inj₁ a) a unite₊rₜᵣ
  uniti₊rₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 uniti₊r a (inj₁ a) uniti₊rₜᵣ
  swap₊ₑ₁   : {t₁ t₂ : 𝕌} {a : ⟦ t₁ ⟧} → 𝔼𝕍 (swap₊ {t₁ = t₁} {t₂ = t₂}) (inj₁ a) (inj₂ a) swap₊ₜᵣ
  swap₊ₑ₂   : {t₁ t₂ : 𝕌} {a : ⟦ t₂ ⟧} → 𝔼𝕍 (swap₊ {t₁ = t₁} {t₂ = t₂}) (inj₂ a) (inj₁ a) swap₊ₜᵣ
  assocl₊ₑ₁ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} → 𝔼𝕍 (assocl₊ {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₁ a) (inj₁ (inj₁ a)) assocl₊ₜᵣ
  assocl₊ₑ₂ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₂ ⟧} → 𝔼𝕍 (assocl₊ {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₂ (inj₁ a)) (inj₁ (inj₂ a)) assocl₊ₜᵣ
  assocl₊ₑ₃ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₃ ⟧} → 𝔼𝕍 (assocl₊ {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₂ (inj₂ a)) (inj₂ a) assocl₊ₜᵣ
  assocr₊ₑ₁ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} → 𝔼𝕍 (assocr₊ {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₁ (inj₁ a)) (inj₁ a) assocr₊ₜᵣ
  assocr₊ₑ₂ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₂ ⟧} → 𝔼𝕍 (assocr₊ {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₁ (inj₂ a)) (inj₂ (inj₁ a)) assocr₊ₜᵣ
  assocr₊ₑ₃ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₃ ⟧} → 𝔼𝕍 (assocr₊ {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₂ a) (inj₂ (inj₂ a)) assocr₊ₜᵣ
  unite⋆lₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 unite⋆l (tt , a) a unite⋆lₜᵣ
  uniti⋆lₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 uniti⋆l a (tt , a) uniti⋆lₜᵣ
  unite⋆rₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 unite⋆r (a , tt) a unite⋆rₜᵣ
  uniti⋆rₑ  : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 uniti⋆r a (a , tt) uniti⋆rₜᵣ
  swap⋆ₑ    : {t₁ t₂ : 𝕌} {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} → 𝔼𝕍 swap⋆ (a , b) (b , a) swap⋆ₜᵣ
  assocl⋆ₑ  : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 assocl⋆ (a , b , c) ((a , b) , c) assocl⋆ₜᵣ
  assocr⋆ₑ  : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 assocr⋆ ((a , b) , c) (a , b , c) assocr⋆ₜᵣ
  absorbrₑ  : {t : 𝕌} {a : ⟦ 𝟘 ⟧} {b : ⟦ t ⟧} → 𝔼𝕍 absorbr (a , b) a absorbrₜᵣ
  absorblₑ  : {t : 𝕌} {a : ⟦ t ⟧} {b : ⟦ 𝟘 ⟧} → 𝔼𝕍 absorbl (a , b) b absorblₜᵣ
  factorzrₑ : {t : 𝕌} {a : ⟦ t ⟧} {b : ⟦ 𝟘 ⟧} → 𝔼𝕍 factorzr b (a , b) factorzrₜᵣ
  factorzlₑ : {t : 𝕌} {a : ⟦ 𝟘 ⟧} {b : ⟦ t ⟧} → 𝔼𝕍 factorzl a (a , b) factorzlₜᵣ
  distₑ₁    : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 (dist {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₁ a , c) (inj₁ (a , c)) distₜᵣ
  distₑ₂    : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 (dist {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₂ a , c) (inj₂ (a , c)) distₜᵣ
  factorₑ₁  : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 (factor {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₁ (a , c)) (inj₁ a , c) factorₜᵣ
  factorₑ₂  : {t₁ t₂ t₃ : 𝕌} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 (factor {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₂ (b , c)) (inj₂ b , c) factorₜᵣ
  distlₑ₁   : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} → 𝔼𝕍 (distl {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (a , inj₁ b) (inj₁ (a , b)) distlₜᵣ
  distlₑ₂   : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 (distl {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (a , inj₂ c) (inj₂ (a , c)) distlₜᵣ
  factorlₑ₁ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} → 𝔼𝕍 (factorl {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₁ (a , b)) (a , inj₁ b) factorlₜᵣ
  factorlₑ₂ : {t₁ t₂ t₃ : 𝕌} {a : ⟦ t₁ ⟧} {c : ⟦ t₃ ⟧} → 𝔼𝕍 (factorl {t₁ = t₁} {t₂ = t₂} {t₃ = t₃}) (inj₂ (a , c)) (a , inj₂ c) factorlₜᵣ
  id↔ₑ      : {t : 𝕌} {a : ⟦ t ⟧} → 𝔼𝕍 {t₁ = t} id↔ a a id↔ₜᵣ
  _◎ₑ_      : {t₁ t₂ t₃ : 𝕌} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {tr₁ : 𝕋𝕣 t₁ t₂} {tr₂ : 𝕋𝕣 t₂ t₃}
            → {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧}
            → 𝔼𝕍 c₁ a b tr₁ → 𝔼𝕍 c₂ b c tr₂ → 𝔼𝕍 (c₁ ◎ c₂) a c (tr₁ ◎ₜᵣ tr₂)
  _⊕lₑ_     : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₃} {c₂ : t₂ ↔ t₄} {tr₁ : 𝕋𝕣 t₁ t₃}
            → {a : ⟦ t₁ ⟧} {c : ⟦ t₃ ⟧}
            → 𝔼𝕍 c₁ a c tr₁ → 𝔼𝕍 (c₁ ⊕ c₂) (inj₁ a) (inj₁ c) (⊕lₜᵣ tr₁)
  _⊕rₑ_     : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₃} {c₂ : t₂ ↔ t₄} {tr₂ : 𝕋𝕣 t₂ t₄}
            → {b : ⟦ t₂ ⟧} {d : ⟦ t₄ ⟧}
            → 𝔼𝕍 c₂ b d tr₂ → 𝔼𝕍 (c₁ ⊕ c₂) (inj₂ b) (inj₂ d) (⊕rₜᵣ tr₂)
  _⊗lₑ_     : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₃} {c₂ : t₂ ↔ t₄} {tr₁ : 𝕋𝕣 t₁ t₃} {tr₂ : 𝕋𝕣 (t₃ ×ᵤ t₂) (t₃ ×ᵤ t₄)}
            → {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} {d : ⟦ t₄ ⟧}
            → 𝔼𝕍 c₁ a c tr₁ → 𝔼𝕍 (id↔ ⊗ c₂) (c , b) (c , d) tr₂ → 𝔼𝕍 (c₁ ⊗ c₂) (a , b) (c , d) ((⊗lₜᵣ {t₄ = t₄} tr₁) ◎ₜᵣ tr₂)
  _⊗rₑ_     : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ↔ t₃} {c₂ : t₂ ↔ t₄} {tr₁ : 𝕋𝕣 t₂ t₄} {tr₂ : 𝕋𝕣 (t₁ ×ᵤ t₄) (t₃ ×ᵤ t₄)}
            → {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} {d : ⟦ t₄ ⟧}
            → 𝔼𝕍 c₂ b d tr₁ → 𝔼𝕍 (c₁ ⊗ id↔) (a , d) (c , d) tr₂ → 𝔼𝕍 (c₁ ⊗ c₂) (a , b) (c , d) ((⊗rₜᵣ {t₃ = t₃} tr₁) ◎ₜᵣ tr₂)
  _⊗lₑ'_    : {t₁ t₂ t₃ t₄ t₁' : 𝕌} {c₁ : t₁ ↔ t₁'} {c₁' : t₁' ↔ t₃} {c₂ : t₂ ↔ t₄} {tr' : 𝕋𝕣 t₁ t₁'} {tr : 𝕋𝕣 (t₁' ×ᵤ t₂) (t₃ ×ᵤ t₄)}
            → {a : ⟦ t₁ ⟧} {a' : ⟦ t₁' ⟧} {b : ⟦ t₂ ⟧} {c : ⟦ t₃ ⟧} {d : ⟦ t₄ ⟧}
            → 𝔼𝕍 c₁ a a' tr' → 𝔼𝕍 (c₁' ⊗ c₂) (a' , b) (c , d) tr → 𝔼𝕍 ((c₁ ◎ c₁') ⊗ c₂) (a , b) (c , d) ((⊗lₜᵣ {t₄ = t₄} tr') ◎ₜᵣ tr)
  _⊗rₑ'_    : {t₁ t₂ t₃ t₄ t₂' : 𝕌} {c₁ : t₁ ↔ t₃} {c₂ : t₂ ↔ t₂'} {c₂' : t₂' ↔ t₄} {tr' : 𝕋𝕣 t₂ t₂'} {tr : 𝕋𝕣 (t₁ ×ᵤ t₂') (t₃ ×ᵤ t₄)}
            → {a : ⟦ t₁ ⟧} {b : ⟦ t₂ ⟧} {b' : ⟦ t₂' ⟧} {c : ⟦ t₃ ⟧} {d : ⟦ t₄ ⟧} 
            → 𝔼𝕍 c₂ b b' tr' → 𝔼𝕍 (c₁ ⊗ c₂') (a , b') (c , d) tr → 𝔼𝕍 (c₁ ⊗ (c₂ ◎ c₂')) (a , b) (c , d) ((⊗rₜᵣ {t₃ = t₃} tr') ◎ₜᵣ tr)

module Example where
  𝟚 : 𝕌
  𝟚 = 𝟙 +ᵤ 𝟙

  𝕥 𝕗 : ⟦ 𝟚 ⟧
  𝕥 = inj₁ tt
  𝕗 = inj₂ tt

  C C' : 𝟚 ×ᵤ (𝟚 ×ᵤ 𝟚) ↔ 𝟚 ×ᵤ (𝟚 ×ᵤ 𝟚)
  C  = swap₊ ⊗ (swap₊ ⊗ swap₊)
  C' =  (swap₊ ◎ swap₊) ⊗ ((swap₊ ◎ swap₊) ⊗ (swap₊ ◎ swap₊))

  -- ev₁ : 𝔼𝕍 C (𝕥 , 𝕥 , 𝕥) (𝕗 , 𝕗 , 𝕗) ((⊗lₜᵣ swap₊ₜᵣ) ◎ₜᵣ (⊗rₜᵣ {t₃ = 𝟚} (⊗lₜᵣ {t₄ = 𝟚} swap₊ₜᵣ)) ◎ₜᵣ (⊗rₜᵣ {t₃ = 𝟚} (⊗rₜᵣ {t₃ = 𝟚} swap₊ₜᵣ)) ◎ₜᵣ id↔ₜᵣ)
  -- ev₁ = swap₊ₑ₁ ⊗lₑ ({!!} ⊗rₑ {!!})

  -- ev₂ : 𝔼𝕍 C (𝕥 , 𝕥 , 𝕥) (𝕗 , 𝕗 , 𝕗) (⊗lₜᵣ swap₊ₜᵣ ◎ₜᵣ ⊗rₜᵣ (⊗rₜᵣ swap₊ₜᵣ ◎ₜᵣ ⊗lₜᵣ swap₊ₜᵣ))
  -- ev₂ = swap₊ₑ₁ ⊗lₑ (swap₊ₑ₁ ⊗rₑ swap₊ₑ₁)

  -- ev₃ : 𝔼𝕍 C' (𝕥 , 𝕥 , 𝕥) (𝕥 , 𝕥 , 𝕥) {!!}
  -- ev₃ = swap₊ₑ₁ ⊗lₑ' ({!!} ⊗rₑ {!!})
