module MPi
import Pi
import Data.Combinators.Applicative

%auto_implicits off
%default total

recNat :  (P : Nat -> Type) 
       -> P 0 
       -> ((n : Nat) -> P n -> P (S n)) 
       -> (n : Nat) -> P n
recNat P p0 ps Z = p0
recNat P p0 ps (S n) = ps n (recNat P p0 ps n)

infix 1 <~>
mutual
  data UM : Type where
    (<~>) : U -> U -> UM
    NAT : UM
    PI : (S : UM) -> (El S -> UM) -> UM

  El : UM -> Type
  El (t1 <~> t2) = t1 <-> t2
  El NAT = Nat
  El (PI S P) = (s : El S) -> El (P s)

infix 1 ~>
(~>) : UM -> UM -> UM
S ~> P = PI S (const P)

mutual
  data Context : Type where
    EMPTY : Context
    (::) : (C : Context) -> (Elc C -> UM) -> Context

  Elc : Context -> Type
  Elc EMPTY = ()
  Elc (c :: s) = DPair (Elc c) (\x => El $ s x)

infix 0 -:
data (-:) : (C : Context) -> (T : Elc C -> UM) -> Type where
  HERE  : {C,T : _} -> (C :: T) -: T . DPair.fst
  THERE : {C,S,T : _} -> C -: T -> (C :: S) -: T . DPair.fst

Elin : {C, T : _} -> C -: T -> (c : Elc C) -> El (T c)
Elin HERE (c ** t) = t
Elin (THERE x) (c ** t) = Elin x c

curry' :  {S,T : _} -> {P : DPair S T -> Type}
       -> ((p : DPair S T) -> P p)
       -> (s : S) -> (t : T s) -> P (s ** t)
curry' f s t = f (s ** t)

PI' : {C : _} -> (S : Elc C -> UM) -> (T : Elc (C :: S) -> UM) -> Elc C -> UM
PI' s t = \c => PI (s c) (\s => t (c ** s))

NAT' : {C : Context} -> (Elc C) -> UM
NAT' = const NAT

infix 1 <~~>
(<~~>) : {C : Context} -> U -> U -> Elc C -> UM
t1 <~~> t2 = const (t1 <~> t2)

infix 0 |-
mutual
  data (|-) : (C : Context) -> (Elc C -> UM) -> Type where
    VAR : {C : _} -> {t : Elc C -> UM} -> C -: t -> C |- t
    ZERO : {C : _} -> C |- NAT' {C}
    SUCC : {C : _} -> C |- NAT' {C} -> C |- NAT' {C}
    LAM : {C, S, T : _} -> (C :: S) |- T
                        ------------------
                        ->  C |- PI' S T
    APP : {C, S, T : _}
        -> C |- PI' S T -> (s : C |- S)
        -----------------------------------
        -> C |- (\c => T (c ** Elt s c))
    REC :  {C : _}
        -> (P : (Elc (C :: NAT' {C})) -> UM)
        -> C |- (\c => P (c ** 0))
        -> C |- (\c => PI NAT (\n => P (c ** n) ~> P (c ** S n)))
        -> (n : C |- NAT' {C})
        -------------------------------------------------------------
        -> C |- (\c => P (c ** Elt n c))
    BOX : {C,t1,t2 : _} ->   t1 <-> t2
                        -------------------
                        -> C |- t1 <~~> t2


  Elt : {C, t : _} -> C |- t -> (c : Elc C) -> El (t c)
  Elt (VAR x) = Elin x
  Elt ZERO = const 0
  Elt (SUCC n) = const S <*> Elt n
  Elt (LAM term) = (\c,s => Elt term (c ** s))
  Elt (APP t1 t2) = \c => Elt t1 c (Elt t2 c)
  Elt (REC P z s n) = \c => recNat (\p => El (P (c ** p))) 
                                   (Elt z c)
                                   (Elt s c) 
                                   (Elt n c)
  Elt (BOX c) = const c


idtest : EMPTY |- (PI' {C = EMPTY} (NAT' {C = EMPTY}) NAT')
idtest = LAM {S = NAT' {C = EMPTY}} (VAR HERE)

add : EMPTY |- PI' {C = EMPTY} (NAT' {C = EMPTY}) (PI' NAT' NAT')
add = LAM {S = NAT' {C = EMPTY}}
          (LAM {S = NAT'}
               (REC NAT' (VAR HERE)
                         (LAM {S = NAT'}
                              (LAM {S = NAT'}
                                   (SUCC (VAR HERE))))
                         (VAR (THERE HERE))))

test : (Elt {C = EMPTY} (APP {T = NAT'} (APP {T = PI' NAT' NAT'} add (SUCC ZERO)) (SUCC ZERO)) ()) = S (S Z)
test = Refl

Two : U
Two = One + One

Pu : Nat -> U
Pu Z = Two
Pu (S n) = Two * Pu n

nnot : (n : Nat) -> Pu n <-> Pu n
nnot Z = swaps
nnot (S n) = swaps |*| nnot n

P : {C : Context} -> Elc (C :: NAT' {C = C}) -> UM
P (c ** n) = Pu n <~> Pu n

nnot' : EMPTY |- PI' {C = EMPTY} (NAT' {C = EMPTY}) (P {C = EMPTY})
nnot' = LAM {S = NAT' {C = EMPTY}}
            (REC (P {C = EMPTY :: (NAT' {C = EMPTY})})
                 (BOX swaps)
                 (LAM {S = NAT'} 
                      (LAM {S = P {C = EMPTY :: (NAT' {C = EMPTY})}}
                           (BOX id)))
                 (VAR HERE))

-- Local Variables:
-- idris-load-packages: ("prelude" "base" "contrib")
-- End:
