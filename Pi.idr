module Pi
%access public export
%default total

data U : Type where
  Zero : U
  One : U
  (+) : U -> U -> U
  (*) : U -> U -> U

infix 1 <->
infixr 10 |@|
infixr 15 |+|
infixr 20 |*|

data (<->) : U -> U -> Type where
  unitesl : {t : U} -> Zero + t <-> t
  unitisl : {t : U} -> t <-> Zero + t
  unitesr : {t : U} -> t + Zero <-> t
  unitisr : {t : U} -> t <-> t + Zero
  swaps   : {t1, t2 : U} -> t1 + t2 <-> t2 + t1
  assocls : {t1, t2, t3 : U} -> t1 + (t2 + t3) <-> (t1 + t2) + t3
  assocrs : {t1, t2, t3 : U} -> (t1 + t2) + t3 <-> t1 + (t2 + t3)
  unitepl : {t : U} -> One * t <-> t
  unitipl : {t : U} -> t <-> One * t
  unitepr : {t : U} -> t * One <-> t
  unitipr : {t : U} -> t <-> t * One
  swapp   : {t1, t2 : U} -> t1 * t2 <-> t2 * t1
  assoclp : {t1, t2, t3 : U} -> t1 * (t2 * t3) <-> (t1 * t2) * t3
  assocrp : {t1, t2, t3 : U} -> (t1 * t2) * t3 <-> t1 * (t2 * t3)
  absorbr : {t : U} -> Zero * t <-> Zero
  absorbl : {t : U} -> t * Zero <-> Zero
  factorzr : {t : U} -> Zero <-> t * Zero
  factorzl : {t : U} -> Zero <-> Zero * t
  dist    : {t1, t2, t3 : U} -> (t1 + t2) * t3 <-> (t1 * t3) + (t2 * t3)
  factor  : {t1, t2, t3 : U} -> (t1 * t3) + (t2 * t3) <-> (t1 + t2) * t3
  distl   : {t1, t2, t3 : U} -> t1 * (t2 + t3) <-> (t1 * t2) + (t1 * t3)
  factorl : {t1, t2, t3 : U } -> (t1 * t2) + (t1 * t3) <-> t1 * (t2 + t3)
  id      : {t : U} -> t <-> t
  (|@|)     : {t1, t2, t3 : U} -> (t1 <-> t2) -> (t2 <-> t3) -> (t1 <-> t3)
  (|+|)     : {t1, t2, t3, t4 : U} -> (t1 <-> t3) -> (t2 <-> t4) -> (t1 + t2 <-> t3 + t4)
  (|*|)     : {t1, t2, t3, t4 : U} -> (t1 <-> t3) -> (t2 <-> t4) -> (t1 * t2 <-> t3 * t4)

total inv : {t1, t2 : U} -> t1 <-> t2 -> t2 <-> t1
inv (unitesl {t}) = unitisl
inv (unitisl {t}) = unitesl
inv (unitesr {t}) = unitisr
inv (unitisr {t}) = unitesr
inv (swaps {t1} {t2}) = swaps
inv (assocls {t1} {t2} {t3}) = assocrs
inv (assocrs {t1} {t2} {t3}) = assocls
inv (unitepl {t}) = unitipl
inv (unitipl {t}) = unitepl
inv (unitepr {t}) = unitipr
inv (unitipr {t}) = unitepr
inv (swapp {t1} {t2}) = swapp
inv (assoclp {t1} {t2} {t3}) = assocrp
inv (assocrp {t1} {t2} {t3}) = assoclp
inv (absorbr {t}) = factorzl
inv (absorbl {t}) = factorzr
inv (factorzr {t}) = absorbl
inv (factorzl {t}) = absorbr
inv (dist {t1} {t2} {t3}) = factor
inv (factor {t1} {t2} {t3}) = dist
inv (distl {t1} {t2} {t3}) = factorl
inv (factorl {t1} {t2} {t3}) = distl
inv (id {t}) = id
inv (x |@| y) =  inv y |@| inv x
inv (x |+| y) = inv x |+| inv y
inv (x |*| y) = inv x |*| inv y
