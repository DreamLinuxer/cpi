module Inj where
open import Data.Bool
open import Data.Empty
open import Data.Unit
open import Data.Sum
open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality

data 𝕌 : Set where
  𝟘 : 𝕌
  𝟙 : 𝕌
  𝕄 : 𝕌 → 𝕌
  _+ᵤ_ : 𝕌 → 𝕌 → 𝕌
  _×ᵤ_ : 𝕌 → 𝕌 → 𝕌

infix  70 _×ᵤ_
infix  60 _+ᵤ_
infix  40 _⟷_
infix  30 _⇔_
infixr 50 _◎_
infixr 50 _⊡_

mutual
  data _⟷_ : 𝕌 → 𝕌 → Set where
    unite₊l : {t : 𝕌} → 𝟘 +ᵤ t ⟷ t
    uniti₊l : {t : 𝕌} → t ⟷ 𝟘 +ᵤ t
    unite₊r : {t : 𝕌} → t +ᵤ 𝟘 ⟷ t
    uniti₊r : {t : 𝕌} → t ⟷ t +ᵤ 𝟘
    swap₊   : {t₁ t₂ : 𝕌} → t₁ +ᵤ t₂ ⟷ t₂ +ᵤ t₁
    assocl₊ : {t₁ t₂ t₃ : 𝕌} → t₁ +ᵤ (t₂ +ᵤ t₃) ⟷ (t₁ +ᵤ t₂) +ᵤ t₃
    assocr₊ : {t₁ t₂ t₃ : 𝕌} → (t₁ +ᵤ t₂) +ᵤ t₃ ⟷ t₁ +ᵤ (t₂ +ᵤ t₃)
    unite⋆l : {t : 𝕌} → 𝟙 ×ᵤ t ⟷ t
    uniti⋆l : {t : 𝕌} → t ⟷ 𝟙 ×ᵤ t
    unite⋆r : {t : 𝕌} → t ×ᵤ 𝟙 ⟷ t
    uniti⋆r : {t : 𝕌} → t ⟷ t ×ᵤ 𝟙
    swap⋆   : {t₁ t₂ : 𝕌} → t₁ ×ᵤ t₂ ⟷ t₂ ×ᵤ t₁
    assocl⋆ : {t₁ t₂ t₃ : 𝕌} → t₁ ×ᵤ (t₂ ×ᵤ t₃) ⟷ (t₁ ×ᵤ t₂) ×ᵤ t₃
    assocr⋆ : {t₁ t₂ t₃ : 𝕌} → (t₁ ×ᵤ t₂) ×ᵤ t₃ ⟷ t₁ ×ᵤ (t₂ ×ᵤ t₃)
    absorbr : {t : 𝕌} → 𝟘 ×ᵤ t ⟷ 𝟘
    absorbl : {t : 𝕌} → t ×ᵤ 𝟘 ⟷ 𝟘
    factorzr : {t : 𝕌} → 𝟘 ⟷ t ×ᵤ 𝟘
    factorzl : {t : 𝕌} → 𝟘 ⟷ 𝟘 ×ᵤ t
    dist    : {t₁ t₂ t₃ : 𝕌} → (t₁ +ᵤ t₂) ×ᵤ t₃ ⟷ (t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃)
    factor  : {t₁ t₂ t₃ : 𝕌} → (t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃) ⟷ (t₁ +ᵤ t₂) ×ᵤ t₃
    distl   : {t₁ t₂ t₃ : 𝕌} → t₁ ×ᵤ (t₂ +ᵤ t₃) ⟷ (t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃)
    factorl : {t₁ t₂ t₃ : 𝕌 } → (t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃) ⟷ t₁ ×ᵤ (t₂ +ᵤ t₃)
    id⟷     : {t : 𝕌} → t ⟷ t
    _◎_     : {t₁ t₂ t₃ : 𝕌} → (t₁ ⟷ t₂) → (t₂ ⟷ t₃) → (t₁ ⟷ t₃)
    _⊕_     : {t₁ t₂ t₃ t₄ : 𝕌} → (t₁ ⟷ t₃) → (t₂ ⟷ t₄) → (t₁ +ᵤ t₂ ⟷ t₃ +ᵤ t₄)
    _⊗_     : {t₁ t₂ t₃ t₄ : 𝕌} → (t₁ ⟷ t₃) → (t₂ ⟷ t₄) → (t₁ ×ᵤ t₂ ⟷ t₃ ×ᵤ t₄)
    _⊝_[_]     : {t₁ t₂ : 𝕌} → (c₁ : t₁ ⟷ t₂) → (c₂ : t₁ ⟷ t₂) → (c₁ ⇔ c₂) → (𝕄 t₁ ⟷ 𝕄 t₂)
    dup     : {t : 𝕌} → t ⟷ 𝕄 t
    eq      : {t : 𝕌} → 𝕄 t ⟷ t

  data _⇔_ : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → (t₁ ⟷ t₂) → Set where
    assoc◎l : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₂ ⟷ t₃} {c₃ : t₃ ⟷ t₄}
            → (c₁ ◎ (c₂ ◎ c₃)) ⇔ ((c₁ ◎ c₂) ◎ c₃)
    assoc◎r : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₂ ⟷ t₃} {c₃ : t₃ ⟷ t₄}
            → ((c₁ ◎ c₂) ◎ c₃) ⇔ (c₁ ◎ (c₂ ◎ c₃))
    assocl⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊) ⇔ (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃))
    assocl⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃)) ⇔ ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊)
    assocl⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆) ⇔ (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃))
    assocl⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃)) ⇔ ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆)
    assocr⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊) ⇔ (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃)))
    assocr⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃))) ⇔ (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆)
    assocr⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆) ⇔ (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃)))
    assocr⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₅ ⟷ t₆}
             → (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃))) ⇔ (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊)
    dist⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
           → ((a ⊕ b) ⊗ c) ◎ dist ⇔ dist ◎ ((a ⊗ c) ⊕ (b ⊗ c))
    dist⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
           → dist ◎ ((a ⊗ c) ⊕ (b ⊗ c)) ⇔ ((a ⊕ b) ⊗ c) ◎ dist
    distl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
            → (a ⊗ (b ⊕ c)) ◎ distl ⇔ distl ◎ ((a ⊗ b) ⊕ (a ⊗ c))
    distl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
            → distl ◎ ((a ⊗ b) ⊕ (a ⊗ c)) ⇔ (a ⊗ (b ⊕ c)) ◎ distl
    factor⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
             → ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor ⇔ factor ◎ ((a ⊕ b) ⊗ c)
    factor⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
             → factor ◎ ((a ⊕ b) ⊗ c) ⇔ ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor
    factorl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
              → ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl ⇔ factorl ◎ (a ⊗ (b ⊕ c))
    factorl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {a : t₁ ⟷ t₂} {b : t₃ ⟷ t₄} {c : t₅ ⟷ t₆}
              → factorl ◎ (a ⊗ (b ⊕ c)) ⇔ ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl
    idl◎l   : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → (id⟷ ◎ c) ⇔ c
    idl◎r   : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → c ⇔ id⟷ ◎ c
    idr◎l   : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → (c ◎ id⟷) ⇔ c
    idr◎r   : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → c ⇔ (c ◎ id⟷) 
    linv◎l  : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → (c ◎ ! c) ⇔ id⟷
    linv◎r  : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → id⟷ ⇔ (c ◎ ! c) 
    rinv◎l  : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → (! c ◎ c) ⇔ id⟷
    rinv◎r  : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → id⟷ ⇔ (! c ◎ c) 
    unite₊l⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → (unite₊l ◎ c₂) ⇔ ((c₁ ⊕ c₂) ◎ unite₊l)
    unite₊l⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → ((c₁ ⊕ c₂) ◎ unite₊l) ⇔ (unite₊l ◎ c₂)
    uniti₊l⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → (uniti₊l ◎ (c₁ ⊕ c₂)) ⇔ (c₂ ◎ uniti₊l)
    uniti₊l⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → (c₂ ◎ uniti₊l) ⇔ (uniti₊l ◎ (c₁ ⊕ c₂))
    unite₊r⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → (unite₊r ◎ c₂) ⇔ ((c₂ ⊕ c₁) ◎ unite₊r)
    unite₊r⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → ((c₂ ⊕ c₁) ◎ unite₊r) ⇔ (unite₊r ◎ c₂)
    uniti₊r⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → (uniti₊r ◎ (c₂ ⊕ c₁)) ⇔ (c₂ ◎ uniti₊r)
    uniti₊r⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟘 ⟷ 𝟘} {c₂ : t₁ ⟷ t₂}
              → (c₂ ◎ uniti₊r) ⇔ (uniti₊r ◎ (c₂ ⊕ c₁))
    swapl₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄}
            → (swap₊ ◎ (c₁ ⊕ c₂)) ⇔ ((c₂ ⊕ c₁) ◎ swap₊)
    swapr₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄}
            → ((c₂ ⊕ c₁) ◎ swap₊) ⇔ (swap₊ ◎ (c₁ ⊕ c₂))
    unitel⋆⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → (unite⋆l ◎ c₂) ⇔ ((c₁ ⊗ c₂) ◎ unite⋆l)
    uniter⋆⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → ((c₁ ⊗ c₂) ◎ unite⋆l) ⇔ (unite⋆l ◎ c₂)
    unitil⋆⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → (uniti⋆l ◎ (c₁ ⊗ c₂)) ⇔ (c₂ ◎ uniti⋆l)
    unitir⋆⇔l : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → (c₂ ◎ uniti⋆l) ⇔ (uniti⋆l ◎ (c₁ ⊗ c₂))
    unitel⋆⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → (unite⋆r ◎ c₂) ⇔ ((c₂ ⊗ c₁) ◎ unite⋆r)
    uniter⋆⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → ((c₂ ⊗ c₁) ◎ unite⋆r) ⇔ (unite⋆r ◎ c₂)
    unitil⋆⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → (uniti⋆r ◎ (c₂ ⊗ c₁)) ⇔ (c₂ ◎ uniti⋆r)
    unitir⋆⇔r : {t₁ t₂ : 𝕌} {c₁ : 𝟙 ⟷ 𝟙} {c₂ : t₁ ⟷ t₂}
              → (c₂ ◎ uniti⋆r) ⇔ (uniti⋆r ◎ (c₂ ⊗ c₁))
    swapl⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄}
            → (swap⋆ ◎ (c₁ ⊗ c₂)) ⇔ ((c₂ ⊗ c₁) ◎ swap⋆)
    swapr⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄}
            → ((c₂ ⊗ c₁) ◎ swap⋆) ⇔ (swap⋆ ◎ (c₁ ⊗ c₂))
    id⇔     : {t₁ t₂ : 𝕌} {c : t₁ ⟷ t₂} → c ⇔ c
    trans⇔  : {t₁ t₂ : 𝕌} {c₁ c₂ c₃ : t₁ ⟷ t₂}
            → (c₁ ⇔ c₂) → (c₂ ⇔ c₃) → (c₁ ⇔ c₃)
    _⊡_  : {t₁ t₂ t₃ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₂ ⟷ t₃} {c₃ : t₁ ⟷ t₂} {c₄ : t₂ ⟷ t₃}
         → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ◎ c₂) ⇔ (c₃ ◎ c₄)
    resp⊕⇔  : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₁ ⟷ t₂} {c₄ : t₃ ⟷ t₄}
            → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊕ c₂) ⇔ (c₃ ⊕ c₄)
    resp⊗⇔  : {t₁ t₂ t₃ t₄ : 𝕌} {c₁ : t₁ ⟷ t₂} {c₂ : t₃ ⟷ t₄} {c₃ : t₁ ⟷ t₂} {c₄ : t₃ ⟷ t₄}
            → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊗ c₂) ⇔ (c₃ ⊗ c₄)
    -- below are the combinators added for the RigCategory structure
    id⟷⊕id⟷⇔ : {t₁ t₂ : 𝕌} → (id⟷ {t₁} ⊕ id⟷ {t₂}) ⇔ id⟷
    split⊕-id⟷ : {t₁ t₂ : 𝕌} → (id⟷ {t₁ +ᵤ t₂}) ⇔ (id⟷ ⊕ id⟷)
    hom⊕◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ⟷ t₁} {c₂ : t₆ ⟷ t₂} {c₃ : t₁ ⟷ t₃} {c₄ : t₂ ⟷ t₄}
           → ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄)) ⇔ ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄))
    hom◎⊕⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ⟷ t₁} {c₂ : t₆ ⟷ t₂} {c₃ : t₁ ⟷ t₃} {c₄ : t₂ ⟷ t₄}
           → ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄)) ⇔ ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄))
    id⟷⊗id⟷⇔ : {t₁ t₂ : 𝕌} → (id⟷ {t₁} ⊗ id⟷ {t₂}) ⇔ id⟷
    split⊗-id⟷ : {t₁ t₂ : 𝕌} → (id⟷ {t₁ ×ᵤ t₂}) ⇔ (id⟷ ⊗ id⟷)
    hom⊗◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ⟷ t₁} {c₂ : t₆ ⟷ t₂} {c₃ : t₁ ⟷ t₃} {c₄ : t₂ ⟷ t₄}
           → ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄)) ⇔ ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄))
    hom◎⊗⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌} {c₁ : t₅ ⟷ t₁} {c₂ : t₆ ⟷ t₂} {c₃ : t₁ ⟷ t₃} {c₄ : t₂ ⟷ t₄}
           → ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄)) ⇔ ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄))
    -- associativity triangle
    triangle⊕l : {t₁ t₂ : 𝕌} → (unite₊r {t₁} ⊕ id⟷ {t₂}) ⇔ assocr₊ ◎ (id⟷ ⊕ unite₊l)
    triangle⊕r : {t₁ t₂ : 𝕌} → assocr₊ ◎ (id⟷ {t₁} ⊕ unite₊l {t₂}) ⇔ (unite₊r ⊕ id⟷)
    triangle⊗l : {t₁ t₂ : 𝕌} → ((unite⋆r {t₁}) ⊗ id⟷ {t₂}) ⇔ assocr⋆ ◎ (id⟷ ⊗ unite⋆l)
    triangle⊗r : {t₁ t₂ : 𝕌} → (assocr⋆ ◎ (id⟷ {t₁} ⊗ unite⋆l {t₂})) ⇔ (unite⋆r ⊗ id⟷)
    pentagon⊕l : {t₁ t₂ t₃ t₄ : 𝕌}
               → assocr₊ ◎ (assocr₊ {t₁} {t₂} {t₃ +ᵤ t₄})
               ⇔ ((assocr₊ ⊕ id⟷) ◎ assocr₊) ◎ (id⟷ ⊕ assocr₊)
    pentagon⊕r : {t₁ t₂ t₃ t₄ : 𝕌}
               → ((assocr₊ {t₁} {t₂} {t₃} ⊕ id⟷ {t₄}) ◎ assocr₊) ◎ (id⟷ ⊕ assocr₊)
               ⇔ assocr₊ ◎ assocr₊
    pentagon⊗l : {t₁ t₂ t₃ t₄ : 𝕌}
               → assocr⋆ ◎ (assocr⋆ {t₁} {t₂} {t₃ ×ᵤ t₄})
               ⇔ ((assocr⋆ ⊗ id⟷) ◎ assocr⋆) ◎ (id⟷ ⊗ assocr⋆)
    pentagon⊗r : {t₁ t₂ t₃ t₄ : 𝕌}
               → ((assocr⋆ {t₁} {t₂} {t₃} ⊗ id⟷ {t₄}) ◎ assocr⋆) ◎ (id⟷ ⊗ assocr⋆)
               ⇔ assocr⋆ ◎ assocr⋆
    -- from the braiding
    -- unit coherence
    unite₊l-coh-l : {t₁ : 𝕌} → unite₊l {t₁} ⇔ swap₊ ◎ unite₊r
    unite₊l-coh-r : {t₁ : 𝕌} → swap₊ ◎ unite₊r ⇔ unite₊l {t₁}
    unite⋆l-coh-l : {t₁ : 𝕌} → unite⋆l {t₁} ⇔ swap⋆ ◎ unite⋆r
    unite⋆l-coh-r : {t₁ : 𝕌} → swap⋆ ◎ unite⋆r ⇔ unite⋆l {t₁}
    hexagonr⊕l : {t₁ t₂ t₃ : 𝕌}
               → (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
               ⇔ ((swap₊ ⊕ id⟷) ◎ assocr₊) ◎ (id⟷ ⊕ swap₊)
    hexagonr⊕r : {t₁ t₂ t₃ : 𝕌}
               → ((swap₊ ⊕ id⟷) ◎ assocr₊) ◎ (id⟷ ⊕ swap₊)
               ⇔ (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
    hexagonl⊕l : {t₁ t₂ t₃ : 𝕌}
               → (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
               ⇔ ((id⟷ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id⟷)
    hexagonl⊕r : {t₁ t₂ t₃ : 𝕌}
               → ((id⟷ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id⟷)
               ⇔ (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
    hexagonr⊗l : {t₁ t₂ t₃ : 𝕌}
               → (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
               ⇔ ((swap⋆ ⊗ id⟷) ◎ assocr⋆) ◎ (id⟷ ⊗ swap⋆)
    hexagonr⊗r : {t₁ t₂ t₃ : 𝕌}
               → ((swap⋆ ⊗ id⟷) ◎ assocr⋆) ◎ (id⟷ ⊗ swap⋆)
               ⇔ (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
    hexagonl⊗l : {t₁ t₂ t₃ : 𝕌}
               → (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
               ⇔ ((id⟷ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id⟷)
    hexagonl⊗r : {t₁ t₂ t₃ : 𝕌}
               → ((id⟷ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id⟷)
               ⇔ (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
    absorbl⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
              → (c₁ ⊗ id⟷ {𝟘}) ◎ absorbl ⇔ absorbl ◎ id⟷ {𝟘}
    absorbl⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
              → absorbl ◎ id⟷ {𝟘} ⇔ (c₁ ⊗ id⟷ {𝟘}) ◎ absorbl
    absorbr⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
              → (id⟷ {𝟘} ⊗ c₁) ◎ absorbr ⇔ absorbr ◎ id⟷ {𝟘}
    absorbr⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
              → absorbr ◎ id⟷ {𝟘} ⇔ (id⟷ {𝟘} ⊗ c₁) ◎ absorbr
    factorzl⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
               → id⟷ ◎ factorzl ⇔ factorzl ◎ (id⟷ ⊗ c₁)
    factorzl⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
               → factorzl ◎ (id⟷ {𝟘} ⊗ c₁) ⇔ id⟷ {𝟘} ◎ factorzl
    factorzr⇔l : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
               → id⟷ ◎ factorzr ⇔ factorzr ◎ (c₁ ⊗ id⟷)
    factorzr⇔r : {t₁ t₂ : 𝕌} {c₁ : t₁ ⟷ t₂}
               → factorzr ◎ (c₁ ⊗ id⟷) ⇔ id⟷ ◎ factorzr
    -- from the coherence conditions of RigCategory
    swap₊distl⇔l : {t₁ t₂ t₃ : 𝕌}
                 → (id⟷ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl ⇔ distl ◎ swap₊
    swap₊distl⇔r : {t₁ t₂ t₃ : 𝕌}
                 → distl ◎ swap₊ ⇔ (id⟷ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl
    dist-swap⋆⇔l : {t₁ t₂ t₃ : 𝕌}
                 → dist {t₁} {t₂} {t₃} ◎ (swap⋆ ⊕ swap⋆) ⇔ swap⋆ ◎ distl
    dist-swap⋆⇔r : {t₁ t₂ t₃ : 𝕌}
                 → swap⋆ ◎ distl {t₁} {t₂} {t₃} ⇔ dist ◎ (swap⋆ ⊕ swap⋆)
    assocl₊-dist-dist⇔l : {t₁ t₂ t₃ t₄ : 𝕌}
                        → ((assocl₊ {t₁} {t₂} {t₃} ⊗ id⟷ {t₄}) ◎ dist) ◎ (dist ⊕ id⟷)
                        ⇔ (dist ◎ (id⟷ ⊕ dist)) ◎ assocl₊
    assocl₊-dist-dist⇔r : {t₁ t₂ t₃ t₄ : 𝕌}
                        → (dist {t₁} ◎ (id⟷ ⊕ dist {t₂} {t₃} {t₄})) ◎ assocl₊
                        ⇔ ((assocl₊ ⊗ id⟷) ◎ dist) ◎ (dist ⊕ id⟷)
    assocl⋆-distl⇔l : {t₁ t₂ t₃ t₄ : 𝕌}
                    → assocl⋆ {t₁} {t₂} ◎ distl {t₁ ×ᵤ t₂} {t₃} {t₄}
                    ⇔ ((id⟷ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
    assocl⋆-distl⇔r : {t₁ t₂ t₃ t₄ : 𝕌}
                    → ((id⟷ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
                    ⇔ assocl⋆ {t₁} {t₂} ◎ distl {t₁ ×ᵤ t₂} {t₃} {t₄}  
    absorbr0-absorbl0⇔ : absorbr {𝟘} ⇔ absorbl {𝟘}
    absorbl0-absorbr0⇔ : absorbl {𝟘} ⇔ absorbr {𝟘}
    absorbr⇔distl-absorb-unite : {t₁ t₂ : 𝕌}
                               → absorbr ⇔ (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l
    distl-absorb-unite⇔absorbr : {t₁ t₂ : 𝕌}
                               → (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l ⇔ absorbr
    unite⋆r0-absorbr1⇔ : unite⋆r ⇔ absorbr
    absorbr1-unite⋆r-⇔ : absorbr ⇔ unite⋆r
    absorbl≡swap⋆◎absorbr : {t₁ : 𝕌} → absorbl {t₁} ⇔ swap⋆ ◎ absorbr
    swap⋆◎absorbr≡absorbl : {t₁ : 𝕌} → swap⋆ ◎ absorbr ⇔ absorbl {t₁}
    absorbr⇔[assocl⋆◎[absorbr⊗id⟷]]◎absorbr : {t₁ t₂ : 𝕌}
                                            → absorbr ⇔ (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id⟷)) ◎ absorbr
    [assocl⋆◎[absorbr⊗id⟷]]◎absorbr⇔absorbr : {t₁ t₂ : 𝕌}
                                            → (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id⟷)) ◎ absorbr ⇔ absorbr
    [id⟷⊗absorbr]◎absorbl⇔assocl⋆◎[absorbl⊗id⟷]◎absorbr : {t₁ t₂ : 𝕌}
                                                        → (id⟷ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
                                                        ⇔ (assocl⋆ ◎ (absorbl ⊗ id⟷)) ◎ absorbr
    assocl⋆◎[absorbl⊗id⟷]◎absorbr⇔[id⟷⊗absorbr]◎absorbl : {t₁ t₂ : 𝕌}
                                                        → (assocl⋆ ◎ (absorbl ⊗ id⟷)) ◎ absorbr
                                                        ⇔ (id⟷ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
    elim⊥-A[0⊕B]⇔l : {t₁ t₂ : 𝕌}
                   → (id⟷ {t₁} ⊗ unite₊l {t₂})
                   ⇔ (distl ◎ (absorbl ⊕ id⟷)) ◎ unite₊l
    elim⊥-A[0⊕B]⇔r : {t₁ t₂ : 𝕌}
                   → (distl ◎ (absorbl ⊕ id⟷)) ◎ unite₊l
                   ⇔ (id⟷ {t₁} ⊗ unite₊l {t₂})
    elim⊥-1[A⊕B]⇔l : {t₁ t₂ : 𝕌}
                   → unite⋆l ⇔ distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂})
    elim⊥-1[A⊕B]⇔r : {t₁ t₂ : 𝕌} → distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂}) ⇔ unite⋆l
    fully-distribute⇔l : {t₁ t₂ t₃ t₄ : 𝕌}
                       → (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
                       ⇔ ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id⟷)) ◎ ((id⟷ ⊕ swap₊) ⊕ id⟷)) ◎ (assocl₊ ⊕ id⟷)
    fully-distribute⇔r : {t₁ t₂ t₃ t₄ : 𝕌}
                       → ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id⟷)) ◎ ((id⟷ ⊕ swap₊) ⊕ id⟷)) ◎ (assocl₊ ⊕ id⟷)
                       ⇔ (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
    -- make things easier
    cong! : {t₁ t₂ : 𝕌} {c₁ c₂ : t₁ ⟷ t₂} → (c₁ ⇔ c₂) → (! c₁ ⇔ ! c₂)

  ! : {t₁ t₂ : 𝕌} → t₁ ⟷ t₂ → t₂ ⟷ t₁
  ! unite₊l = uniti₊l
  ! uniti₊l = unite₊l
  ! unite₊r = uniti₊r
  ! uniti₊r = unite₊r
  ! swap₊ = swap₊
  ! assocl₊ = assocr₊
  ! assocr₊ = assocl₊
  ! unite⋆l = uniti⋆l
  ! uniti⋆l = unite⋆l
  ! unite⋆r = uniti⋆r
  ! uniti⋆r = unite⋆r
  ! swap⋆ = swap⋆
  ! assocl⋆ = assocr⋆
  ! assocr⋆ = assocl⋆
  ! absorbr = factorzl
  ! absorbl = factorzr
  ! factorzr = absorbl
  ! factorzl = absorbr
  ! dist = factor
  ! factor = dist
  ! distl = factorl
  ! factorl = distl
  ! id⟷ = id⟷
  ! (c₁ ◎ c₂) = ! c₂ ◎ ! c₁
  ! (c₁ ⊕ c₂) = ! c₁ ⊕ ! c₂
  ! (c₁ ⊗ c₂) = ! c₁ ⊗ ! c₂
  ! (c₁ ⊝ c₂ [ p ]) = ! c₁ ⊝ ! c₂ [ cong! p ]
  ! dup = eq
  ! eq = dup

⟦_⟧ : 𝕌 → Set
⟦ 𝟘 ⟧ = ⊥
⟦ 𝟙 ⟧ = ⊤
⟦ t₁ +ᵤ t₂ ⟧ = ⟦ t₁ ⟧ ⊎ ⟦ t₂ ⟧
⟦ t₁ ×ᵤ t₂ ⟧ = ⟦ t₁ ⟧ × ⟦ t₂ ⟧
⟦ 𝕄 t ⟧ = Σ[ c₁ ∈ (t ⟷ t) ] Σ[ c₂ ∈ (t ⟷ t) ] Σ[ c₁⇔c₂ ∈ c₁ ⇔ c₂ ] (⟦ t ⟧ × ⟦ t ⟧)

interpF : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → ⟦ t₁ ⟧ → ⟦ t₂ ⟧
interpF unite₊l (inj₁ ())
interpF unite₊l (inj₂ v) = v
interpF uniti₊l v = inj₂ v
interpF unite₊r (inj₁ v) = v
interpF unite₊r (inj₂ ())
interpF uniti₊r v = inj₁ v
interpF swap₊ (inj₁ v) = inj₂ v
interpF swap₊ (inj₂ v) = inj₁ v
interpF assocl₊ (inj₁ v) = inj₁ (inj₁ v)
interpF assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
interpF assocl₊ (inj₂ (inj₂ v)) = inj₂ v
interpF assocr₊ (inj₁ (inj₁ v)) = inj₁ v
interpF assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
interpF assocr₊ (inj₂ v) = inj₂ (inj₂ v)
interpF unite⋆l (tt , v₂) = v₂
interpF uniti⋆l v = tt , v
interpF unite⋆r (v , tt) = v
interpF uniti⋆r v = v , tt
interpF swap⋆ (v₁ , v₂) = v₂ , v₁
interpF assocl⋆ (v₁ , (v₂ , v₃)) = (v₁ , v₂) , v₃
interpF assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
interpF absorbr (() , v)
interpF absorbl (v , ())
interpF factorzr ()
interpF factorzl ()
interpF dist (inj₁ v₁ , v₂) = inj₁ (v₁ , v₂)
interpF dist (inj₂ v₁ , v₂) = inj₂ (v₁ , v₂)
interpF factor (inj₁ (v₁ , v₂)) = inj₁ v₁ , v₂
interpF factor (inj₂ (v₁ , v₂)) = inj₂ v₁ , v₂
interpF distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
interpF distl (v₁ , inj₂ v₂) = inj₂ (v₁ , v₂)
interpF factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
interpF factorl (inj₂ (v₁ , v₂)) = v₁ , inj₂ v₂
interpF id⟷ v = v
interpF (c₁ ◎ c₂) v = interpF c₂ (interpF c₁ v)
interpF (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interpF c₁ v)
interpF (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interpF c₂ v)
interpF (c₁ ⊗ c₂) (v₁ , v₂) = interpF c₁ v₁ , interpF c₂ v₂
interpF (c₁ ⊝ c₂ [ p ]) (c₁' , c₂' , p' , a₁ , a₂) =
        ! c₁ ◎ c₁' ◎ c₁ , ! c₂ ◎ c₂' ◎ c₂ , (cong! p ⊡ p' ⊡ p) , interpF c₁ a₁  , interpF c₂ a₂
interpF dup a = id⟷ , id⟷ , id⇔ , a , a
interpF eq (c₁ , c₂ , p , a , _) = a

interpB : {t₁ t₂ : 𝕌} → (t₁ ⟷ t₂) → ⟦ t₂ ⟧ → ⟦ t₁ ⟧
interpB c v = interpF (! c) v

𝟚 = 𝟙 +ᵤ 𝟙
c₁ : 𝟚 ⟷ 𝟚
c₁ = dup ◎ (swap₊ ⊝ swap₊ ◎ swap₊ ◎ swap₊ [ trans⇔ idr◎r (id⇔ ⊡ linv◎r) ]) ◎ eq

r₁ : interpF c₁ (inj₁ tt) ≡ inj₂ tt
r₁ = refl
r₂ : interpB c₁ (inj₂ tt) ≡ inj₁ tt
r₂ = refl
