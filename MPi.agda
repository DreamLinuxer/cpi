module MPi where

open import Function using (_∘_; _ˢ_; const)
open import Data.Unit
open import Data.Nat
open import Data.Product
open import Relation.Binary.PropositionalEquality
open import Pi

recℕ : (P : ℕ → Set) → P 0 → ((n : ℕ) → P n → P (suc n)) → (n : ℕ) → P n
recℕ P z s zero = z
recℕ P z s (suc n) = s n (recℕ P z s n)

mutual
  -- types
  data 𝕌ᴹ : Set where
    _↔ᴹ_ : 𝕌 → 𝕌 → 𝕌ᴹ
    nat : 𝕌ᴹ
    Π : (S : 𝕌ᴹ) → (⟦ S ⟧ᴹ → 𝕌ᴹ) → 𝕌ᴹ

  -- meaning
  ⟦_⟧ᴹ : 𝕌ᴹ → Set
  ⟦ t₁ ↔ᴹ t₂ ⟧ᴹ = t₁ ↔ t₂
  ⟦ nat ⟧ᴹ = ℕ
  ⟦ Π S T ⟧ᴹ = (s : ⟦ S ⟧ᴹ) → ⟦ T s ⟧ᴹ

_⇒_ : 𝕌ᴹ → 𝕌ᴹ → 𝕌ᴹ
S ⇒ T = Π S (const T)

mutual
  data Context : Set where
    ε : Context
    _,_ : (Γ : Context) → (⟦ Γ ⟧ᶜ → 𝕌ᴹ) → Context

  ⟦_⟧ᶜ : Context → Set
  ⟦ ε ⟧ᶜ = ⊤
  ⟦ Γ , S ⟧ᶜ = Σ[ γ ∈ ⟦ Γ ⟧ᶜ ] ⟦ S γ ⟧ᴹ

data _∋_ : (Γ : Context) (T : ⟦ Γ ⟧ᶜ → 𝕌ᴹ) → Set where
  here  : ∀ {Γ T} → (Γ , T) ∋ (T ∘ proj₁)
  there : ∀ {Γ S T} → Γ ∋ T → (Γ , S) ∋ (T ∘ proj₁)

⟦_⟧∋ : ∀ {Γ T} → Γ ∋ T → (γ : ⟦ Γ ⟧ᶜ) → ⟦ T γ ⟧ᴹ
⟦ here ⟧∋ (γ , t) = t
⟦ there x ⟧∋ (γ , t) = ⟦ x ⟧∋ γ

-- infix 0 _⊢ₜ_
infix 0 _⊢_
infixl 20 _$_

Π' : ∀ {Γ} (S : ⟦ Γ ⟧ᶜ → 𝕌ᴹ) (T : ⟦ Γ , S ⟧ᶜ → 𝕌ᴹ) →  ⟦ Γ ⟧ᶜ → 𝕌ᴹ
Π' S T = λ γ → Π (S γ) (λ s → T (γ , s))

nat' : ∀ {Γ} → ⟦ Γ ⟧ᶜ → 𝕌ᴹ
nat' = const nat

_↔ᴹ'_ : ∀ {Γ} → 𝕌 → 𝕌 → ⟦ Γ ⟧ᶜ → 𝕌ᴹ
t₁ ↔ᴹ' t₂ = const (t₁ ↔ᴹ t₂)

mutual
  -- syntax for type
  -- data _⊢ₜ_ (Γ : Context) : (⟦ Γ ⟧ᶜ → 𝕌ᴹ) → Set where
  --   □nat : Γ ⊢ₜ const nat
  --   □_↔_ :  (t₁ t₂ : 𝕌)
  --        ------------------
  --        → Γ ⊢ₜ const (t₁ ↔ᴹ t₂)
  --   □Π : ∀ {S T} → Γ ⊢ₜ S → Γ , S ⊢ₜ T
  --                ---------------------------------------
  --                → Γ ⊢ₜ λ γ → Π (S γ) (λ s → T (γ , s))

  -- syntax for term
  data _⊢_ : (Γ : Context) → (⟦ Γ ⟧ᶜ → 𝕌ᴹ) → Set where
    var : ∀ {Γ T} → Γ ∋ T → Γ ⊢ T
    zero : ∀ {Γ} → Γ ⊢ nat'
    succ : ∀ {Γ} → Γ ⊢ nat' → Γ ⊢ nat'
    lam : ∀ {Γ S T} → Γ , S ⊢ T
                    -------------
                    → Γ ⊢ Π' S T
                  
    _$_ : ∀ {Γ S T}
        → Γ ⊢ Π' S T → (s : Γ ⊢ S)
        ---------------------------
        → Γ ⊢ curry T ˢ ⟦ s ⟧
        
    rec : ∀ {Γ}
        → (P : ⟦ Γ , nat' ⟧ᶜ → 𝕌ᴹ)
        → Γ ⊢ (λ γ → P (γ , 0))
        → Γ ⊢ (λ γ → Π nat (λ n → P (γ , n) ⇒ P (γ , suc n)))
        → (n : Γ ⊢ nat')
        ----------------------------------------------------
        → Γ ⊢ curry P ˢ ⟦ n ⟧
        
    □ : ∀ {Γ t₁ t₂} →    t₁ ↔ t₂
                    ----------------
                    → Γ ⊢ t₁ ↔ᴹ' t₂
                  
    _⊠_ : ∀ {Γ t₁ t₂ t₃ t₄} → Γ ⊢ t₁ ↔ᴹ' t₂ → Γ ⊢ t₃ ↔ᴹ' t₄
                            --------------------------------
                            → Γ ⊢ (t₁ ×ᵤ t₃) ↔ᴹ' (t₂ ×ᵤ t₄)
                          
    _⊞_ : ∀ {Γ t₁ t₂ t₃ t₄} → Γ ⊢ t₁ ↔ᴹ' t₂ → Γ ⊢ t₃ ↔ᴹ' t₄
                            --------------------------------
                            → Γ ⊢ (t₁ +ᵤ t₃) ↔ᴹ' (t₂ +ᵤ t₄)
                          
    _▣_ : ∀ {Γ t₁ t₂ t₃} → Γ ⊢ t₁ ↔ᴹ' t₂ → Γ ⊢ t₂ ↔ᴹ' t₃
                         --------------------------------
                         →      Γ ⊢ (t₁ ↔ᴹ' t₃)

  -- interpreter
  ⟦_⟧ : ∀ {Γ T} → Γ ⊢ T → (γ : ⟦ Γ ⟧ᶜ) → ⟦ T γ ⟧ᴹ
  ⟦ var x ⟧ = ⟦ x ⟧∋
  ⟦ zero ⟧ = const 0
  ⟦ succ t ⟧ = const suc ˢ ⟦ t ⟧
  ⟦ lam t ⟧ = curry ⟦ t ⟧
  ⟦ t₁ $ t₂ ⟧ = ⟦ t₁ ⟧ ˢ ⟦ t₂ ⟧
  ⟦ rec P z s n ⟧ = λ γ → recℕ (λ p → ⟦ P (γ , p) ⟧ᴹ) (⟦ z ⟧ γ) (⟦ s ⟧ γ) (⟦ n ⟧ γ)
  ⟦ □ c ⟧ = const c
  ⟦ c₁ ⊠ c₂ ⟧ = λ γ → ⟦ c₁ ⟧ γ ⊗ ⟦ c₂ ⟧ γ
  ⟦ c₁ ⊞ c₂ ⟧ = λ γ → ⟦ c₁ ⟧ γ ⊕ ⟦ c₂ ⟧ γ
  ⟦ c₁ ▣ c₂ ⟧ = λ γ → ⟦ c₁ ⟧ γ ◎ ⟦ c₂ ⟧ γ

⟨_⟩ : (⟦ ε ⟧ᶜ → 𝕌ᴹ) → Set
⟨ T ⟩ = ε ⊢ T

add : ⟨ Π' nat' (Π' nat' nat') ⟩
add = lam (lam (rec nat' (var here)
                         (lam (lam (succ (var here))))
                         (var (there here))))

test : ⟦ add $ (succ (succ zero)) $ (succ zero) ⟧ tt ≡ 3
test = refl

𝟚 = 𝟙 +ᵤ 𝟙

Pu : ℕ → 𝕌
Pu 0 = 𝟚
Pu (suc n) = 𝟚 ×ᵤ Pu n

nnot : (n : ℕ) → Pu n ↔ Pu n
nnot zero = swap₊
nnot (suc n) = swap₊ ⊗ nnot n


P : ∀ {Γ} → ⟦ Γ , nat' ⟧ᶜ → 𝕌ᴹ
P (γ , n) = Pu n ↔ᴹ Pu n
{-
nnot' : ⟨ Π' nat' P ⟩
nnot' = lam (rec P (□ swap₊)
                   (lam (lam (□ id↔))) -- why?
                   (var here))
-}

