module Q where

open import Data.Bool
open import Data.Empty
open import Data.Unit
open import Data.Sum
open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality
open import Data.Vec
open import Relation.Nullary
open import Agda.Builtin.Nat
open import Data.Nat
open import Data.Nat.Properties
open import Pi

infix  70 _×𝕢_
infix  60 _+𝕢_
infix  40 _⇿_
infixr 50 _⊚_

data ℂ : 𝕌 → Set where
  ℂ₁ : ℂ 𝟙
  ℂₛ : {t : 𝕌} → ℂ t → ℂ (𝟙 +ᵤ t)

shift : {t : 𝕌} → ℂ t → t ↔ t
shift ℂ₁ = id↔
shift (ℂₛ ℂ₁) = swap₊
shift (ℂₛ (ℂₛ Ct)) = (id↔ ⊕ shift (ℂₛ Ct)) ⊚ assocl₊ ⊚ (swap₊ ⊕ id↔) ⊚ assocr₊

size : 𝕌 → ℕ
size 𝟘 = 0
size 𝟙 = 1
size (Ut₁ +ᵤ Ut₂) = size Ut₁ + size Ut₂
size (Ut₁ ×ᵤ Ut₂) = size Ut₁ * size Ut₂

size≡ : {t₁ t₂ : 𝕌} → t₁ ↔ t₂ → size t₁ ≡ size t₂
size≡ unite₊l = refl
size≡ uniti₊l = refl
size≡ unite₊r = +-identityʳ _
size≡ uniti₊r = sym (+-identityʳ _)
size≡ (swap₊ {t₁} {t₂}) = +-comm (size t₁) (size t₂)
size≡ (assocl₊ {t₁} {t₂} {t₃}) = sym (+-assoc (size t₁) (size t₂) (size t₃))
size≡ (assocr₊ {t₁} {t₂} {t₃}) = +-assoc (size t₁) (size t₂) (size t₃)
size≡ unite⋆l = +-comm _ 0
size≡ uniti⋆l = sym (+-comm _ 0)
size≡ unite⋆r = *-identityʳ _
size≡ uniti⋆r = sym (*-identityʳ _)
size≡ (swap⋆ {t₁} {t₂}) = *-comm (size t₁) (size t₂)
size≡ (assocl⋆ {t₁} {t₂} {t₃}) = sym (*-assoc (size t₁) (size t₂) (size t₃))
size≡ (assocr⋆ {t₁} {t₂} {t₃}) = *-assoc (size t₁) (size t₂) (size t₃)
size≡ absorbr = refl
size≡ (absorbl {t}) = *-zeroʳ (size t)
size≡ (factorzr {t}) = sym (*-zeroʳ (size t))
size≡ factorzl = refl
size≡ (dist {t₁} {t₂} {t₃}) = *-distribʳ-+ (size t₃) (size t₁) (size t₂)
size≡ (factor {t₁} {t₂} {t₃}) = sym (*-distribʳ-+ (size t₃) (size t₁) (size t₂))
size≡ (distl {t₁} {t₂} {t₃}) = *-distribˡ-+ (size t₁) (size t₂) (size t₃)
size≡ (factorl {t₁} {t₂} {t₃}) = sym (*-distribˡ-+ (size t₁) (size t₂) (size t₃))
size≡ id↔ = refl
size≡ (c₁ ⊚ c₂) rewrite size≡ c₁ | size≡ c₂ = refl
size≡ (c₁ ⊕ c₂) rewrite size≡ c₁ | size≡ c₂ = refl
size≡ (c₁ ⊗ c₂) rewrite size≡ c₁ | size≡ c₂ = refl

initℂ : {t : 𝕌} → ℂ t → ⟦ t ⟧
initℂ ℂ₁ = tt
initℂ (ℂₛ Ct) = inj₁ tt

Decℂ : {t : 𝕌} {Ct : ℂ t} → (v v' : ⟦ t ⟧) → (v ≡ v') ⊎  (¬ (v ≡ v'))
Decℂ {.𝟙} {ℂ₁} tt tt = inj₁ refl
Decℂ {.(𝟙 +ᵤ _)} {ℂₛ Ct} (inj₁ tt) (inj₁ tt) = inj₁ refl
Decℂ {.(𝟙 +ᵤ _)} {ℂₛ Ct} (inj₁ x) (inj₂ y) = inj₂ (λ ())
Decℂ {.(𝟙 +ᵤ _)} {ℂₛ Ct} (inj₂ y) (inj₁ x) = inj₂ (λ ())
Decℂ {.(𝟙 +ᵤ _)} {ℂₛ Ct} (inj₂ y₁) (inj₂ y₂) with Decℂ {Ct = Ct} y₁ y₂
Decℂ {.(𝟙 +ᵤ _)} {ℂₛ Ct} (inj₂ y₁) (inj₂ .y₁) | inj₁ refl = inj₁ refl
Decℂ {.(𝟙 +ᵤ _)} {ℂₛ Ct} (inj₂ y₁) (inj₂ y₂) | inj₂ p = inj₂ λ {refl → p refl}

data ℚ : Set where
  𝕟 : 𝕌 → ℚ
  _+𝕢_ : ℚ → ℚ → ℚ
  _×𝕢_ : ℚ → ℚ → ℚ
  𝟙∙ : {t : 𝕌} {Ct : ℂ t} → (v : ⟦ t ⟧) → ℚ
  𝟙/_ : (t : 𝕌) → ℚ

data _⇿_ : ℚ → ℚ → Set where
  unite₊l : {t : ℚ} → 𝕟 𝟘 +𝕢 t ⇿ t
  uniti₊l : {t : ℚ} → t ⇿ 𝕟 𝟘 +𝕢 t
  unite₊r : {t : ℚ} → t +𝕢 𝕟 𝟘 ⇿ t
  uniti₊r : {t : ℚ} → t ⇿ t +𝕢 𝕟 𝟘
  swap₊   : {t₁ t₂ : ℚ} → t₁ +𝕢 t₂ ⇿ t₂ +𝕢 t₁
  assocl₊ : {t₁ t₂ t₃ : ℚ} → t₁ +𝕢 (t₂ +𝕢 t₃) ⇿ (t₁ +𝕢 t₂) +𝕢 t₃
  assocr₊ : {t₁ t₂ t₃ : ℚ} → (t₁ +𝕢 t₂) +𝕢 t₃ ⇿ t₁ +𝕢 (t₂ +𝕢 t₃)
  unite⋆l : {t : ℚ} → 𝕟 𝟙 ×𝕢 t ⇿ t
  uniti⋆l : {t : ℚ} → t ⇿ 𝕟 𝟙 ×𝕢 t
  unite⋆r : {t : ℚ} → t ×𝕢 𝕟 𝟙 ⇿ t
  uniti⋆r : {t : ℚ} → t ⇿ t ×𝕢 𝕟 𝟙
  swap⋆   : {t₁ t₂ : ℚ} → t₁ ×𝕢 t₂ ⇿ t₂ ×𝕢 t₁
  assocl⋆ : {t₁ t₂ t₃ : ℚ} → t₁ ×𝕢 (t₂ ×𝕢 t₃) ⇿ (t₁ ×𝕢 t₂) ×𝕢 t₃
  assocr⋆ : {t₁ t₂ t₃ : ℚ} → (t₁ ×𝕢 t₂) ×𝕢 t₃ ⇿ t₁ ×𝕢 (t₂ ×𝕢 t₃)
  absorbr : {t : ℚ} → 𝕟 𝟘 ×𝕢 t ⇿ 𝕟 𝟘
  absorbl : {t : ℚ} → t ×𝕢 𝕟 𝟘 ⇿ 𝕟 𝟘
  factorzr : {t : ℚ} → 𝕟 𝟘 ⇿ t ×𝕢 𝕟 𝟘
  factorzl : {t : ℚ} → 𝕟 𝟘 ⇿ 𝕟 𝟘 ×𝕢 t
  dist    : {t₁ t₂ t₃ : ℚ} → (t₁ +𝕢 t₂) ×𝕢 t₃ ⇿ (t₁ ×𝕢 t₃) +𝕢 (t₂ ×𝕢 t₃)
  factor  : {t₁ t₂ t₃ : ℚ} → (t₁ ×𝕢 t₃) +𝕢 (t₂ ×𝕢 t₃) ⇿ (t₁ +𝕢 t₂) ×𝕢 t₃
  distl   : {t₁ t₂ t₃ : ℚ} → t₁ ×𝕢 (t₂ +𝕢 t₃) ⇿ (t₁ ×𝕢 t₂) +𝕢 (t₁ ×𝕢 t₃)
  factorl : {t₁ t₂ t₃ : ℚ } → (t₁ ×𝕢 t₂) +𝕢 (t₁ ×𝕢 t₃) ⇿ t₁ ×𝕢 (t₂ +𝕢 t₃)
  id⇿     : {t : ℚ} → t ⇿ t
  _⊚_     : {t₁ t₂ t₃ : ℚ} → (t₁ ⇿ t₂) → (t₂ ⇿ t₃) → (t₁ ⇿ t₃)
  _⊕_     : {t₁ t₂ t₃ t₄ : ℚ} → (t₁ ⇿ t₃) → (t₂ ⇿ t₄) → (t₁ +𝕢 t₂ ⇿ t₃ +𝕢 t₄)
  _⊗_     : {t₁ t₂ t₃ t₄ : ℚ} → (t₁ ⇿ t₃) → (t₂ ⇿ t₄) → (t₁ ×𝕢 t₂ ⇿ t₃ ×𝕢 t₄)
  _𝕢      : {t₁ t₂ : 𝕌} → (t₁ ↔ t₂) → 𝕟 t₁ ⇿ 𝕟 t₂
  unitl∙  : {t : 𝕌} {Ct : ℂ t} (v : ⟦ t ⟧) → 𝕟 𝟙 ⇿ 𝟙∙ {Ct = Ct} v
  unitr∙  : {t : 𝕌} {Ct : ℂ t} (v : ⟦ t ⟧) → 𝟙∙ {Ct = Ct} v ⇿ 𝕟 𝟙
  𝟙/[_]   : {t₁ t₂ : 𝕌} (c : t₁ ↔ t₂) → (𝟙/ t₁) ⇿ (𝟙/ t₂)
  η       : {t : 𝕌} {Ct : ℂ t} (v : ⟦ t ⟧) → 𝟙∙ {Ct = Ct} v ⇿ 𝕟 t ×𝕢 (𝟙/ t)
  ε       : {t : 𝕌} {Ct : ℂ t} (v : ⟦ t ⟧) → 𝕟 t ×𝕢 (𝟙/ t) ⇿ 𝟙∙ {Ct = Ct} v

!𝕢 : {t₁ t₂ : ℚ} → t₁ ⇿ t₂ → t₂ ⇿ t₁
!𝕢 unite₊l = uniti₊l
!𝕢 uniti₊l = unite₊l
!𝕢 unite₊r = uniti₊r
!𝕢 uniti₊r = unite₊r
!𝕢 swap₊ = swap₊
!𝕢 assocl₊ = assocr₊
!𝕢 assocr₊ = assocl₊
!𝕢 unite⋆l = uniti⋆l
!𝕢 uniti⋆l = unite⋆l
!𝕢 unite⋆r = uniti⋆r
!𝕢 uniti⋆r = unite⋆r
!𝕢 swap⋆ = swap⋆
!𝕢 assocl⋆ = assocr⋆
!𝕢 assocr⋆ = assocl⋆
!𝕢 absorbr = factorzl
!𝕢 absorbl = factorzr
!𝕢 factorzr = absorbl
!𝕢 factorzl = absorbr
!𝕢 dist = factor
!𝕢 factor = dist
!𝕢 distl = factorl
!𝕢 factorl = distl
!𝕢 id⇿ = id⇿
!𝕢 (c₁ ⊚ c₂) = !𝕢 c₂ ⊚ !𝕢 c₁
!𝕢 (c₁ ⊕ c₂) = !𝕢 c₁ ⊕ !𝕢 c₂
!𝕢 (c₁ ⊗ c₂) = !𝕢 c₁ ⊗ !𝕢 c₂
!𝕢 (c 𝕢) = (! c) 𝕢
!𝕢 (unitl∙ v) = unitr∙ v
!𝕢 (unitr∙ v) = unitl∙ v
!𝕢 𝟙/[ c ] = 𝟙/[ ! c ]
!𝕢 (η v) = ε v
!𝕢 (ε v) = η v

⟦_⟧ℚ : ℚ → Set
⟦ 𝕟 t ⟧ℚ = ⟦ t ⟧
⟦ t₁ +𝕢 t₂ ⟧ℚ = ⟦ t₁ ⟧ℚ ⊎ ⟦ t₂ ⟧ℚ
⟦ t₁ ×𝕢 t₂ ⟧ℚ = ⟦ t₁ ⟧ℚ × ⟦ t₂ ⟧ℚ
⟦ 𝟙∙ {t} v ⟧ℚ = Σ[ x ∈ ⟦ t ⟧ ] x ≡ v
⟦ 𝟙/ t ⟧ℚ = ⟦ t ⟧ × Vec (t ↔ t) (size t - 1)

index : {t₁ t₂ : 𝕌} → ℂ t₁ → (vi : ⟦ t₁ ⟧) → Vec (t₂ ↔ t₂) (size t₁ - 1) → (t₂ ↔ t₂)
index ℂ₁ tt vec = id↔
index (ℂₛ Ct) (inj₁ tt) vec = id↔
index {𝟙 +ᵤ .𝟙} {t₂} (ℂₛ ℂ₁) (inj₂ vi) (c ∷ []) = c
index {𝟙 +ᵤ .(𝟙 +ᵤ _)} {t₂} (ℂₛ (ℂₛ Ct)) (inj₂ vi) (c ∷ vec) = c ⊚ index (ℂₛ Ct) vi vec

interpℚ : {t₁ t₂ : ℚ} → (t₁ ⇿ t₂) → ⟦ t₁ ⟧ℚ → ⟦ t₂ ⟧ℚ
interpℚ unite₊l (inj₁ ())
interpℚ unite₊l (inj₂ v) = v
interpℚ uniti₊l v = inj₂ v
interpℚ unite₊r (inj₁ v) = v
interpℚ unite₊r (inj₂ ())
interpℚ uniti₊r v = inj₁ v
interpℚ swap₊ (inj₁ v) = inj₂ v
interpℚ swap₊ (inj₂ v) = inj₁ v
interpℚ assocl₊ (inj₁ v) = inj₁ (inj₁ v)
interpℚ assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
interpℚ assocl₊ (inj₂ (inj₂ v)) = inj₂ v
interpℚ assocr₊ (inj₁ (inj₁ v)) = inj₁ v
interpℚ assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
interpℚ assocr₊ (inj₂ v) = inj₂ (inj₂ v)
interpℚ unite⋆l v = proj₂ v
interpℚ uniti⋆l v = tt , v
interpℚ unite⋆r v = proj₁ v
interpℚ uniti⋆r v = v , tt
interpℚ swap⋆ (v₁ , v₂) = v₂ , v₁
interpℚ assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
interpℚ assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
interpℚ absorbr (() , v)
interpℚ absorbl (v , ())
interpℚ factorzr ()
interpℚ factorzl ()
interpℚ dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
interpℚ dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
interpℚ factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
interpℚ factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
interpℚ distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
interpℚ distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
interpℚ factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
interpℚ factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
interpℚ id⇿ v = v
interpℚ (c₁ ⊚ c₂) v = interpℚ c₂ (interpℚ c₁ v)
interpℚ (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interpℚ c₁ v)
interpℚ (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interpℚ c₂ v)
interpℚ (c₁ ⊗ c₂) (v₁ , v₂) = interpℚ c₁ v₁ , interpℚ c₂ v₂
interpℚ (c 𝕢) = interp c
interpℚ (unitl∙ v∙) tt = v∙ , refl
interpℚ (unitr∙ _) _ = tt
interpℚ 𝟙/[ c ] (v , vec) rewrite size≡ c = (interp c v) , (Data.Vec.map (λ c' → ! c ⊚ c' ⊚ c) vec)
interpℚ (η {t} {Ct} v∙) v = v∙ , ((initℂ Ct) , replicate (shift Ct))
interpℚ (ε {t} {Ct} v∙) (vi , v₀ , vec) with Decℂ {t} {Ct} (interp (index Ct vi vec) v₀) v∙
... | (inj₁ eq)  = (interp (index Ct vi vec) v₀) , eq
... | (inj₂ neq) = {!!}
