

Inductive 𝕌 : Type :=
| ZERO : 𝕌
| ONE : 𝕌
| TIME : 𝕌 -> 𝕌 -> 𝕌
| PLUS : 𝕌 -> 𝕌 -> 𝕌.

Notation "A *u B" := (TIME A B) (at level 50, left associativity).
Notation "A +u B" := (PLUS A B) (at level 50, left associativity).

Inductive 𝕌ᴹ : Type :=
| ℕ : 𝕌ᴹ
| Pi : (S : 𝕌ᴹ) -> (El S -> 𝕌ᴹ) -> 𝕌ᴹ
| combinator : 𝕌 -> 𝕌 -> 𝕌ᴹ.
with El (t : 𝕌ᴹ) : Type :=
  match t with
  | ℕ => nat
  | combinator _ _ => unit
  end.
    
