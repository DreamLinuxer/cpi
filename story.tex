\documentclass[12pt]{article}
\usepackage[margin=.5in]{geometry}
\usepackage{amsmath}
\usepackage{amsthm,amssymb,proof}
\usepackage{mathpartir,mathabx}
%\usepackage{scrextend}
\usepackage{enumitem}
\usepackage{graphicx}
%\usepackage{resizegather}
\usepackage{bbold,stmaryrd,bbm}
\usepackage{tikz-cd}
\usepackage{comment}
\usepackage{hyperref}
\usepackage[square,sort,comma,numbers]{natbib}
\newcommand{\D}{\mathcal{D}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\1}{\mathbbm{1}}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newcommand{\interp}[1]{\llbracket {#1} \rrbracket}
\pagenumbering{gobble}
%% \setlength\parindent{0pt}
\begin{document}
%% \section{}
Studying the algebraic theory is ubiquitous in programming language research, it provides a powerful tool to reason about the property
of computational models and proving correctness of programs \cite{Milner:1982:CCS:539036,Barendregt:1993:LCT:162552.162561,Milner:1999:CMS:329902,Baeten:2005:BHP:1085667.1085669}.
It is a well-studied area, however there are still two unexplored directions:
\begin{enumerate}
\item Computation model of nature algebraic theory:\\
  There is no computational model which satisfies algebraic theory of natural mathematical algebraic structure like group, ring and field.
  One possible reason of this is because usually researchers start with certain feature in mind to derived a computational model,
  and studying its algebraic theory after the operational semantics is developed, for example the calculus of communicating systems (CCS) \cite{Milner:1982:CCS:539036}
  started with a computational model incorporates interaction between transducers, and then the algebraic theory is derived through analyzing its operational semantics.
  This approach usually leads to a theory that fits our intuition about computation but not mathematically nature.
  For example the following are some common laws for process algebra from \cite{Baeten:2005:BHP:1085667.1085669}:
  \begin{align*}
  &x + y = y + x & x+(y+z) = (x+y) + z\\
  &x + x = x & (x+y);z = x;z + x;y\\
  &(x;y);z = x;(y;z) & x\|y = y\| x\\
  &(x\|y)\|z = x\|(y\|z) &
  \end{align*}
  where $+$ denotes alternative composition, $;$ denotes sequential composition and $\|$ denotes parallel composition.
  These rules is natural to most programmers because they fit the intuition of computation which is why they appear in most process algebras,
  if we compare this to the theory of ring:
  \begin{align*}
  & (a+b)+c = a+(b+c) & a+b = b+a\\
  & a + 0 = a & a + (-a) = 0\\
  &(a \cdot b) \cdot c = a \cdot (b \cdot c) & a \cdot 1 = a = 1 \cdot a\\
  &a \cdot (b + c) = (a \cdot b) + (a \cdot c) & (b + c) \cdot a = (b \cdot a) + (c \cdot a)
  \end{align*}
  we can see that both theories contain commutativity, associativity and distributivity laws, the piece that are missing are identities and inverses.
  Identities are not rare in normal operational semantics, for example in most operational semantics for imperative programming language we usually have
  term called $skip$ (or $nop$) which acts as identity of sequential composition, i.e.  $skip;x = x = x ; skip$.
  In contrast, we rarely see a operational semantics that has inverse.
  Does this means that inverses are not natural for computation?
  Based on the development of reversible computation, the answer to the problem is that we need to think about computation in a more general way,
  i.e.\ every computation should be reversible. The construction of Toffoli gate \cite{10.1007/3-540-10003-2_104} shows that reversible computation has
  equal power of conventional computation, hence reversible is not really a specialization but a generalization about computation.
  Following this lines of research, several reversible programming language is proposed, for example Janus~\cite{YOKOYAMA201071} is a imperative
  reversible programming language constructed in a way that every piece of program has a well-defined inversion, hence a Janus program is always reversible,
  the detailed operational semantics of Janus is given in~\cite{Yokoyama:2008:PRP:1366230.1366239} however the algebraic theory has not been studied.
  There are also works starting from algebraic theory to derive computational model, for example Algebra of Communicating Processes (ACP)~\cite{BERGSTRA1984109,bergstra1987convergence}
  started with an equational theory which has a range of models.
  And the reversible programming language $\Pi$~\cite{10.1007/978-3-662-49498-1_6} is constructed in a way such that every $\Pi$ program
  is a isomorphism between finite types based on rig (ring without negative elements) category, hence a $\Pi$ program corresponds to a proof in rig theory,
  and its algebraic theory of $\Pi$ is the equational theory of arrows in rig category which is one level higher than what we are looking for.\par
  As we can see from above, the general algebraic theory for computation is actually very close to a natural mathematical structure,
  it would be interesting to find the last missing piece by developing a practical computational model which has a natural algebraic theory.
\item Higher algebraic theory:\\
  Usually the study of algebraic theory for computational model stayed in the first level, and left the higher level algebraic theory unexplored.
  For a simple example of higher level (level-2 in this case) algebraic theory, let's consider the theory of monoid in the signature $\sigma = (0,+)$ is:
  \begin{align*}
    a &= a     \;\;\;\; \text{(id)}  & a = b & \implies b = a \;\;\;\;\text{(sym)}   & a = b \wedge b = c & \implies a = c \;\;\;\; \text{(tran)}\\
    a + 0 &= a \;\;\;\; \text{(lunit)} & 0 + a & = a     \;\;\;\;\text{(runit)} & (a+b)+c & = a+(b+c) \;\;\;\; \text{(assoc)}\\
    & & a = b \wedge c = d &\implies a + c = b + d \;\;\;\;\text{(cong)}
  \end{align*}
  This is the 1-level theory for monoid which is what we usually see in algebra textbook,
  note that usually we omit the rules which states that $=$ is a congruent equivalence relation,
  but it will be important when we go to the next level.
  A model satisfies the theory of monoid would be $\mathcal{M} = (M,0^M,+^M)$ where $M$ is a set, $0^M \in M$ and $+^M : M \times M \to M$ such that for all $a,b,c \in M$:
  \begin{align*}
    a +^M 0^M &=^M a  & 0 +^M a & =^M a  & (a+^Mb)+c & =^M a+^M(b+^Mc)
  \end{align*}
  where $=^M$ is an equivalence relation on $M$.
  Another way to view the three rules (lunit), (runit) and (assoc) is to consider them as functions defined with its only restricted domain on $M$ which preserve $=^M$,
  and the three rules state that $=$ is an equivalence relation actually corresponds to identity function, inverse and function composition.
  So now, we have a new signature $\sigma' = (lunit,runit,assoc,{}^{-1},\circ,id,\otimes)$ for the algebraic theory in 2-level where
  ${}^{-1}$ comes from (sym) $\circ$ comes from (tran), $id$ comes from (id) and $\otimes$ comes from (cong).
  For a model $\mathcal{M}$ to satisfy a 2-level theory a new equivalence relation $=_2^M$ for functions is needed.\par
  From categorical viewpoint, a 2-level theory is the constraint on the equivalence between arrows.
  For example, consider the categorified version of monoid \---- monoidal category,
  a monoidal category $\C$ is a category equipped with a functor $\otimes : \C \times \C \to \C$, an object $I \in \C$ and natural isomorphisms:
  \begin{align*}
    \alpha : ((-) \otimes (-)) \otimes (-) & \xrightarrow{\simeq} (-) \otimes ((-) \otimes (-)) & \text{with component $\alpha_{x,y,z} : (x \otimes y) \otimes z \to x \otimes (y \otimes z)$}\\
    \lambda : (I \otimes (-)) & \xrightarrow{\simeq} (-) & \text{with component $\lambda_x : I \otimes x \to x$}\\
    \rho : ((-) \otimes I) & \xrightarrow{\simeq} (-) & \text{with component $\rho_x : x \otimes I \to x$}
  \end{align*}
  such that the following two diagrams commute:
  \begin{center}
  \includegraphics[scale=0.3]{fig/680px-Monoidal_category_triangle.png}\\
  \includegraphics[scale=0.3]{fig/1400px-Monoidal_category_pentagon.png}
  \end{center}
  These two diagrams is the 2-level theory for monoidal category, we can also write it down using the new signature $\sigma'$:
  \begin{align*}
    (id \otimes lunit) \circ assoc & = runit \otimes id\\
    (id \otimes assoc) \circ assoc \circ (assoc \otimes id) & = assoc \circ assoc
  \end{align*}
  \par
  For computational model, there are two ways to think about 1-level theory and 2-level theory:
  \begin{itemize}
  \item The model is a representation of program:\\
    The 1-level theory in this case would be program transformations which preserve semantics,
    and the 2-level theory would be a theory which describe the relation between transformations.
    For example, a set of compiler optimizations which preserve semantic of program can be considered as a 1-level theory,
    and a possible 2-level theory would be formed by axiomatize the sequence of optimizations which preserve memory usage.
  \item The model is a state of system:\\
    The 1-level theory in this case would be operations on system states, and the 2-level theory would be a theory describe the relation between these operations.
    For example, a model for distributed system would have a set of primitive which can be considered as 1-level theory,
    and a possible 2-level theory would be the sequences of operations which preserve number of communication.
  \end{itemize}
  For an existing computational model, studying the 2-level theory can better our understanding about the model.
  And on the other way, a 2-level theory provides a finer way to distinguish different models.
\end{enumerate}
Combining these two direction, a concrete topic would be starting with a algebraic structure and consider its categorified version,
and then based on the 1 and 2-level theories, tried to develop or find an existing computational model which satisfy the theories.
\begin{comment}

However, the thing rarely goes another way, usually we do not a computational model derived from common mathematical structures
or satisfying common algebraic theory such as group or ring.
One obstacle to derived such model is the inverse operation, in normal computational model we usually do not have computations that can
cancel each other out.
But I think we should have systems which every computation has inverse, because almost all structure from algebra textbook \cite{herstein1975topics}
has inverse operation, and those algebraic theories are the most well-studied and intuitive ones.
The development of reversible computation \cite{10.1007/978-3-540-28644-8_19,DBLP:journals/corr/BrownS16,10.1007/978-3-319-99498-7_7,YOKOYAMA201071}
shed some light on this,
it would be interesting to see if we can use the development from reversible computation to derived practical system from
common algebraic structure.
The benefit of developing such system is we can use all the results from algebra to reason about computations and it should arguably be more
intuitive than existing system.
\par
Another missing piece is hinted by homotopy type theory \cite{hottbook}, an algebraic theory for a computational model gives a lot of paths
between computations/programs/terms.
Statically these paths correspond to program transformations and dynamically the paths correspond to equivalence preserving runtime operations.
Moreover, these paths provide a language for level 2 algebraic theory which consists of 2-paths.
Statically 2-path are just transformations which satisfies finer equivalence,
the dynamic case is more interesting because it allowed us to consider various aspect about operations.
For example we can consider the cost of each operations, and a level 2 algebraic theory specifies operations which has the same cost at runtime.
Hence, starting with an algebraic theory we can have a family of level 2 algebraic theories, and based on that we can find a family of systems
(some might not be realistic).
\par
The aim of this work is to combined the two things mentioned before:
starting with an algebraic theory of common mathematical structure, derived and implement a family of systems based on various level 2 algebraic theory.
\section{Example}
Given a signature $\sigma$ and a $\sigma$-theory $T$, the first step of this work is to study the computational $\sigma$-model of $T$.
A computational $\sigma$-model is just an $\sigma$-structure with a set of computations as domain and a machine (operational) semantics,
and a computational $\sigma$-model $M$ is a model of $T$ if $T \vdash c_1 = c_2 \iff M \vDash c_1^M \approx^M c_2^M$ where $\approx^M$ is some equivalence relation between computations.\par
For example, let the signature $\sigma = \{|\}$ and $T = \{\forall c_1,c_2,c_3. c_1 | (c_2 | c_3) = (c_1 | c_2) | c_3 \}$. A $\sigma$-model of $T$ would be a semigroup,
and when we restrict to computational $\sigma$-model the sentence in $T$ actually gives two program transformations which preserve $\approx$:
\begin{align*}
  assoc_\leftarrow &: c_1 \times (c_2 \times c_3) \rightarrow (c_1 \times c_2) \times c_3\\
  assoc_\rightarrow &: (c_1 \times c_2) \times c_3 \rightarrow c_1 \times (c_2 \times c_3)
\end{align*}
This means that in the computational $\sigma$-model, we can perform these two operations at anytime when we executing the computation without affect
the result of the computation.
For example, a simple computational $\sigma$-model $M_1 = (C,\Theta_1,\to_1;\|,\approx_1;{assoc_{\leftarrow_1},assoc_{\rightarrow_1}})$ for $T$ is defined as follows:
\begin{enumerate}
\item $C$ is the set of computation which is recursively defined
  \begin{align*}
    T &:= plus\; n\; |\; nop &\text{where $n \in \mathbb{N}$}\\
    C &:= T\; | C \| C
  \end{align*}
\item A machine state for $M_1$ is represented by a vector of $T$:\\
  $\Theta_1 : C \to Vector \; C$\\
  $\Theta_1 \; T = [T]$\\
  $\Theta_1 \; (C_1 \| C_2) = (\Theta_1\; C_1)\;++ \;(\Theta_1\;C_2)$
\item $\to_1$ describe how system executes:\\
  \infer[I \subseteq \{1,\ldots,n\},T_j' = T_j \text{ if $j \notin I$, otherwise } T_j' = nop]{n;[T_1,\ldots,T_n] \to_1 n+\Sigma_{i\in I} n_i;[T_1',\ldots,T_n']}{\forall i \in I .T_i = plus\; n_i}
\item The interpretation of the operator is just $\|$, i.e. $|^{M_1} = \|$.
\item The equivalence relation $\approx^{M_1} = \approx_1$ is defined as\\ $C_1 \approx_1 C_2 \iff (\forall n,n'\in\mathbb{N}. n;\Theta_1(C_1) \to_1^* n';[nop,\ldots,nop] \iff n;\Theta_1(C_2) \to_1^* n';[nop,\ldots,nop])$
\item $assoc_{\leftarrow_1}$ and $assoc_{\rightarrow_1}$ are just identity function since $\Theta_1(c_1 \| (c_2 \| c_3)) = \Theta_1((c_1 \| c_2) \| c_3)$.
\end{enumerate}
Conceptually, $M_1$ models a system has $\aleph_0$ many processors with a heap which only contains an natural number.
Another example $M_2=(C,\Theta_2,\to_2;\|,\approx_2;{assoc_{\leftarrow_2},assoc_{\rightarrow_2}})$ for $T$ where $assoc_{\leftarrow_2}$ and $assoc_{\rightarrow_2}$ are nontrivial is given as follows:
\begin{enumerate}
\item A machine state for $M_1$ is represented by a pair of $C$:\\
  $\Theta_2 : C \to (C,C)$\\
  $\Theta_2 \; T = (T,nop)$\\
  $\Theta_2 \; (C_1 \| C_2) = (C_1,C_2)$
\item $\to_2$ describe how system executes:\\
  \infer[]{n;(C_1,C_2) \to_2 n+n_1+n_2;(C_1',C_2')}{n;;C_1 \rightsquigarrow n+n_1 ;; C_1' & n;;C_2 \rightsquigarrow n+n_2 ;; C_2'}\\\\
  \infer[]{n;(C_1,C_2) \to_2 n+n_1;(C_1',C_2)}{n;;C_1 \rightsquigarrow n+n_1 ;; C_1'}\;\;\;\;\;\;
  \infer[]{n;(C_1,C_2) \to_2 n+n_2;(C_1,C_2')}{n;;C_2 \rightsquigarrow n+n_2 ;; C_2'} where $\rightsquigarrow$ is defined as\\\\
  \infer{n;;plus\;k \rightsquigarrow n+k ;; nop}{}\;\;\;\;\;\;
  \infer{n;;C_1 \| C_2 \rightsquigarrow n+k ;; C_1' \| C_2}{n;;C_1 \rightsquigarrow n+k ;; C_1'}\;\;\;\;\;\;\;
  \infer{n;;C_1 \| nop \rightsquigarrow n ;; C_1}{}\\\\
  \infer{n;;C_1 \| C_2 \rightsquigarrow n+k ;; C_1 \| C_2'}{n;;C_2 \rightsquigarrow n+k ;; C_2'}\;\;\;\;\;\;\;
  \infer{n;;nop \| C_2 \rightsquigarrow n ;; C_2}{}
  
\item The equivalence relation $\approx^{M_2} = \approx_2$ is defined as\\ $C_1 \approx_2 C_2 \iff (\forall n,n'\in\mathbb{N}. n;\Theta_2(C_1) \to_2^* n';(nop,nop) \iff n;\Theta_2(C_2) \to_2^* n';(nop,nop))$
\item $assoc_{\leftarrow_2}\;(c_1, (c_2 \| c_3)) = ((c_1 \|c_2) | c_3)$\\
  $assoc_{\rightarrow_2}\;((c_1 \|c_2) | c_3) = (c_1, (c_2 \| c_3))$
\end{enumerate}
Conceptually, $M_2$ is just like $M_1$ but only contains two processors.\par
Now we have more things in our language, for every sentence $\varphi$ in $T$ gives us two function symbol $\varphi_\rightarrow$ and $\varphi_\leftarrow$.
So we can have a 2-level theory $T'$ with signature $\sigma_T$ = $\{\circ,id_{\leftrightarrow}\} \cup \{\varphi_\rightarrow,\varphi_\leftarrow | \varphi \in T\}$
(note that $\circ$ and $id_{\leftrightarrow}$ actually come from the condition of equivalence relation).
A model of this 2-level theory $T'$ must be a model $T$ and in addition another equivalence relation $\equiv$.
Let $M_1'=(M_1,\equiv_1)$ and $M_2' = (M_2,\equiv_2)$  where
\begin{align*}
f \equiv_1 g &\iff (\forall C,L_f,L_g,n,n',k.\; n;f(\Theta_1(C)) \to_1^k n';L_f \iff n;g(\Theta_1(C)) \to_1^k n';L_g)\\
f \equiv_2 g &\iff (\forall C,C_{f_1},C_{f_2},C_{g_1},C_{g_2},n,n',k.\; n;f(\Theta_2(C)) \to_2^k n';(C_{f_1},C_{f_2}) \iff n;g(\Theta_2(C)) \to_2^k n';(C_{g_1},C_{g_2}))
\end{align*}
For $M_1'$, it satisfies all equations because all operations are identities.
But it is not the case for $M_2'$, for example $assoc_{\leftarrow_2} \not \equiv id_{\leftrightarrow}$:
\begin{align*}
  n ; (plus\;1, (plus\;10 \| plus\;100)) &\to_2^1 n+11 ; (nop, (nop \| plus\;100))\\
  n;((plus\;1 \| plus\;10), plus\;100) &\nrightarrow_2^1 n+11 ; (C_1,C_2) \;\;\;\; \forall C_1,C_2
\end{align*}
\end{comment}
\begin{comment}
The second step is to consider the 2-level theories, as what we sees above every sentence $\varphi$ in $T$ induces two operations/transformations $\varphi_\rightarrow$ and $\varphi_\leftarrow$,
a 2-level theory of $T'$ is a set of $\sigma_T$-sentence where $\sigma_T$ = $\{\circ,id_{\leftrightarrow}\} \cup \{\varphi_\rightarrow,\varphi_\leftarrow | \varphi \in T\}$.
A computational $\sigma$-model of $(T,T')$ would be a computational $\sigma$-model of $T$ where the machine semantics which has a cost function $C$ for each $\varphi$ operations,
and if $T' \vdash \varphi = \psi \iff C(\varphi^M) = C(\psi^M)$. For example, let $T' = {assoc_\rightarrow \circ assoc_\leftarrow = id_\leftrightarrow}$, the sequential model in last paragraph is also a model of $(T,T')$
since the cost of $assoc_\leftarrow$ and $assoc_\rightarrow$ is 0 for sequential single processor machine.\par
The goal of this work is to start from some common algebraic theory $T$ and then consider all possible 2-level theories $T'$.
Then for each $(T,T')$, try to figure out computational model for $(T,T')$ with real world implementation (which might not be possible for some $(T,T')$).
\end{comment}

\bibliographystyle{plain}
\bibliography{refs}

\end{document}
