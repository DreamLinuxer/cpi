module Sigma where
  data 𝔼 : 𝕌 → Set where
    𝔼₀ : {t₁ t₂ : 𝕌} (c : t₁ ↔ t₂) → 𝔼 t₁
    𝔼₊ : {t₁ t₂ : 𝕌} (c : t₁ ↔ t₂) → 𝔼 t₂ → 𝔼 t₁

  data _⇌ˢ[_]_ : {t₁ t₂ : 𝕌} → 𝔼 t₁ → t₁ ↔ t₂ → 𝔼 t₂ → Set where
    _⊝₀_ : {t₁ t₂ t₃ t₄ : 𝕌} {c : t₁ ↔ t₂} (c₁ : t₁ ↔ t₃) (c₂ : t₂ ↔ t₄)
         → 𝔼₀ c ⇌ˢ[ c₁ ] 𝔼₀ (! c₁ ◎ c ◎ c₂)
    _⊝₊_ : {t₁ t₂ t₃ t₄ : 𝕌} {c : t₁ ↔ t₂} {c' : t₂ ↔ t₄} {e₁ : 𝔼 t₂} {e₂ : 𝔼 t₄}
         → (c₁ : t₁ ↔ t₃) (c₂ : e₁ ⇌ˢ[ c' ] e₂)
         → 𝔼₊ c e₁ ⇌ˢ[ c₁ ] 𝔼₊ (! c₁ ◎ c ◎ c') e₂

  data 𝕌ˢ : Set where
    ℙ : 𝕌 → 𝕌ˢ
    _×ᵤ_ : 𝕌ˢ → 𝕌ˢ → 𝕌ˢ
    _+ᵤ_ : 𝕌ˢ → 𝕌ˢ → 𝕌ˢ
    ∑ : {t : 𝕌} → 𝔼 t → 𝕌ˢ

  ⟦_⟧ˢ : 𝕌ˢ → Set
  ⟦ ℙ t ⟧ˢ = ⟦ t ⟧
  ⟦ t₁ ×ᵤ t₂ ⟧ˢ = ⟦ t₁ ⟧ˢ × ⟦ t₂ ⟧ˢ
  ⟦ t₁ +ᵤ t₂ ⟧ˢ = ⟦ t₁ ⟧ˢ ⊎ ⟦ t₂ ⟧ˢ
  ⟦ ∑ {t} (𝔼₀ c) ⟧ˢ = ⟦ t ⟧
  ⟦ ∑ {t} (𝔼₊ c e) ⟧ˢ = ⟦ t ⟧

  data _⇌_ : 𝕌ˢ → 𝕌ˢ → Set where
    _↾ : {t₁ t₂ : 𝕌} (c : t₁ ↔ t₂) → ℙ t₁ ⇌ ℙ t₂
    _◎_ : {t₁ t₂ t₃ : 𝕌ˢ} (c₁ : t₁ ⇌ t₂) (c₂ : t₂ ⇌ t₃) → t₁ ⇌ t₃
    _⊕_ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} → (t₁ ⇌ t₃) → (t₂ ⇌ t₄) → (t₁ +ᵤ t₂ ⇌ t₃ +ᵤ t₄)
    _⊗_ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} → (t₁ ⇌ t₃) → (t₂ ⇌ t₄) → (t₁ ×ᵤ t₂ ⇌ t₃ ×ᵤ t₄)
    sigma : {t₁ t₂ : 𝕌} → (c : t₁ ↔ t₂) → ℙ t₁ ⇌ ∑ (𝔼₀ c)
    extend : {t₁ t₂ : 𝕌} {e : 𝔼 t₂} → (c : t₁ ↔ t₂) → ∑ e ⇌ ∑ (𝔼₊ c e)
    _ˢ : {t₁ t₂ : 𝕌} {e₁ : 𝔼 t₁} {e₂ : 𝔼 t₂} {c : t₁ ↔ t₂} (c' : e₁ ⇌ˢ[ c ] e₂) → ∑ e₁ ⇌ ∑ e₂
    extract₀ : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → ∑ (𝔼₀ c) ⇌ ℙ t₁
    extract₊ : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} {e : 𝔼 t₂} → ∑ (𝔼₊ c e) ⇌ ∑ e
    coercion₀ : {t₁ t₂ : 𝕌} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → ∑ (𝔼₀ c₁) ⇌ ∑ (𝔼₀ c₂)
    coercion₊ : {t₁ t₂ : 𝕌} {c₁ c₂ : t₁ ↔ t₂} {e : 𝔼 t₂} → c₁ ⇔ c₂ → ∑ (𝔼₊ c₁ e) ⇌ ∑ (𝔼₊ c₂ e)
    dist    : {t₁ t₂ : 𝕌} {t₃ : 𝕌ˢ} → ℙ (t₁ +ᵤ t₂) ×ᵤ t₃ ⇌ (ℙ t₁ ×ᵤ t₃) +ᵤ (ℙ t₂ ×ᵤ t₃)
    factor  : {t₁ t₂ : 𝕌} {t₃ : 𝕌ˢ} → (ℙ t₁ ×ᵤ t₃) +ᵤ (ℙ t₂ ×ᵤ t₃) ⇌ ℙ (t₁ +ᵤ t₂) ×ᵤ t₃
    distl   : {t₁ : 𝕌ˢ} {t₂ t₃ : 𝕌} → t₁ ×ᵤ ℙ (t₂ +ᵤ t₃) ⇌ (t₁ ×ᵤ ℙ t₂) +ᵤ (t₁ ×ᵤ ℙ t₃)
    factorl : {t₁ : 𝕌ˢ} {t₂ t₃ : 𝕌 } → (t₁ ×ᵤ ℙ t₂) +ᵤ (t₁ ×ᵤ ℙ t₃) ⇌ t₁ ×ᵤ ℙ (t₂ +ᵤ t₃)
    assocl⋆ : {t₁ t₂ t₃ : 𝕌ˢ} → t₁ ×ᵤ (t₂ ×ᵤ t₃) ⇌ (t₁ ×ᵤ t₂) ×ᵤ t₃
    assocr⋆ : {t₁ t₂ t₃ : 𝕌ˢ} → (t₁ ×ᵤ t₂) ×ᵤ t₃ ⇌ t₁ ×ᵤ (t₂ ×ᵤ t₃)
    id↔ : {t : 𝕌ˢ} → t ⇌ t

  interp𝕌ˢ : {t₁ t₂ : 𝕌ˢ} → (t₁ ⇌ t₂) → ⟦ t₁ ⟧ˢ → ⟦ t₂ ⟧ˢ
  interp𝕌ˢ (c ↾) v = interp c v
  interp𝕌ˢ (c₁ ◎ c₂) v = interp𝕌ˢ c₂ (interp𝕌ˢ c₁ v)
  interp𝕌ˢ (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interp𝕌ˢ c₁ v)
  interp𝕌ˢ (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interp𝕌ˢ c₂ v)
  interp𝕌ˢ (c₁ ⊗ c₂) (v₁ , v₂) = interp𝕌ˢ c₁ v₁ , interp𝕌ˢ c₂ v₂
  interp𝕌ˢ dist (inj₁ v₁ , v₂) = inj₁ (v₁ , v₂)
  interp𝕌ˢ dist (inj₂ v₁ , v₂) = inj₂ (v₁ , v₂)
  interp𝕌ˢ factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
  interp𝕌ˢ factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
  interp𝕌ˢ distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
  interp𝕌ˢ distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
  interp𝕌ˢ factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
  interp𝕌ˢ factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
  interp𝕌ˢ assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
  interp𝕌ˢ assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
  interp𝕌ˢ id↔ v = v
  interp𝕌ˢ (sigma c) v = v
  interp𝕌ˢ (extend {e = 𝔼₀ c₁} c) v = interp (! c) v
  interp𝕌ˢ (extend {e = 𝔼₊ c₁ e} c) v = interp (! c) v
  interp𝕌ˢ ((c₁ ⊝₀ c₂) ˢ) v = interp c₁ v
  interp𝕌ˢ ((c₁ ⊝₊ c₂) ˢ) v = interp c₁ v
  interp𝕌ˢ extract₀ v = v
  interp𝕌ˢ (extract₊ {c = c} {𝔼₀ c₁}) v = interp c v
  interp𝕌ˢ (extract₊ {c = c} {𝔼₊ c₁ e}) v = interp c v
  interp𝕌ˢ (coercion₀ x) v = v
  interp𝕌ˢ (coercion₊ x) v = v

  𝟚 : 𝕌
  𝟚 = 𝟙 +ᵤ 𝟙

  -- Examples:
  swap𝔼 : {t₁ t₂ : 𝕌} {c : t₁ ↔ t₂} → ∑ (𝔼₀ c) ⇌ ∑ (𝔼₀ (! c))
  swap𝔼 {t₁} {t₂} {c} = ((c ⊝₀ (! c)) ˢ) ◎ coercion₀ (trans⇔ (id⇔ ⊡ linv◎l) idr◎l)
