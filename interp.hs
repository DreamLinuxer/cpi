module Interp where
import Data.Maybe
import Data.List

data T = One | Zero | T :+: T | T :*: T | M T deriving (Eq, Show)
data C = UniteSl  | UnitiSl  | UniteSr  | UnitiSr  | SwapS   | AssocSl | AssocSr
       | UnitePl  | UnitiPl  | UnitePr  | UnitiPr  | SwapP   | AssocPl | AssocPr   
       | Absorbr  | Absorbl  | Factorzr | Factorzl  | Dist  | Factor  | Distl  | Factorl   
       | Id  | C :@ C | C :+ C | C :* C | C :- C | Idx | Ret  | Ext 
       deriving (Eq, Show)
data U = T :=>: T deriving (Eq, Show)
data V = Top | P (V , V) | Inl V | Inr V | MV (V , Bool) deriving (Eq, Show)

interpF :: C -> V -> V
interpF UniteSl (Inr v) = v
interpF UnitiSl v = Inr v
interpF UniteSr (Inl v) = v
interpF UnitiSr v = Inl v
interpF SwapS (Inl v) = Inr v
interpF SwapS (Inr v) = Inl v
interpF AssocSl (Inl v) = Inl (Inl v)
interpF AssocSl (Inr (Inl v)) = Inl (Inr v)
interpF AssocSl (Inr (Inr v)) = Inr v
interpF AssocSr (Inl (Inl v)) = (Inl v)
interpF AssocSr (Inl (Inr v)) = Inr (Inl v)
interpF AssocSr (Inr v) = (Inr (Inr v))
interpF UnitePl (P (Top , v)) = v
interpF UnitiPl v = (P (Top , v))
interpF UnitePr (P (v , Top)) = v
interpF UnitiPr v = (P (v , Top))
interpF SwapP (P (v1 , v2)) = (P (v2 , v1))
interpF AssocPl (P (v1 , P (v2 , v3))) = (P ((P (v1 , v2)) , v3))
interpF AssocPr (P ((P (v1 , v2)) , v3)) = (P (v1 , P (v2 , v3)))
interpF Dist (P (Inl v1 , v2)) = Inl (P (v1 , v2))
interpF Dist (P (Inr v1 , v2)) = Inr ( P (v1 , v2))
interpF Factor (Inl (P (v1 , v2))) = (P (Inl v1 , v2))
interpF Factor (Inr ( P (v1 , v2))) = (P (Inr v1 , v2))
interpF Distl (P (v1 , Inl v2)) = Inl (P (v1 , v2))
interpF Distl (P (v1 , Inr v2)) = Inr (P (v1 , v2))
interpF Factorl (Inl (P (v1 , v2))) = (P (v1 , Inl v2))
interpF Factorl (Inr (P (v1 , v2))) = (P (v1 , Inr v2))
interpF Id v = v
interpF (c1 :@ c2) v = (interpF c2 (interpF c1 v))
interpF (c1 :+ c2) (Inl v) = Inl (interpF c1 v)
interpF (c1 :+ c2) (Inr v) = Inr (interpF c2 v)
interpF (c1 :* c2) (P (v1 , v2)) = P (interpF c1 v1 , interpF c2 v2)
interpF (c1 :- c2) (MV (v , True)) = MV (interpF c1 v , True)
interpF (c1 :- c2) (MV (v , False)) = MV (interpF c2 v , False)
interpF Idx (MV (v , b)) = MV (v , not b)
interpF Ret v = MV (v , True)
interpF Ext (MV (v , b)) = v
interpF c v = undefined

rev :: C -> C
rev UniteSl = UnitiSl
rev UnitiSl = UniteSl
rev UniteSr = UnitiSr
rev UnitiSr = UniteSr
rev SwapS   = SwapS
rev AssocSl = AssocSr
rev AssocSr = AssocSl
rev UnitePl = UnitiPl
rev UnitiPl = UnitePl
rev UnitePr = UnitiPr
rev UnitiPr = UnitePr
rev SwapP   = SwapP
rev AssocPl = AssocPr
rev AssocPr = AssocPl
rev Absorbr = Factorzl
rev Absorbl = Factorzr
rev Factorzr = Absorbl
rev Factorzl = Absorbr
rev Dist = Factor
rev Factor = Dist
rev Distl = Factorl
rev Factorl = Distl
rev Id = Id
rev (c1 :@ c2) = (rev c2) :@ (rev c1)
rev (c1 :+ c2) = (rev c1) :+ (rev c2)
rev (c1 :* c2) = (rev c1) :* (rev c2)
rev (c1 :- c2) = (rev c1) :- (rev c2)
rev Idx = Idx
rev Ret = Ext
rev Ext = Ret

--interpB :: C -> V -> V
interpB c v = fst $ fromJust $ fromJust $ find isJust $
              map (\xs -> interpM c xs v) $ guess $ count c

guess :: Int -> [[Bool]]
guess 0 = [[]]
guess n = (map (True :) $ guess (n - 1)) ++
          (map (False :) $ guess (n - 1))

count :: C -> Int
count (c1 :@ c2) = (count c1) + (count c2)
count (c1 :+ c2) = (count c1) + (count c2)
count (c1 :* c2) = (count c1) + (count c2)
count (c1 :- c2) = (count c1) + (count c2)
count Ret = 1
count _ = 0

interpM :: C -> [Bool] -> V -> Maybe (V , [Bool])
interpM (c1 :@ c2) bs v = do
  (v1 , bs') <- interpM c1 bs v
  (v2 , bs'') <- interpM c2 bs' v1
  return (v2 , bs'')
interpM (c1 :+ c2) bs (Inl v) = do
  (v' , bs') <- interpM c1 bs v
  return (v' , bs')
interpM (c1 :+ c2) bs (Inr v) = do
  (v' , bs') <- interpM c2 bs v
  return (v' , bs')
interpM (c1 :* c2) bs (P (v1 , v2)) = do
  (v1' , bs') <- interpM c1 bs v1
  (v2' , bs'') <- interpM c2 bs' v2
  return $ (P (v1' , v2') , bs'')
interpM (c1 :- c2) bs (MV (v , True)) = do
  (v' , bs') <- interpM c1 bs v
  return (MV (v' , True) , bs')
interpM (c1 :- c2) bs (MV (v , False)) = do
  (v' , bs') <- interpM c2 bs v
  return (MV (v' , False), bs')
interpM Ret (b : bs) v = return (MV (v , b) , bs)
interpM Ext bs (MV (v , True)) = return (v , bs)
interpM Ext bs (MV (v , False)) = Nothing
interpM c bs v = return (interpF c v , bs)

ex1,ex2 :: C
ex1 = SwapP :@ Ret :@ Idx :@ (Id :- (SwapS :* SwapS)) :@ Ext
ex2 = (Ret :* Ret) :@ Ret :@ ((Idx :* Idx) :- (Idx :* Idx)) :@ ((Ext :* Ext) :- (Ext :* Ext)) :@ Ext
