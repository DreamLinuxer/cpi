module Gbit where
  open import Data.Bool
  open import Data.Empty
  open import Data.Unit
  open import Data.Sum
  open import Data.Product
  open import Function
  open import Relation.Binary.PropositionalEquality

  infix  70 _×ᵤ_
  infix  60 _+ᵤ_
  infix  40 _↔_
  infix  30 _⇔_
  infixr 50 _◎_
  infixr 50 _⊡_

  mutual
    data 𝕌ˢ : Set where
      𝟘 : 𝕌ˢ
      𝟙 : 𝕌ˢ
      _+ᵤ_ : 𝕌ˢ → 𝕌ˢ → 𝕌ˢ
      _×ᵤ_ : 𝕌ˢ → 𝕌ˢ → 𝕌ˢ
      ∑ : {t₁ t₂ : 𝕌ˢ} → t₁ ↔ t₂ → 𝕌ˢ

    𝟚 : 𝕌ˢ
    𝟚 = 𝟙 +ᵤ 𝟙

    data _↔_ : 𝕌ˢ → 𝕌ˢ → Set where
      unite₊l : {t : 𝕌ˢ} → 𝟘 +ᵤ t ↔ t
      uniti₊l : {t : 𝕌ˢ} → t ↔ 𝟘 +ᵤ t
      unite₊r : {t : 𝕌ˢ} → t +ᵤ 𝟘 ↔ t
      uniti₊r : {t : 𝕌ˢ} → t ↔ t +ᵤ 𝟘
      swap₊   : {t₁ t₂ : 𝕌ˢ} → t₁ +ᵤ t₂ ↔ t₂ +ᵤ t₁
      assocl₊ : {t₁ t₂ t₃ : 𝕌ˢ} → t₁ +ᵤ (t₂ +ᵤ t₃) ↔ (t₁ +ᵤ t₂) +ᵤ t₃
      assocr₊ : {t₁ t₂ t₃ : 𝕌ˢ} → (t₁ +ᵤ t₂) +ᵤ t₃ ↔ t₁ +ᵤ (t₂ +ᵤ t₃)
      unite⋆l : {t : 𝕌ˢ} → 𝟙 ×ᵤ t ↔ t
      uniti⋆l : {t : 𝕌ˢ} → t ↔ 𝟙 ×ᵤ t
      unite⋆r : {t : 𝕌ˢ} → t ×ᵤ 𝟙 ↔ t
      uniti⋆r : {t : 𝕌ˢ} → t ↔ t ×ᵤ 𝟙
      swap⋆   : {t₁ t₂ : 𝕌ˢ} → t₁ ×ᵤ t₂ ↔ t₂ ×ᵤ t₁
      assocl⋆ : {t₁ t₂ t₃ : 𝕌ˢ} → t₁ ×ᵤ (t₂ ×ᵤ t₃) ↔ (t₁ ×ᵤ t₂) ×ᵤ t₃
      assocr⋆ : {t₁ t₂ t₃ : 𝕌ˢ} → (t₁ ×ᵤ t₂) ×ᵤ t₃ ↔ t₁ ×ᵤ (t₂ ×ᵤ t₃)
      absorbr : {t : 𝕌ˢ} → 𝟘 ×ᵤ t ↔ 𝟘
      absorbl : {t : 𝕌ˢ} → t ×ᵤ 𝟘 ↔ 𝟘
      factorzr : {t : 𝕌ˢ} → 𝟘 ↔ t ×ᵤ 𝟘
      factorzl : {t : 𝕌ˢ} → 𝟘 ↔ 𝟘 ×ᵤ t
      dist    : {t₁ t₂ t₃ : 𝕌ˢ} → (t₁ +ᵤ t₂) ×ᵤ t₃ ↔ (t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃)
      factor  : {t₁ t₂ t₃ : 𝕌ˢ} → (t₁ ×ᵤ t₃) +ᵤ (t₂ ×ᵤ t₃) ↔ (t₁ +ᵤ t₂) ×ᵤ t₃
      distl   : {t₁ t₂ t₃ : 𝕌ˢ} → t₁ ×ᵤ (t₂ +ᵤ t₃) ↔ (t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃)
      factorl : {t₁ t₂ t₃ : 𝕌ˢ } → (t₁ ×ᵤ t₂) +ᵤ (t₁ ×ᵤ t₃) ↔ t₁ ×ᵤ (t₂ +ᵤ t₃)
      id↔     : {t : 𝕌ˢ} → t ↔ t
      _◎_     : {t₁ t₂ t₃ : 𝕌ˢ} → (t₁ ↔ t₂) → (t₂ ↔ t₃) → (t₁ ↔ t₃)
      _⊕_     : {t₁ t₂ t₃ t₄ : 𝕌ˢ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ +ᵤ t₂ ↔ t₃ +ᵤ t₄)
      _⊗_     : {t₁ t₂ t₃ t₄ : 𝕌ˢ} → (t₁ ↔ t₃) → (t₂ ↔ t₄) → (t₁ ×ᵤ t₂ ↔ t₃ ×ᵤ t₄)
      dpair : {t₁ t₂ : 𝕌ˢ} (c : t₁ ↔ t₂) → t₁ ↔ ∑ c
      pr₁ : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → ∑ c ↔ t₁
      coercionˢ : {t₁ t₂ : 𝕌ˢ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → ∑ c₁ ↔ ∑ c₂
      _⊝_ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c : t₁ ↔ t₂} (c₁ : t₁ ↔ t₃) (c₂ : t₂ ↔ t₄) → ∑ c ↔ ∑ (! c₁ ◎ c ◎ c₂)
      -- distˢ    : {t₁ t₂ t₃ : 𝕌ˢ} {c : t₂ ↔ t₃} → t₁ ×ᵤ ∑ c ↔ ∑ (id↔ {t₁} ⊗ c)
      -- factorˢ  : {t₁ t₂ t₃ : 𝕌ˢ} {c : t₂ ↔ t₃} → ∑ (id↔ {t₁} ⊗ c) ↔ t₁ ×ᵤ ∑ c
      -- distlˢ    : {t₁ t₂ t₃ : 𝕌ˢ} {c : t₂ ↔ t₃} → ∑ c ×ᵤ t₁  ↔ ∑ (c ⊗ id↔ {t₁})
      -- factorlˢ  : {t₁ t₂ t₃ : 𝕌ˢ} {c : t₂ ↔ t₃} → ∑ (c ⊗ id↔ {t₁}) ↔ ∑ c ×ᵤ t₁
      !const₀   : 𝟚 ↔ 𝟙
      !coercion₊ : {t : 𝕌ˢ} → t ↔ t +ᵤ t
      const₀   : 𝟙 ↔ 𝟚
      coercion₊ : {t : 𝕌ˢ} → t +ᵤ t ↔ t
      
    data _⇔_ : {t₁ t₂ : 𝕌ˢ} → (t₁ ↔ t₂) → (t₁ ↔ t₂) → Set where
      assoc◎l : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → (c₁ ◎ (c₂ ◎ c₃)) ⇔ ((c₁ ◎ c₂) ◎ c₃)
      assoc◎r : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₃ ↔ t₄}
              → ((c₁ ◎ c₂) ◎ c₃) ⇔ (c₁ ◎ (c₂ ◎ c₃))
      assocl⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊) ⇔ (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃))
      assocl⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl₊ ◎ ((c₁ ⊕ c₂) ⊕ c₃)) ⇔ ((c₁ ⊕ (c₂ ⊕ c₃)) ◎ assocl₊)
      assocl⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆) ⇔ (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃))
      assocl⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocl⋆ ◎ ((c₁ ⊗ c₂) ⊗ c₃)) ⇔ ((c₁ ⊗ (c₂ ⊗ c₃)) ◎ assocl⋆)
      assocr⊕r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊) ⇔ (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃)))
      assocr⊗l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃))) ⇔ (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆)
      assocr⊗r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (((c₁ ⊗ c₂) ⊗ c₃) ◎ assocr⋆) ⇔ (assocr⋆ ◎ (c₁ ⊗ (c₂ ⊗ c₃)))
      assocr⊕l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₅ ↔ t₆}
               → (assocr₊ ◎ (c₁ ⊕ (c₂ ⊕ c₃))) ⇔ (((c₁ ⊕ c₂) ⊕ c₃) ◎ assocr₊)
      dist⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → ((a ⊕ b) ⊗ c) ◎ dist ⇔ dist ◎ ((a ⊗ c) ⊕ (b ⊗ c))
      dist⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
             → dist ◎ ((a ⊗ c) ⊕ (b ⊗ c)) ⇔ ((a ⊕ b) ⊗ c) ◎ dist
      distl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → (a ⊗ (b ⊕ c)) ◎ distl ⇔ distl ◎ ((a ⊗ b) ⊕ (a ⊗ c))
      distl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
              → distl ◎ ((a ⊗ b) ⊕ (a ⊗ c)) ⇔ (a ⊗ (b ⊕ c)) ◎ distl
      factor⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor ⇔ factor ◎ ((a ⊕ b) ⊗ c)
      factor⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
               → factor ◎ ((a ⊕ b) ⊗ c) ⇔ ((a ⊗ c) ⊕ (b ⊗ c)) ◎ factor
      factorl⇔l : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl ⇔ factorl ◎ (a ⊗ (b ⊕ c))
      factorl⇔r : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {a : t₁ ↔ t₂} {b : t₃ ↔ t₄} {c : t₅ ↔ t₆}
                → factorl ◎ (a ⊗ (b ⊕ c)) ⇔ ((a ⊗ b) ⊕ (a ⊗ c)) ◎ factorl
      idl◎l   : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → (id↔ ◎ c) ⇔ c
      idl◎r   : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → c ⇔ id↔ ◎ c
      idr◎l   : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → (c ◎ id↔) ⇔ c
      idr◎r   : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → c ⇔ (c ◎ id↔) 
      linv◎l  : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → (c ◎ ! c) ⇔ id↔
      linv◎r  : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → id↔ ⇔ (c ◎ ! c) 
      rinv◎l  : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → (! c ◎ c) ⇔ id↔
      rinv◎r  : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → id↔ ⇔ (! c ◎ c) 
      unite₊l⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊l ◎ c₂) ⇔ ((c₁ ⊕ c₂) ◎ unite₊l)
      unite₊l⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊕ c₂) ◎ unite₊l) ⇔ (unite₊l ◎ c₂)
      uniti₊l⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊l ◎ (c₁ ⊕ c₂)) ⇔ (c₂ ◎ uniti₊l)
      uniti₊l⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊l) ⇔ (uniti₊l ◎ (c₁ ⊕ c₂))
      unite₊r⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (unite₊r ◎ c₂) ⇔ ((c₂ ⊕ c₁) ◎ unite₊r)
      unite₊r⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊕ c₁) ◎ unite₊r) ⇔ (unite₊r ◎ c₂)
      uniti₊r⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (uniti₊r ◎ (c₂ ⊕ c₁)) ⇔ (c₂ ◎ uniti₊r)
      uniti₊r⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟘 ↔ 𝟘} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti₊r) ⇔ (uniti₊r ◎ (c₂ ⊕ c₁))
      swapl₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap₊ ◎ (c₁ ⊕ c₂)) ⇔ ((c₂ ⊕ c₁) ◎ swap₊)
      swapr₊⇔ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊕ c₁) ◎ swap₊) ⇔ (swap₊ ◎ (c₁ ⊕ c₂))
      unitel⋆⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆l ◎ c₂) ⇔ ((c₁ ⊗ c₂) ◎ unite⋆l)
      uniter⋆⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₁ ⊗ c₂) ◎ unite⋆l) ⇔ (unite⋆l ◎ c₂)
      unitil⋆⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆l ◎ (c₁ ⊗ c₂)) ⇔ (c₂ ◎ uniti⋆l)
      unitir⋆⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆l) ⇔ (uniti⋆l ◎ (c₁ ⊗ c₂))
      unitel⋆⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (unite⋆r ◎ c₂) ⇔ ((c₂ ⊗ c₁) ◎ unite⋆r)
      uniter⋆⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → ((c₂ ⊗ c₁) ◎ unite⋆r) ⇔ (unite⋆r ◎ c₂)
      unitil⋆⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (uniti⋆r ◎ (c₂ ⊗ c₁)) ⇔ (c₂ ◎ uniti⋆r)
      unitir⋆⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : 𝟙 ↔ 𝟙} {c₂ : t₁ ↔ t₂}
                → (c₂ ◎ uniti⋆r) ⇔ (uniti⋆r ◎ (c₂ ⊗ c₁))
      swapl⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → (swap⋆ ◎ (c₁ ⊗ c₂)) ⇔ ((c₂ ⊗ c₁) ◎ swap⋆)
      swapr⋆⇔ : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄}
              → ((c₂ ⊗ c₁) ◎ swap⋆) ⇔ (swap⋆ ◎ (c₁ ⊗ c₂))
      id⇔     : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → c ⇔ c
      sym⇔    : {t₁ t₂ : 𝕌ˢ} {c₁ c₂ : t₁ ↔ t₂} → c₁ ⇔ c₂ → c₂ ⇔ c₁
      trans⇔  : {t₁ t₂ : 𝕌ˢ} {c₁ c₂ c₃ : t₁ ↔ t₂}
              → (c₁ ⇔ c₂) → (c₂ ⇔ c₃) → (c₁ ⇔ c₃)
      _⊡_  : {t₁ t₂ t₃ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₂ ↔ t₃} {c₃ : t₁ ↔ t₂} {c₄ : t₂ ↔ t₃}
           → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ◎ c₂) ⇔ (c₃ ◎ c₄)
      resp⊕⇔  : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊕ c₂) ⇔ (c₃ ⊕ c₄)
      resp⊗⇔  : {t₁ t₂ t₃ t₄ : 𝕌ˢ} {c₁ : t₁ ↔ t₂} {c₂ : t₃ ↔ t₄} {c₃ : t₁ ↔ t₂} {c₄ : t₃ ↔ t₄}
              → (c₁ ⇔ c₃) → (c₂ ⇔ c₄) → (c₁ ⊗ c₂) ⇔ (c₃ ⊗ c₄)
      -- below are the combinators added for the RigCategory structure
      id↔⊕id↔⇔ : {t₁ t₂ : 𝕌ˢ} → (id↔ {t₁} ⊕ id↔ {t₂}) ⇔ id↔
      split⊕-id↔ : {t₁ t₂ : 𝕌ˢ} → (id↔ {t₁ +ᵤ t₂}) ⇔ (id↔ ⊕ id↔)
      hom⊕◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄)) ⇔ ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄))
      hom◎⊕⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊕ c₂) ◎ (c₃ ⊕ c₄)) ⇔ ((c₁ ◎ c₃) ⊕ (c₂ ◎ c₄))
      id↔⊗id↔⇔ : {t₁ t₂ : 𝕌ˢ} → (id↔ {t₁} ⊗ id↔ {t₂}) ⇔ id↔
      split⊗-id↔ : {t₁ t₂ : 𝕌ˢ} → (id↔ {t₁ ×ᵤ t₂}) ⇔ (id↔ ⊗ id↔)
      hom⊗◎⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄)) ⇔ ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄))
      hom◎⊗⇔ : {t₁ t₂ t₃ t₄ t₅ t₆ : 𝕌ˢ} {c₁ : t₅ ↔ t₁} {c₂ : t₆ ↔ t₂} {c₃ : t₁ ↔ t₃} {c₄ : t₂ ↔ t₄}
             → ((c₁ ⊗ c₂) ◎ (c₃ ⊗ c₄)) ⇔ ((c₁ ◎ c₃) ⊗ (c₂ ◎ c₄))
      -- associativity triangle
      triangle⊕l : {t₁ t₂ : 𝕌ˢ} → (unite₊r {t₁} ⊕ id↔ {t₂}) ⇔ assocr₊ ◎ (id↔ ⊕ unite₊l)
      triangle⊕r : {t₁ t₂ : 𝕌ˢ} → assocr₊ ◎ (id↔ {t₁} ⊕ unite₊l {t₂}) ⇔ (unite₊r ⊕ id↔)
      triangle⊗l : {t₁ t₂ : 𝕌ˢ} → ((unite⋆r {t₁}) ⊗ id↔ {t₂}) ⇔ assocr⋆ ◎ (id↔ ⊗ unite⋆l)
      triangle⊗r : {t₁ t₂ : 𝕌ˢ} → (assocr⋆ ◎ (id↔ {t₁} ⊗ unite⋆l {t₂})) ⇔ (unite⋆r ⊗ id↔)
      pentagon⊕l : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                 → assocr₊ ◎ (assocr₊ {t₁} {t₂} {t₃ +ᵤ t₄})
                 ⇔ ((assocr₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
      pentagon⊕r : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                 → ((assocr₊ {t₁} {t₂} {t₃} ⊕ id↔ {t₄}) ◎ assocr₊) ◎ (id↔ ⊕ assocr₊)
                 ⇔ assocr₊ ◎ assocr₊
      pentagon⊗l : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                 → assocr⋆ ◎ (assocr⋆ {t₁} {t₂} {t₃ ×ᵤ t₄})
                 ⇔ ((assocr⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
      pentagon⊗r : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                 → ((assocr⋆ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ assocr⋆) ◎ (id↔ ⊗ assocr⋆)
                 ⇔ assocr⋆ ◎ assocr⋆
      -- from the braiding
      -- unit coherence
      unite₊l-coh-l : {t₁ : 𝕌ˢ} → unite₊l {t₁} ⇔ swap₊ ◎ unite₊r
      unite₊l-coh-r : {t₁ : 𝕌ˢ} → swap₊ ◎ unite₊r ⇔ unite₊l {t₁}
      unite⋆l-coh-l : {t₁ : 𝕌ˢ} → unite⋆l {t₁} ⇔ swap⋆ ◎ unite⋆r
      unite⋆l-coh-r : {t₁ : 𝕌ˢ} → swap⋆ ◎ unite⋆r ⇔ unite⋆l {t₁}
      hexagonr⊕l : {t₁ t₂ t₃ : 𝕌ˢ}
                 → (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
                 ⇔ ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
      hexagonr⊕r : {t₁ t₂ t₃ : 𝕌ˢ}
                 → ((swap₊ ⊕ id↔) ◎ assocr₊) ◎ (id↔ ⊕ swap₊)
                 ⇔ (assocr₊ ◎ swap₊) ◎ assocr₊ {t₁} {t₂} {t₃}
      hexagonl⊕l : {t₁ t₂ t₃ : 𝕌ˢ}
                 → (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
      hexagonl⊕r : {t₁ t₂ t₃ : 𝕌ˢ}
                 → ((id↔ ⊕ swap₊) ◎ assocl₊) ◎ (swap₊ ⊕ id↔)
                 ⇔ (assocl₊ ◎ swap₊) ◎ assocl₊ {t₁} {t₂} {t₃}
      hexagonr⊗l : {t₁ t₂ t₃ : 𝕌ˢ}
                 → (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
                 ⇔ ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
      hexagonr⊗r : {t₁ t₂ t₃ : 𝕌ˢ}
                 → ((swap⋆ ⊗ id↔) ◎ assocr⋆) ◎ (id↔ ⊗ swap⋆)
                 ⇔ (assocr⋆ ◎ swap⋆) ◎ assocr⋆ {t₁} {t₂} {t₃}
      hexagonl⊗l : {t₁ t₂ t₃ : 𝕌ˢ}
                 → (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
                 ⇔ ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
      hexagonl⊗r : {t₁ t₂ t₃ : 𝕌ˢ}
                 → ((id↔ ⊗ swap⋆) ◎ assocl⋆) ◎ (swap⋆ ⊗ id↔)
                 ⇔ (assocl⋆ ◎ swap⋆) ◎ assocl⋆ {t₁} {t₂} {t₃}
      absorbl⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                → (c₁ ⊗ id↔ {𝟘}) ◎ absorbl ⇔ absorbl ◎ id↔ {𝟘}
      absorbl⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                → absorbl ◎ id↔ {𝟘} ⇔ (c₁ ⊗ id↔ {𝟘}) ◎ absorbl
      absorbr⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                → (id↔ {𝟘} ⊗ c₁) ◎ absorbr ⇔ absorbr ◎ id↔ {𝟘}
      absorbr⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                → absorbr ◎ id↔ {𝟘} ⇔ (id↔ {𝟘} ⊗ c₁) ◎ absorbr
      factorzl⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzl ⇔ factorzl ◎ (id↔ ⊗ c₁)
      factorzl⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                 → factorzl ◎ (id↔ {𝟘} ⊗ c₁) ⇔ id↔ {𝟘} ◎ factorzl
      factorzr⇔l : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                 → id↔ ◎ factorzr ⇔ factorzr ◎ (c₁ ⊗ id↔)
      factorzr⇔r : {t₁ t₂ : 𝕌ˢ} {c₁ : t₁ ↔ t₂}
                 → factorzr ◎ (c₁ ⊗ id↔) ⇔ id↔ ◎ factorzr
      -- from the coherence conditions of RigCategory
      swap₊distl⇔l : {t₁ t₂ t₃ : 𝕌ˢ}
                   → (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl ⇔ distl ◎ swap₊
      swap₊distl⇔r : {t₁ t₂ t₃ : 𝕌ˢ}
                   → distl ◎ swap₊ ⇔ (id↔ {t₁} ⊗ swap₊ {t₂} {t₃}) ◎ distl
      dist-swap⋆⇔l : {t₁ t₂ t₃ : 𝕌ˢ}
                   → dist {t₁} {t₂} {t₃} ◎ (swap⋆ ⊕ swap⋆) ⇔ swap⋆ ◎ distl
      dist-swap⋆⇔r : {t₁ t₂ t₃ : 𝕌ˢ}
                   → swap⋆ ◎ distl {t₁} {t₂} {t₃} ⇔ dist ◎ (swap⋆ ⊕ swap⋆)
      assocl₊-dist-dist⇔l : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                          → ((assocl₊ {t₁} {t₂} {t₃} ⊗ id↔ {t₄}) ◎ dist) ◎ (dist ⊕ id↔)
                          ⇔ (dist ◎ (id↔ ⊕ dist)) ◎ assocl₊
      assocl₊-dist-dist⇔r : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                          → (dist {t₁} ◎ (id↔ ⊕ dist {t₂} {t₃} {t₄})) ◎ assocl₊
                          ⇔ ((assocl₊ ⊗ id↔) ◎ dist) ◎ (dist ⊕ id↔)
      assocl⋆-distl⇔l : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                      → assocl⋆ {t₁} {t₂} ◎ distl {t₁ ×ᵤ t₂} {t₃} {t₄}
                      ⇔ ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
      assocl⋆-distl⇔r : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                      → ((id↔ ⊗ distl) ◎ distl) ◎ (assocl⋆ ⊕ assocl⋆)
                      ⇔ assocl⋆ {t₁} {t₂} ◎ distl {t₁ ×ᵤ t₂} {t₃} {t₄}  
      absorbr0-absorbl0⇔ : absorbr {𝟘} ⇔ absorbl {𝟘}
      absorbl0-absorbr0⇔ : absorbl {𝟘} ⇔ absorbr {𝟘}
      absorbr⇔distl-absorb-unite : {t₁ t₂ : 𝕌ˢ}
                                 → absorbr ⇔ (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l
      distl-absorb-unite⇔absorbr : {t₁ t₂ : 𝕌ˢ}
                                 → (distl {t₂ = t₁} {t₂} ◎ (absorbr ⊕ absorbr)) ◎ unite₊l ⇔ absorbr
      unite⋆r0-absorbr1⇔ : unite⋆r ⇔ absorbr
      absorbr1-unite⋆r-⇔ : absorbr ⇔ unite⋆r
      absorbl≡swap⋆◎absorbr : {t₁ : 𝕌ˢ} → absorbl {t₁} ⇔ swap⋆ ◎ absorbr
      swap⋆◎absorbr≡absorbl : {t₁ : 𝕌ˢ} → swap⋆ ◎ absorbr ⇔ absorbl {t₁}
      absorbr⇔[assocl⋆◎[absorbr⊗id↔]]◎absorbr : {t₁ t₂ : 𝕌ˢ}
                                              → absorbr ⇔ (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr
      [assocl⋆◎[absorbr⊗id↔]]◎absorbr⇔absorbr : {t₁ t₂ : 𝕌ˢ}
                                              → (assocl⋆ {𝟘} {t₁} {t₂} ◎ (absorbr ⊗ id↔)) ◎ absorbr ⇔ absorbr
      [id↔⊗absorbr]◎absorbl⇔assocl⋆◎[absorbl⊗id↔]◎absorbr : {t₁ t₂ : 𝕌ˢ}
                                                          → (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
                                                          ⇔ (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
      assocl⋆◎[absorbl⊗id↔]◎absorbr⇔[id↔⊗absorbr]◎absorbl : {t₁ t₂ : 𝕌ˢ}
                                                          → (assocl⋆ ◎ (absorbl ⊗ id↔)) ◎ absorbr
                                                          ⇔ (id↔ ⊗ absorbr {t₂}) ◎ absorbl {t₁}
      elim⊥-A[0⊕B]⇔l : {t₁ t₂ : 𝕌ˢ}
                     → (id↔ {t₁} ⊗ unite₊l {t₂})
                     ⇔ (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
      elim⊥-A[0⊕B]⇔r : {t₁ t₂ : 𝕌ˢ}
                     → (distl ◎ (absorbl ⊕ id↔)) ◎ unite₊l
                     ⇔ (id↔ {t₁} ⊗ unite₊l {t₂})
      elim⊥-1[A⊕B]⇔l : {t₁ t₂ : 𝕌ˢ}
                     → unite⋆l ⇔ distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂})
      elim⊥-1[A⊕B]⇔r : {t₁ t₂ : 𝕌ˢ} → distl ◎ (unite⋆l {t₁} ⊕ unite⋆l {t₂}) ⇔ unite⋆l
      fully-distribute⇔l : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                         → (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
                         ⇔ ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
      fully-distribute⇔r : {t₁ t₂ t₃ t₄ : 𝕌ˢ}
                         → ((((dist ◎ (distl ⊕ distl)) ◎ assocl₊) ◎ (assocr₊ ⊕ id↔)) ◎ ((id↔ ⊕ swap₊) ⊕ id↔)) ◎ (assocl₊ ⊕ id↔)
                         ⇔ (distl ◎ (dist {t₁} {t₂} {t₃} ⊕ dist {t₁} {t₂} {t₄})) ◎ assocl₊
      -- make things easier
      cong! : {t₁ t₂ : 𝕌ˢ} {c₁ c₂ : t₁ ↔ t₂} → (c₁ ⇔ c₂) → (! c₁ ⇔ ! c₂)
  
    ! : {t₁ t₂ : 𝕌ˢ} → t₁ ↔ t₂ → t₂ ↔ t₁
    ! unite₊l = uniti₊l
    ! uniti₊l = unite₊l
    ! unite₊r = uniti₊r
    ! uniti₊r = unite₊r
    ! swap₊ = swap₊
    ! assocl₊ = assocr₊
    ! assocr₊ = assocl₊
    ! unite⋆l = uniti⋆l
    ! uniti⋆l = unite⋆l
    ! unite⋆r = uniti⋆r
    ! uniti⋆r = unite⋆r
    ! swap⋆ = swap⋆
    ! assocl⋆ = assocr⋆
    ! assocr⋆ = assocl⋆
    ! absorbr = factorzl
    ! absorbl = factorzr
    ! factorzr = absorbl
    ! factorzl = absorbr
    ! dist = factor
    ! factor = dist
    ! distl = factorl
    ! factorl = distl
    ! id↔ = id↔
    ! (c₁ ◎ c₂) = ! c₂ ◎ ! c₁
    ! (c₁ ⊕ c₂) = ! c₁ ⊕ ! c₂
    ! (c₁ ⊗ c₂) = ! c₁ ⊗ ! c₂
    ! (dpair c) = pr₁
    ! pr₁ = dpair _
    ! (coercionˢ c₁⇔c₂) = coercionˢ (sym⇔ c₁⇔c₂)
    ! (c₁ ⊝ c₂) = (! c₁ ⊝ ! c₂) ◎ coercionˢ (trans⇔ assoc◎l
                                            (trans⇔ (assoc◎l ⊡ id⇔)
                                            (trans⇔ ((rinv◎l ⊡ id⇔) ⊡ id⇔)
                                            (trans⇔ (idl◎l ⊡ id⇔)
                                            (trans⇔ assoc◎r
                                            (trans⇔ (id⇔ ⊡ linv◎l) idr◎l))))))
    -- ! distˢ = factorˢ
    -- ! factorˢ = distˢ
    -- ! distlˢ = factorlˢ
    -- ! factorlˢ = distlˢ
    ! !const₀ = !coercion₊
    ! !coercion₊ = coercion₊
    ! const₀ = !const₀
    ! coercion₊ = !coercion₊

  swapˢ : {t₁ t₂ : 𝕌ˢ} {c : t₁ ↔ t₂} → ∑ c ↔ ∑ (! c)
  swapˢ {t₁} {t₂} {c} = (c ⊝ ! c) ◎ coercionˢ (trans⇔ (id⇔ ⊡ linv◎l) idr◎l)

  mutual
    data 𝔻𝕡 : (t₁ t₂ : 𝕌ˢ) → Set where
      _ˢ : {t₁ t₂ : 𝕌ˢ} → ⟦ t₁ ⟧ˢ → 𝔻𝕡 t₁ t₂
      
    ⟦_⟧ˢ : 𝕌ˢ → Set
    ⟦ 𝟘 ⟧ˢ = ⊥
    ⟦ 𝟙 ⟧ˢ = ⊤
    ⟦ t₁ ×ᵤ t₂ ⟧ˢ = ⟦ t₁ ⟧ˢ × ⟦ t₂ ⟧ˢ
    ⟦ t₁ +ᵤ t₂ ⟧ˢ = ⟦ t₁ ⟧ˢ ⊎ ⟦ t₂ ⟧ˢ
    ⟦ ∑ {t₁} {t₂} c ⟧ˢ = 𝔻𝕡 t₁ t₂

    0₂ 1₂ : ⟦ 𝟚 ⟧ˢ
    0₂ = inj₁ tt
    1₂ = inj₂ tt
    
  interp𝕌ˢ : {t₁ t₂ : 𝕌ˢ} → (t₁ ↔ t₂) → ⟦ t₁ ⟧ˢ → ⟦ t₂ ⟧ˢ
  interp𝕌ˢ (dpair c) v = v ˢ
  interp𝕌ˢ (c₁ ⊝ c₂) (v ˢ) = interp𝕌ˢ c₁ v ˢ
  interp𝕌ˢ pr₁ (v ˢ) = v
  interp𝕌ˢ (coercionˢ _) v = v
  -- interp𝕌ˢ distˢ (v₁ , v₂ ˢ) = (v₁ , v₂) ˢ
  -- interp𝕌ˢ factorˢ ((v₁ , v₂) ˢ) = v₁ , (v₂ ˢ)
  -- interp𝕌ˢ distlˢ (v₁ ˢ , v₂) = (v₁ , v₂) ˢ
  -- interp𝕌ˢ factorlˢ ((v₁ , v₂) ˢ) = (v₁ ˢ) , v₂
  interp𝕌ˢ unite₊l (inj₁ ())
  interp𝕌ˢ unite₊l (inj₂ v) = v
  interp𝕌ˢ uniti₊l v = inj₂ v
  interp𝕌ˢ unite₊r (inj₁ v) = v
  interp𝕌ˢ unite₊r (inj₂ ())
  interp𝕌ˢ uniti₊r v = inj₁ v
  interp𝕌ˢ swap₊ (inj₁ v) = inj₂ v
  interp𝕌ˢ swap₊ (inj₂ v) = inj₁ v
  interp𝕌ˢ assocl₊ (inj₁ v) = inj₁ (inj₁ v)
  interp𝕌ˢ assocl₊ (inj₂ (inj₁ v)) = inj₁ (inj₂ v)
  interp𝕌ˢ assocl₊ (inj₂ (inj₂ v)) = inj₂ v
  interp𝕌ˢ assocr₊ (inj₁ (inj₁ v)) = inj₁ v
  interp𝕌ˢ assocr₊ (inj₁ (inj₂ v)) = inj₂ (inj₁ v)
  interp𝕌ˢ assocr₊ (inj₂ v) = inj₂ (inj₂ v)
  interp𝕌ˢ unite⋆l v = proj₂ v
  interp𝕌ˢ uniti⋆l v = tt , v
  interp𝕌ˢ unite⋆r v = proj₁ v
  interp𝕌ˢ uniti⋆r v = v , tt
  interp𝕌ˢ swap⋆ (v₁ , v₂) = v₂ , v₁
  interp𝕌ˢ assocl⋆ (v₁ , v₂ , v₃) = (v₁ , v₂) , v₃
  interp𝕌ˢ assocr⋆ ((v₁ , v₂) , v₃) = v₁ , v₂ , v₃
  interp𝕌ˢ absorbr (() , v)
  interp𝕌ˢ absorbl (v , ())
  interp𝕌ˢ factorzr ()
  interp𝕌ˢ factorzl ()
  interp𝕌ˢ dist (inj₁ v₁ , v₃) = inj₁ (v₁ , v₃)
  interp𝕌ˢ dist (inj₂ v₂ , v₃) = inj₂ (v₂ , v₃)
  interp𝕌ˢ factor (inj₁ (v₁ , v₃)) = inj₁ v₁ , v₃
  interp𝕌ˢ factor (inj₂ (v₂ , v₃)) = inj₂ v₂ , v₃
  interp𝕌ˢ distl (v₁ , inj₁ v₂) = inj₁ (v₁ , v₂)
  interp𝕌ˢ distl (v₁ , inj₂ v₃) = inj₂ (v₁ , v₃)
  interp𝕌ˢ factorl (inj₁ (v₁ , v₂)) = v₁ , inj₁ v₂
  interp𝕌ˢ factorl (inj₂ (v₁ , v₃)) = v₁ , inj₂ v₃
  interp𝕌ˢ id↔ v = v
  interp𝕌ˢ (c₁ ◎ c₂) v = interp𝕌ˢ c₂ (interp𝕌ˢ c₁ v)
  interp𝕌ˢ (c₁ ⊕ c₂) (inj₁ v) = inj₁ (interp𝕌ˢ c₁ v)
  interp𝕌ˢ (c₁ ⊕ c₂) (inj₂ v) = inj₂ (interp𝕌ˢ c₂ v)
  interp𝕌ˢ (c₁ ⊗ c₂) (v₁ , v₂) = interp𝕌ˢ c₁ v₁ , interp𝕌ˢ c₂ v₂
  interp𝕌ˢ !const₀ v = tt
  interp𝕌ˢ !coercion₊ v = inj₁ v
  interp𝕌ˢ const₀ v = 0₂
  interp𝕌ˢ coercion₊ (inj₁ v) = v
  interp𝕌ˢ coercion₊ (inj₂ v) = v

  ℝ : 𝕌ˢ
  ℝ = 𝟚
  
  𝔾bit : 𝕌ˢ
  𝔾bit = ∑ (id↔ {𝟚}) +ᵤ ∑ (swap₊ {𝟙} {𝟙})

  Init : ℝ ↔ 𝔾bit
  Init = uniti⋆l ◎ distl ◎ ( (unite⋆l ◎ const₀ ◎ dpair _ ◎ swapˢ)
                           ⊕ (unite⋆l ◎ const₀ ◎ dpair _ ◎ swapˢ))

  Swap : 𝔾bit ↔ 𝔾bit
  Swap = swapˢ ⊕ swapˢ

  Read : 𝔾bit ↔ 𝟚
  Read = (pr₁ ⊕ pr₁) ◎ coercion₊

  Write : 𝟚 ×ᵤ ℝ ↔ 𝔾bit
  Write = distl ◎ ( (unite⋆r ◎ (dpair id↔))
                  ⊕ (unite⋆r ◎ (dpair swap₊)))

  CNOT : 𝔾bit ×ᵤ 𝟚 ↔ 𝔾bit ×ᵤ 𝟚
  CNOT = distl
       ◎ ( id↔   -- no flip
         ⊕ ( ( ((id↔ ⊝ swap₊) ⊕ (id↔ ⊝ swap₊)) -- flip internal bit
             ◎ ( (coercionˢ (trans⇔ idl◎l idl◎l))
               ⊕ (coercionˢ (trans⇔ idl◎l linv◎l)))
             ◎ swap₊) 
           ⊗ id↔)) 
       ◎ factorl
  
  Tick : 𝔾bit ×ᵤ 𝔾bit ↔ 𝔾bit ×ᵤ 𝔾bit
  Tick = distl
       ◎ ((id↔ ⊗ pr₁) ⊕ (id↔ ⊗ pr₁)) -- project B
       ◎ (CNOT ⊕ CNOT)
       ◎ ((id↔ ⊗ dpair id↔) ⊕ (id↔ ⊗ dpair swap₊)) -- rebuild B
       ◎ factorl
       ◎ dist
       ◎ ((pr₁ ⊗ id↔) ⊕ (pr₁ ⊗ id↔)) -- project A
       ◎ (swap⋆ ⊕ swap⋆)
       ◎ (CNOT ⊕ CNOT)
       ◎ (swap⋆ ⊕ swap⋆)
       ◎ ((dpair id↔ ⊗ id↔) ⊕ (dpair swap₊ ⊗ id↔)) -- rebuild A
       ◎ factor


  test01 : interp𝕌ˢ (Init ◎ Read) 0₂ ≡ 0₂
  test01 = refl
  test02 : interp𝕌ˢ (Init ◎ Swap ◎ Read) 0₂ ≡ 0₂
  test02 = refl
  test03 : interp𝕌ˢ (Init ◎ Read) 1₂ ≡ 1₂
  test03 = refl
  test04 : interp𝕌ˢ (Init ◎ Swap ◎ Read) 1₂ ≡ 0₂
  test04 = refl
  test05 : interp𝕌ˢ (Write ◎ Read) (0₂ , 0₂) ≡ 0₂
  test05 = refl
  test06 : interp𝕌ˢ (Write ◎ Swap ◎ Read) (0₂ , 0₂) ≡ 0₂
  test06 = refl
  test07 : interp𝕌ˢ (Write ◎ Read) (0₂ , 1₂) ≡ 0₂
  test07 = refl
  test08 : interp𝕌ˢ (Write ◎ Swap ◎ Read) (0₂ , 1₂) ≡ 1₂
  test08 = refl
  test09 : interp𝕌ˢ (Write ◎ Read) (1₂ , 0₂) ≡ 1₂
  test09 = refl
  test10 : interp𝕌ˢ (Write ◎ Swap ◎ Read) (1₂ , 0₂) ≡ 1₂
  test10 = refl
  test11 : interp𝕌ˢ (Write ◎ Read) (1₂ , 1₂) ≡ 1₂
  test11 = refl
  test12 : interp𝕌ˢ (Write ◎ Swap ◎ Read) (1₂ , 1₂) ≡ 0₂
  test12 = refl
  -- (0,0) (0,0)
  test13 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 0₂) , (0₂ , 0₂)) ≡ (0₂ , 0₂)
  test13 = refl
  test14 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 0₂) , (0₂ , 0₂)) ≡ (0₂ , 0₂)
  test14 = refl
  -- (0,0) (0,1)
  test15 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 0₂) , (0₂ , 1₂)) ≡ (0₂ , 0₂)
  test15 = refl
  test16 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 0₂) , (0₂ , 1₂)) ≡ (0₂ , 1₂)
  test16 = refl
  -- (0,0) (1,1)
  test17 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 0₂) , (1₂ , 0₂)) ≡ (0₂ , 1₂)
  test17 = refl
  test18 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 0₂) , (1₂ , 0₂)) ≡ (1₂ , 1₂)
  test18 = refl
  -- (0,0) (1,0)
  test19 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 0₂) , (1₂ , 1₂)) ≡ (0₂ , 1₂)
  test19 = refl
  test20 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 0₂) , (1₂ , 1₂)) ≡ (1₂ , 0₂)
  test20 = refl
  -- (0,1) (0,0)
  test21 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 1₂) , (0₂ , 0₂)) ≡ (0₂ , 0₂)
  test21 = refl
  test22 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 1₂) , (0₂ , 0₂)) ≡ (1₂ , 0₂)
  test22 = refl
  -- (0,1) (0,1)
  test23 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 1₂) , (0₂ , 1₂)) ≡ (0₂ , 0₂)
  test23 = refl
  test24 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 1₂) , (0₂ , 1₂)) ≡ (1₂ , 1₂)
  test24 = refl
  -- (0,1) (1,1)
  test25 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 1₂) , (1₂ , 0₂)) ≡ (0₂ , 1₂)
  test25 = refl
  test26 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 1₂) , (1₂ , 0₂)) ≡ (0₂ , 1₂)
  test26 = refl
  -- (0,1) (1,0)
  test27 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((0₂ , 1₂) , (1₂ , 1₂)) ≡ (0₂ , 1₂)
  test27 = refl
  test28 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((0₂ , 1₂) , (1₂ , 1₂)) ≡ (0₂ , 0₂)
  test28 = refl
  -- (1,1) (0,0)
  test29 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 0₂) , (0₂ , 0₂)) ≡ (1₂ , 0₂)
  test29 = refl
  test30 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 0₂) , (0₂ , 0₂)) ≡ (1₂ , 1₂)
  test30 = refl
  -- (1,1) (0,1)
  test31 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 0₂) , (0₂ , 1₂)) ≡ (1₂ , 0₂)
  test31 = refl
  test32 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 0₂) , (0₂ , 1₂)) ≡ (1₂ , 0₂)
  test32 = refl
  -- (1,1) (1,1)
  test33 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 0₂) , (1₂ , 0₂)) ≡ (1₂ , 1₂)
  test33 = refl
  test34 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 0₂) , (1₂ , 0₂)) ≡ (0₂ , 0₂)
  test34 = refl
  -- (1,1) (1,0)
  test35 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 0₂) , (1₂ , 1₂)) ≡ (1₂ , 1₂)
  test35 = refl
  test36 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 0₂) , (1₂ , 1₂)) ≡ (0₂ , 1₂)
  test36 = refl
  -- (1,0) (0,0)
  test37 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 1₂) , (0₂ , 0₂)) ≡ (1₂ , 0₂)
  test37 = refl
  test38 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 1₂) , (0₂ , 0₂)) ≡ (0₂ , 1₂)
  test38 = refl
  -- (1,0) (0,1)
  test39 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 1₂) , (0₂ , 1₂)) ≡ (1₂ , 0₂)
  test39 = refl
  test40 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 1₂) , (0₂ , 1₂)) ≡ (0₂ , 0₂)
  test40 = refl
  -- (1,0) (1,1)
  test41 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 1₂) , (1₂ , 0₂)) ≡ (1₂ , 1₂)
  test41 = refl
  test42 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 1₂) , (1₂ , 0₂)) ≡ (1₂ , 0₂)
  test42 = refl
  -- (1,0) (1,0)
  test43 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ (Read ⊗ Read)) ((1₂ , 1₂) , (1₂ , 1₂)) ≡ (1₂ , 1₂)
  test43 = refl
  test44 : interp𝕌ˢ ((Write ⊗ Write) ◎ Tick ◎ ((Swap ◎ Read) ⊗ (Swap ◎ Read))) ((1₂ , 1₂) , (1₂ , 1₂)) ≡ (1₂ , 1₂)
  test44 = refl

  CTickₗ : 𝟚 ×ᵤ 𝔾bit ↔ 𝟚 ×ᵤ 𝔾bit
  CTickₗ = swap⋆ ◎ CNOT ◎ swap⋆

  CTickᵣ : 𝔾bit ×ᵤ 𝟚 ↔ 𝔾bit ×ᵤ 𝟚
  CTickᵣ = CNOT

  IRead : 𝔾bit ↔ 𝟚
  IRead = Swap ◎ Read 

  entanglement : ℝ ×ᵤ ℝ ↔ 𝔾bit ×ᵤ 𝔾bit
  entanglement = (Init ⊗ Init) ◎ Tick ◎ (id↔ ⊗ Swap)

  densecoding : (𝟚 ×ᵤ 𝟚) ×ᵤ (ℝ ×ᵤ ℝ) ↔ (𝟚 ×ᵤ 𝟚) ×ᵤ (𝟚 ×ᵤ 𝟚)
  densecoding = (id↔ ⊗ entanglement)
              ◎ assocl⋆
              ◎ (assocr⋆ ⊗ id↔)
              ◎ ((id↔ ⊗ CTickₗ) ⊗ id↔)
              ◎ (assocl⋆ ⊗ id↔)
              ◎ ((swap⋆ ⊗ Swap) ⊗ id↔)
              ◎ (assocr⋆ ⊗ id↔)
              ◎ ((id↔ ⊗ CTickₗ) ⊗ id↔)
              ◎ (assocl⋆ ⊗ id↔)
              ◎ assocr⋆
              ◎ (swap⋆ ⊗ Tick)
              ◎ (id↔ ⊗ (IRead ⊗ IRead))

  test45 : interp𝕌ˢ densecoding ((1₂ , 0₂) , (1₂ , 0₂)) ≡ ((1₂ , 0₂) , (1₂ , 0₂))
  test45 = refl

  teleport : 𝔾bit ×ᵤ (ℝ ×ᵤ ℝ) ↔ (𝟚 ×ᵤ 𝟚) ×ᵤ 𝔾bit
  teleport = (id↔ ⊗ entanglement)
           ◎ assocl⋆
           ◎ (Tick ⊗ id↔)
           ◎ ((IRead ⊗ IRead) ⊗ id↔)
           ◎ assocr⋆
           ◎ (id↔ ⊗ CTickₗ)
           ◎ assocl⋆
           ◎ (swap⋆ ⊗ Swap)
           ◎ assocr⋆
           ◎ (id↔ ⊗ CTickₗ)
           ◎ assocl⋆
           ◎ (swap⋆ ⊗ id↔)
