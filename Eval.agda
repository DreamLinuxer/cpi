module Eval where
open import Data.Empty
open import Data.Unit
open import Data.Product
open import Data.Sum
open import Pi

⟦_⟧ : 𝕌 → Set
⟦ 𝟘 ⟧ = ⊥
⟦ 𝟙 ⟧ = ⊤
⟦ t₁ +ᵤ t₂ ⟧ = ⟦ t₁ ⟧ ⊎ ⟦ t₂ ⟧
⟦ t₁ ×ᵤ t₂ ⟧ = ⟦ t₁ ⟧ × ⟦ t₂ ⟧

𝕖𝕧 : {t₁ t₂ : 𝕌} → (c : t₁ ↔ t₂) → ⟦ t₁ ⟧ → ⟦ t₂ ⟧
𝕖𝕧 unite₊l (inj₁ ())
𝕖𝕧 unite₊l (inj₂ x) = x
𝕖𝕧 uniti₊l a = inj₂ a
𝕖𝕧 unite₊r (inj₁ x) = x
𝕖𝕧 unite₊r (inj₂ ())
𝕖𝕧 uniti₊r a = inj₁ a
𝕖𝕧 swap₊ (inj₁ x) = inj₂ x
𝕖𝕧 swap₊ (inj₂ x) = inj₁ x
𝕖𝕧 assocl₊ (inj₁ x) = inj₁ (inj₁ x)
𝕖𝕧 assocl₊ (inj₂ (inj₁ x)) = inj₁ (inj₂ x)
𝕖𝕧 assocl₊ (inj₂ (inj₂ x)) = inj₂ x
𝕖𝕧 assocr₊ (inj₁ (inj₁ x)) = inj₁ x
𝕖𝕧 assocr₊ (inj₁ (inj₂ x)) = inj₂ (inj₁ x)
𝕖𝕧 assocr₊ (inj₂ x) = inj₂ (inj₂ x)
𝕖𝕧 unite⋆l x = proj₂ x
𝕖𝕧 uniti⋆l x = tt , x
𝕖𝕧 unite⋆r x = proj₁ x
𝕖𝕧 uniti⋆r x = x , tt
𝕖𝕧 swap⋆ (x , y) = y , x
𝕖𝕧 assocl⋆ (x , y , z) = (x , y) , z
𝕖𝕧 assocr⋆ ((x , y) , z) = x , y , z
𝕖𝕧 absorbr (() , _)
𝕖𝕧 absorbl (_ , ())
𝕖𝕧 factorzr ()
𝕖𝕧 factorzl ()
𝕖𝕧 dist (inj₁ x , z) = inj₁ (x , z)
𝕖𝕧 dist (inj₂ y , z) = inj₂ (y , z)
𝕖𝕧 factor (inj₁ (x , y)) = inj₁ x , y
𝕖𝕧 factor (inj₂ (x , y)) = inj₂ x , y
𝕖𝕧 distl (x , inj₁ y) = inj₁ (x , y)
𝕖𝕧 distl (x , inj₂ y) = inj₂ (x , y)
𝕖𝕧 factorl (inj₁ (x , y)) = x , inj₁ y
𝕖𝕧 factorl (inj₂ (x , y)) = x , inj₂ y
𝕖𝕧 id↔ a = a
𝕖𝕧 (c₁ ◎ c₂) a = 𝕖𝕧 c₂ (𝕖𝕧 c₁ a)
𝕖𝕧 (c₁ ⊕ c₂) (inj₁ x) = inj₁ (𝕖𝕧 c₁ x)
𝕖𝕧 (c₁ ⊕ c₂) (inj₂ y) = inj₂ (𝕖𝕧 c₂ y)
𝕖𝕧 (c₁ ⊗ c₂) (x , y) = 𝕖𝕧 c₁ x , 𝕖𝕧 c₂ y
