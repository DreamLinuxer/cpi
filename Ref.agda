module Ref where

open import Data.Unit
open import Data.Nat
open import Data.Product

-- Axioms for reference type
postulate
  Ref : ∀ {ℓ} (A : Set ℓ) → Set ℓ
  get : ∀ {ℓ} {A : Set ℓ} → Ref A → A
  put : ∀ {ℓ} {A : Set ℓ} → A → Ref A → ⊤


